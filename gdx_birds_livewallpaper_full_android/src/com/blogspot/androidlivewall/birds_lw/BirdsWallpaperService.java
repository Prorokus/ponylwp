package com.blogspot.androidlivewall.birds_lw;

import lw_engine.birds_lw.BirdsLW;

import com.badlogic.gdx.backends.android.livewallpaper.AndroidApplication;
import com.eightbitmage.gdxlw.LibdgxWallpaperService;

public class BirdsWallpaperService extends LibdgxWallpaperService {
	
	@Override
	public Engine onCreateEngine() {
		//android.os.Debug.waitForDebugger();
		// TODO Auto-generated method stub
		return new BirdsWallpaperEngine(this);
	}
	
	public class BirdsWallpaperEngine extends LibdgxWallpaperEngine
	{

		public BirdsWallpaperEngine(
				LibdgxWallpaperService libdgxWallpaperService) {
			super(libdgxWallpaperService);
			// TODO Auto-generated constructor stub
		} 
		
		protected void initialize(AndroidApplication androidApplicationLW)
		{
			BirdsLW app = new BirdsLW();
			setWallpaperListener(app);
			androidApplicationLW.initialize(app,false);
		}
	}
}