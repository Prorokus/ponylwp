package com.blogspot.androidlivewall.hover_pony_lw_free;

import lw_engine.hover_pony_lw.HoverPonyLW;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.livewallpaper.AndroidPreferences;

public class HoverPonyWallpaperSettings extends PreferenceActivity
implements SharedPreferences.OnSharedPreferenceChangeListener, OnPreferenceClickListener
{
	public String MY_PONY_PREFS = "com.blogspot.androidlivewall.hover_pony_lw_free_preferences";
	private AndroidPreferences prefs;
	private boolean prefsChanged = false;
	
	private String uriPkg;
	
	protected void onCreate(Bundle icicle) {
		
    	super.onCreate(icicle);
    	try{
    		if (Gdx.app != null)
    			prefs = (AndroidPreferences) Gdx.app.getPreferences(MY_PONY_PREFS);
    	}
    	catch(NullPointerException ex)
    	{
    		Log.e("MyPonyLW_Setting Error", ex.toString());
    	}
    	addPreferencesFromResource(R.xml.settings);
    	
    	uriPkg = "market://details?id=" + getString(R.string.other_pony_pkg);
    	
    	
    	final Preference hover_full_ver_but = (Preference)findPreference(getString(R.string.full_ver_but_key));
    	hover_full_ver_but.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_VIEW);
				final String uriTo = "hover_pony_lw_full";
				intent.setData(Uri.parse( uriPkg + uriTo));
				
				try {
		            startActivity(intent);
		        } catch (ActivityNotFoundException e) {
		        	Toast.makeText(getBaseContext(), "Cannot find Google Play",
		                    Toast.LENGTH_LONG).show();
		            return false;
		        } 
				return true;
			}
		});
    	
    	final Preference my_pony_lwp_but = (Preference)findPreference(getString(R.string.my_pony_but_key));
    	my_pony_lwp_but.setOnPreferenceClickListener(this);
    	
    	final Preference patrick_lwp_but = (Preference)findPreference(getString(R.string.patrick_but_key));
    	patrick_lwp_but.setOnPreferenceClickListener(this);
    	
    	final Preference easter_lwp_but = (Preference)findPreference(getString(R.string.easter_but_key));
    	easter_lwp_but.setOnPreferenceClickListener(this);
    	
    	
	}
	
	protected void onResume() {
    	super.onResume();
    	if(prefs != null)
    		prefs.getSharedPrefs().registerOnSharedPreferenceChangeListener(this);
	}

	protected void onPause()
	{
		super.onPause();
		if(prefs != null)
			prefs.getSharedPrefs().unregisterOnSharedPreferenceChangeListener(this);
		
	 	if(this.prefsChanged)
	 		HoverPonyLW.prefsChanged = true;
	}
	
	@Override
	protected void onDestroy() {
	 	super.onDestroy();

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		// TODO Auto-generated method stub
		this.prefsChanged = true;
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_VIEW);
		String intentData = "";
		
		switch (preference.getOrder()) {
		case 0:
			intentData = "my_pony_lw";
			break;
		case 1:
			intentData = "saint_patrick_pony_lw";
			break;
		case 2:
			intentData = "easter_pony_lw";
			break;
		default:
			break;
		}
		
		intent.setData(Uri.parse( uriPkg + intentData));
		
		try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
        	Toast.makeText(getBaseContext(), "Cannot find Google Play",
                    Toast.LENGTH_LONG).show();
            return false;
        } 
		return true;
	}
}