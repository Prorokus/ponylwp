/*
 * Copyright 2010 Elijah Cornell
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 */

package com.eightbitmage.gdxlw;

import android.os.Bundle;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;
import com.badlogic.gdx.backends.android.livewallpaper.AndroidApplication;
import com.badlogic.gdx.backends.android.livewallpaper.AndroidGraphics;
import com.badlogic.gdx.backends.android.livewallpaper.surfaceview.DefaultGLSurfaceView;
import com.badlogic.gdx.utils.ObjectMap;

public abstract class LibdgxWallpaperService extends WallpaperService {

	private final String TAG = "GDX-LW-Service";
	
	
	protected AndroidApplication app;
	protected ObjectMap<SurfaceHolder, AndroidGraphics> graphicsMap;
	protected LibdgxWallpaperListener wallpaperListener;
	
	private boolean DEBUG = false;

	public LibdgxWallpaperService() {
		super();
	}

	@Override
	public void onCreate() {
		if (DEBUG)
			Log.d(TAG, " > LibdgxWallpaperService - onCreate()");
		super.onCreate();
		graphicsMap = new ObjectMap<SurfaceHolder, AndroidGraphics>();
	}

	@Override
	abstract public Engine onCreateEngine();

	@Override
	public void onDestroy() {
		if (DEBUG)
			Log.d(TAG, " > LibdgxWallpaperService - onDestroy()");
		app.onDestroy();
		super.onDestroy();
	}

	// ~~~~~~~~ MyEngine ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	public abstract class LibdgxWallpaperEngine extends Engine {

		protected DefaultGLSurfaceView view;

		abstract protected void initialize(AndroidApplication app);

		public LibdgxWallpaperEngine(
				final LibdgxWallpaperService libdgxWallpaperService) {
			super();

			if (DEBUG)
				Log.d(TAG, " > MyEngine() " + hashCode());

			if (app == null)
			{
				app = new AndroidApplication(libdgxWallpaperService, this);
				initialize(app);

				graphicsMap.put(getSurfaceHolder(), (AndroidGraphics) app.getGraphics());
			}
			else
			{
				app.setEngine(this);
				AndroidGraphics graphics = app.createGraphics();
				app.setGraphics(graphics);
				graphicsMap.put(getSurfaceHolder(), graphics);
			}
			
			view =  graphicsMap.get(getSurfaceHolder()).getView();

		}
		
		public void setWallpaperListener(LibdgxWallpaperListener wallpaperListener) {
			LibdgxWallpaperService.this.wallpaperListener = wallpaperListener ;
		}

		@Override
		public Bundle onCommand(final String pAction, final int pX,
				final int pY, final int pZ, final Bundle pExtras,
				final boolean pResultRequested) {

			if (DEBUG)
				Log.d(TAG, " > onCommand(" + pAction + " " + pX + " " + pY
						+ " " + pZ + " " + pExtras + " " + pResultRequested
						+ ")");

			/*
			if (pAction.equals(WallpaperManager.COMMAND_TAP)) {
				((AndroidInput) app.getInput()).onTouch(view, MotionEvent.ACTION_DOWN); //onTap(pX, pY);
			} else if (pAction.equals(WallpaperManager.COMMAND_DROP)) {
				((AndroidInput) app.getInput()).onTouch(view, MotionEvent.ACTION_UP);//onDrop(pX, pY);
			}
			*/
			
			return super.onCommand(pAction, pX, pY, pZ, pExtras,
					pResultRequested);
		}

		/*
		@Override
		public void onCreate(final SurfaceHolder surfaceHolder) {

			if (DEBUG)
				Log.d(TAG, " > onCreate() " + hashCode());

			super.onCreate(surfaceHolder);

			/*
			//skynet. ������� ������ �������.
			if (previousEngine != null) {
				((DefaultGLSurfaceView) previousEngine.view).onPause(true);
			}
			
			previousEngine = this;
			 *//*
			wallpaperListener.setIsPreview(this.isPreview());
		}
*/
		@Override
		public void onDestroy() {

			if (DEBUG)
				Log.d(TAG, " > onDestroy() " + hashCode());

			//((GLBaseSurfaceView) view).onDetachedFromWindow();
			//app.onDestroy();
			
			view =  graphicsMap.get(getSurfaceHolder()).getView();
			view.onDestoy();
			graphicsMap.remove(getSurfaceHolder());
			super.onDestroy();

		}

		public void onPause() {

			if (DEBUG)
				Log.d(TAG, " > onPause() " + hashCode());

			app.onPause();
			//((DefaultGLSurfaceView) view).onPause(false);

			view =  graphicsMap.get(getSurfaceHolder()).getView();
			view.onPause();
		}

		public void onResume() {

			if (DEBUG)
				Log.d(TAG, " > onResume() " + hashCode());

			AndroidGraphics curr_graphics = graphicsMap.get(getSurfaceHolder());
			if (app.getGraphics() != curr_graphics)
				app.setGraphics(curr_graphics);
			
			app.onResume();
			//((DefaultGLSurfaceView) view).onResume();
			
			view = curr_graphics.getView();
			for (AndroidGraphics graph:graphicsMap.values())
			{
				DefaultGLSurfaceView tmp_view = graph.getView();
				if (view != tmp_view)
					tmp_view.onDestoy();
			}
			view.onResume();
		}

		@Override
		public void onSurfaceChanged(final SurfaceHolder holder,
				final int format, final int width, final int height) {

			if (DEBUG)
				Log.d(TAG, " > onSurfaceChanged() " + isPreview() + " "
						+ hashCode());

			super.onSurfaceChanged(holder, format, width, height);
		}

		@Override
		public void onSurfaceCreated(final SurfaceHolder holder) {

			if (DEBUG)
				Log.d(TAG, " > onSurfaceCreated() " + hashCode());

			super.onSurfaceCreated(holder);
		}

		@Override
		public void onSurfaceDestroyed(final SurfaceHolder holder) {

			if (DEBUG)
				Log.d(TAG, " > onSurfaceDestroyed() " + hashCode());

			super.onSurfaceDestroyed(holder);
		}

		@Override
		public void onVisibilityChanged(final boolean visible) {

			if (DEBUG)
				Log.d(TAG, " > onVisibilityChanged(" + visible + ") "
						+ hashCode());

			if (visible) {
				onResume();
			} else {
				onPause();
			}

			super.onVisibilityChanged(visible);

		}

		@Override
		public void onOffsetsChanged(float xOffset, float yOffset,
				float xOffsetStep, float yOffsetStep, int xPixelOffset,
				int yPixelOffset) {

			if (DEBUG)
				Log.d(TAG, " > onVisibilityChanged(" + xOffset + " " + yOffset
						+ " " + xOffsetStep + " " + yOffsetStep + " "
						+ xPixelOffset + " " + yPixelOffset + ") " + hashCode());

			wallpaperListener.offsetChange(xOffset, yOffset, xOffsetStep,
					yOffsetStep, xPixelOffset, yPixelOffset);

			super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep,
					xPixelOffset, yPixelOffset);

		}
		
	}

}