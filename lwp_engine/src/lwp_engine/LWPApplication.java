package lwp_engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.livewallpaper.AndroidPreferences;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.eightbitmage.gdxlw.LibdgxWallpaperApp;

public abstract class LWPApplication extends LibdgxWallpaperApp{
	
	public static Stage root;
	public static WallpaperScreen wallScreen;
		
	public static boolean inPortraitMode = false;
	public static boolean prefsChanged = false;
	public static boolean enableParallax = false;
	
	public static final int VIRTUAL_WALL_WIDTH = 480;
	public static final int VIRTUAL_WALL_HEIGHT = 800;
	public static final int VIRTUAL_WALL_FULL_WIDTH= 960;
	
	public final String MY_PONY_PREFS = "PACKAGE_NAME + _preferences";
	
	protected boolean wallNotMovable = false;
	protected int selectedWall = 0;
	protected int prevSelectedWall = -1;
		
	protected AndroidPreferences prefs;
	protected float xOffset;
	
    protected ParallaxCamera camera;
	

	@Override
	public void create()
	{
		//remove in release version. ���������������
		if(!Gdx.app.getClass().getName().equals("com.badlogic.gdx.backends.jogl.JoglApplication"))
		{
			prefs = (AndroidPreferences) Gdx.app.getPreferences(getPrefsName());
			readAllPrefs();
		}
		
		if (getRoot() == null)
		{
			root = new Stage(VIRTUAL_WALL_WIDTH, VIRTUAL_WALL_HEIGHT, true);
			camera = new ParallaxCamera(VIRTUAL_WALL_WIDTH, VIRTUAL_WALL_HEIGHT);
			getRoot().setCamera(camera);
			
			loadWallScreen(true);
		}
		else
		{
			camera = (ParallaxCamera) getRoot().getCamera();
		}
	}

	protected String getPrefsName()
	{
		return MY_PONY_PREFS;
	}
	
	@Override
	public void dispose() 
	{
		if (wallScreen != null)
		{
			wallScreen.destroy();
			wallScreen = null;
		}
		root = null;
	}

	@Override
	public void pause() {
	}

	@Override
	public void render()
	{
		if(wallScreen != null)
			wallScreen.draw(Gdx.graphics.getDeltaTime());
	}

	private void resizeFixedViewport(int width, int height)
	{
		float shiftX = 0;
		float shiftY = 0;
		
		if (width < VIRTUAL_WALL_FULL_WIDTH)
		{
			if (width < height)
			{
				getRoot().setViewport(VIRTUAL_WALL_WIDTH, VIRTUAL_WALL_HEIGHT, false);
				inPortraitMode = true;
			}
			else
			{
				getRoot().setViewport(VIRTUAL_WALL_HEIGHT, VIRTUAL_WALL_WIDTH, false);
				shiftY = (VIRTUAL_WALL_HEIGHT - camera.viewportHeight) * 0.5f;
				inPortraitMode = false;
			}
		}
		else
		{
			getRoot().setViewport(width, height, true);
			camera.zoom = (float)VIRTUAL_WALL_FULL_WIDTH / (float)width;
			inPortraitMode = false;
		}
		
		camera.position.x = getXOffset() - shiftX;
		camera.position.y = camera.viewportHeight * 0.5f + shiftY;
		
		camera.screenRotated = true;
	}
	
	private void checkCamPosition()
	{
		if(wallNotMovable)
			camera.position.x = VIRTUAL_WALL_FULL_WIDTH * camera.zoom * 0.5f;
	}
	
	abstract protected void onResumeWallpaper();
	
	@Override
	public void resize(int width, int height) {
		
		resizeFixedViewport(width, height);
		
		checkCamPosition();
	}

	@Override
	public void resume()
	{
		ImageCache.getManager().finishLoading();
		
		getRoot().setCamera(camera);
		
		if (prefsChanged)
		{
			readAllPrefs();

			onResumeWallpaper();
			
			checkCamPosition();
			
			prefsChanged = false;
		}		
	}

	@Override
	public void offsetChange(float xOffset, float yOffset, float xOffsetStep,
			float yOffsetStep, int xPixelOffset, int yPixelOffset) {

		this.xOffset = xOffset;
		
		if(!wallNotMovable && camera != null)
			camera.position.x = getXOffset();
		
	}

	private float getXOffset()
	{
		final float screen_width = camera.viewportWidth;
		if (screen_width < VIRTUAL_WALL_FULL_WIDTH)
			return xOffset * (VIRTUAL_WALL_FULL_WIDTH - screen_width) + screen_width * 0.5f;
		else
		{
			return screen_width * camera.zoom * 0.5f;
		}
	}
	
	abstract protected void readAllPrefs();
	
	abstract protected void loadWallScreen(boolean ... params);

	public static Stage getRoot() {
		return root;
	}

	public class ParallaxCamera extends OrthographicCamera
	{
    	private Matrix4 parallaxNormal = new Matrix4();
    	private Matrix4 parallaxCombined = new Matrix4();
    	private float lastCameraPosX = -1;
    	private float lastParallaxX = 1;
    	public boolean screenRotated = false;
    
    	public ParallaxCamera (float viewportWidth, float viewportHeight) {
            super(viewportWidth, viewportHeight);
    	}

    	public Matrix4 calculateParallaxMatrix (float parallaxX)
    	{
           // update();
    		/*if(lastParallaxX == parallaxX && (lastCameraPosX == position.x) && !screenRotated)
    		{
    			 if (parallaxX != 1.0f)
    				 return parallaxCombined;
    			 else
    				 return parallaxNormal;
    		}*/
            
            if ((lastCameraPosX != position.x) || screenRotated)
            {
            	screenRotated = false;
            	lastCameraPosX = position.x;
            	lastParallaxX = 1;
            	parallaxNormal.set(projection);
            	parallaxNormal.val[12] = -1 * parallaxNormal.val[0] * position.x;
            	parallaxNormal.val[13] = -1 * parallaxNormal.val[5] * position.y;
            }
            
            if (parallaxX <= 0.99f || parallaxX >= 1.01f)
            {
            	if (lastParallaxX != parallaxX)
            	{
            		lastParallaxX = parallaxX;
            		parallaxCombined.set(parallaxNormal);
            		parallaxCombined.val[12] = parallaxCombined.val[12] * parallaxX;
            	}
            	
            	return parallaxCombined;
            }
            
            lastParallaxX = parallaxX;
            return parallaxNormal;
    	}
    	
    	public boolean recalculateParallax(float parallaxX)
    	{
    		return (lastParallaxX != parallaxX || (lastCameraPosX != position.x) || screenRotated) ? true : false;
    	}
    }
}
