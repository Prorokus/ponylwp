package lwp_engine;

import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class WallpaperScreen {

	public String name;
	
	public boolean sparklesVisible = false;
	public boolean showParticles = true;
	
	public WallpaperScreen(String name)
	{
		this.name = name;
		
		ImageCache.create(name, "main");
		
		initialize();
	}
	
	abstract public void initialize();
	
	abstract public void onConfigChanged(); 

	public void draw(float delta)
	{
		if (ImageCache.getManager().update())
		{
			LWPApplication.getRoot().draw();
			LWPApplication.getRoot().act(delta);
		}
	}
	
	public void destroy()
	{
		if (LWPApplication.getRoot() != null)
		{
			List<Actor> objects = LWPApplication.getRoot().getActors();
			for(int i = 0; i < objects.size(); i++)
			{
				GameObject obj = (GameObject) objects.get(i);
				obj.clear();
			}
			
			LWPApplication.getRoot().clear();
		}
		
		if(ImageCache.manager != null)
		{
			ImageCache.getManager().finishLoading();
			ImageCache.getManager().clear();
			ImageCache.manager = null;
		}
	}
}
