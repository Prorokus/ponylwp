package lwp_engine;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class MoveTo extends AnimationAction {

	private GameObject target;
	
	private float toX;
	private float toY;
	
	private float fromX;
	private float fromY;

	private float deltaX;
	private float deltaY;
	
	private float counter_time;
	
	private float speed;
	private float startTime;
	
	private Boolean stop_anim;
	private Boolean loop;
	
	private float time_past;
	private float dist_x;
	private float dist_y;	
	private float interpolatedTime;
	
	public MoveTo(float from_x, float from_y, float to_x, float to_y, float duration, float start_time, Boolean loop)
	{
		this(to_x, to_y, duration, 0.0f, true, start_time, loop);
		this.fromX = from_x;
		this.fromY = from_y;
	}
	
	public MoveTo(float to_x, float to_y, float duration, float start_time, Boolean loop)
	{
		this(to_x, to_y, duration, 0.0f, true, start_time, loop);
	}
	
	public MoveTo(float to_x, float to_y, float duration, float speed, float start_time, Boolean loop)
	{
		this(to_x,to_y, duration, speed, true, start_time, loop);
	}
		
	public MoveTo(float to_x, float to_y, float duration, float speed, Boolean stop, float start_time,  Boolean loop)
	{
		this.toX = to_x;
		this.toY = to_y;
		
		this.speed = speed;
		
		this.startTime = start_time;
		this.duration = duration;
		this.invDuration = 1/duration;
		
		this.stop_anim = stop;
		this.loop = loop;
	}

	
	@Override
	public void setTarget(Actor actor) {
		// TODO Auto-generated method stub
		this.target = (GameObject) actor;

		if (this.fromX == 0.0f && this.fromY == 0.0f)
		{
			this.fromX = this.target.x;
			this.fromY = this.target.y;
		}
		
		this.deltaX = this.toX - fromX;
		this.deltaY = this.toY - fromY;
		
		if (speed != 0)
		{
			float spd_x = Math.abs(this.deltaX)/this.speed;
			float spd_y = Math.abs(this.deltaY)/this.speed;
			duration = Math.max(spd_x, spd_y);
		}
		
		this.counter_time = 0;
		this.done = false;
		
	}

	private void RestartMoveTo()
	{
		this.target.x = this.fromX;
		this.target.y = this.fromY;
		this.counter_time = this.startTime;
	}
	
	@Override
	public void act(float delta) {
		
		counter_time += delta;
		
		if (this.startTime <= counter_time)
		{
			if ((counter_time - this.startTime) >= duration && stop_anim)
			{
				done = true;
				target.x = toX;
				target.y = toY;
				
				if (this.loop)
				{
					done = false;
					RestartMoveTo();
				}
			}
			else
			{
				if (interpolator == null) 
				{
					this.time_past = (counter_time - this.startTime)/duration;
				} 
				else 
				{
					this.interpolatedTime = interpolator.getInterpolation(counter_time / duration) * duration;
					this.time_past = interpolatedTime * invDuration;
				}				

				this.dist_x = deltaX  * time_past;
				this.dist_y = deltaY * time_past;
			
				target.x = fromX + dist_x;
				target.y = fromY + dist_y;
			}
		}
	}
	
	@Override
	public void finish () {
		super.finish();
		
	}

	@Override
	public Action copy() {
		// TODO Auto-generated method stub
		MoveTo moveTo = new MoveTo(this.toX, this.toY, duration, this.startTime, this.loop);
		if (interpolator != null) moveTo.setInterpolator(interpolator.copy());
		return moveTo;
	}
}