package lwp_engine;

//import lw_engine.my_pony_lw.MyPonyLW.ParallaxCamera;

import java.util.ArrayList;
import java.util.List;

import lwp_engine.GameObjectAnimation.AnimKey;
import lwp_engine.LWPApplication.ParallaxCamera;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.ObjectMap;

public class GameObject extends Group{
	
	public TextureRegion skinRegion;
	public String skinName;
	public String objectSubFolder = ImageCache.subFolderName;
	
	private ObjectMap <String, ParticleEffect> particleStack;
	private boolean effectsInheritPos;
	private boolean enableBlending = true;
	
	private float parallaxX = 1.0f;
	//private float parallaxY = 1.0f;
	
	public Animation animCache;
	public boolean haveAnimation = false;
	public boolean pongAnimation = false;
	public boolean animPaused = false;
	public float pauseAnim = 0;
	public float animWaitTimer = 0;
	private float stateTime = 0;
	
	private ParallaxCamera camera = (ParallaxCamera) LWPApplication.getRoot().getCamera();
	
	private boolean useParallax = false;
	private float parallaxCameraOffsetX;
	
	private boolean needChangePos = false;
	private boolean changeParticlesParallaxPos = true;
	private boolean filter = false;
	
	private AnimKey animParam;
	
	public GameObject(String name, boolean addToRoot, float parallaxX, float parallaxY)
	{
		super(name);
		
		this.parallaxX = parallaxX;
		if (addToRoot)
			LWPApplication.getRoot().addActor(this);
		
		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;
	}
	
	public GameObject(boolean addToRoot, float parallaxX, float parallaxY)
	{
		super();

		this.parallaxX = parallaxX;
		if (addToRoot)
			LWPApplication.getRoot().addActor(this);
		
		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;
	}
	
	public GameObject()
	{
		super();

		LWPApplication.getRoot().addActor(this);
		
		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;
	
	}
	
	public GameObject(String name)
	{
		this(name,name, true);
	}

	public GameObject(String name, boolean addToRoot)
	{
		this(name,name, addToRoot);
	}
	
	public GameObject(String name, boolean addToRoot, boolean enableBlending, float parallaxX, float parallaxY)
	{
		this(name,name, addToRoot);
		this.enableBlending = enableBlending;
		this.parallaxX = parallaxX;
		//this.parallaxY = parallaxY;
		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;
	}
	
	public GameObject(String name, String skinName, boolean addToRoot, boolean enableBlending, float parallaxX, float parallaxY)
	{
		this(name, skinName, addToRoot);
		this.enableBlending = enableBlending;
		this.parallaxX = parallaxX;
		//this.parallaxY = parallaxY;
		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;
	}
	
	public GameObject(String name, boolean addToRoot, boolean enableBlending)
	{
		this(name,name, addToRoot);
		this.enableBlending = enableBlending;
	}

	public GameObject(String name, String skinName, boolean addToRoot)
	{
		super(name);
		
		this.skinName = skinName;
		
		if (addToRoot)
			LWPApplication.getRoot().addActor(this);
		
	}
	
	public void setOnCreateFilter(Boolean filter)
	{
		this.filter = filter;
	}
	
	public void setSkin () {
		if (!visible)
			return;		
		
		this.skinRegion = ImageCache.getManager().get("data/atlas/" + ImageCache.folderName + "/" + this.objectSubFolder + "/pack", TextureAtlas.class).findRegion(skinName);
		if (this.skinRegion == null) return;
		
		if (filter)
			this.skinRegion.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		if (width == 0)
			width = this.skinRegion.getRegionWidth();
		
		if (height == 0)
			height = this.skinRegion.getRegionHeight();
		
	}
	
	public void loadAnimationFrames ()
	{
		this.animCache = ImageCache.animations.get(this.animParam.anim_tex_name);
		
		if(this.animCache == null)
		{
			List<TextureRegion> anim_tex_regions = new ArrayList<TextureRegion>();
			TextureRegion currFrame;
				
			while (true) {
				currFrame = ImageCache.getManager().get("data/atlas/" + ImageCache.folderName + "/" + this.objectSubFolder + "/pack", TextureAtlas.class).findRegion(this.animParam.anim_tex_name + "-f0" + this.animParam.framesCount); 
				if (currFrame != null)
					anim_tex_regions.add(currFrame);
				else
					break;
				
				this.animParam.framesCount ++;
			}
			this.animParam.framesCount --;
			int pong_frames = this.animParam.framesCount;
			
			if (this.pongAnimation)
			{
				this.animParam.framesCount = this.animParam.framesCount + this.animParam.framesCount - 1;
				
				while (pong_frames > 2)
				{
					pong_frames --;
					currFrame = ImageCache.getManager().get("data/atlas/" + ImageCache.folderName + "/" + this.objectSubFolder + "/pack", TextureAtlas.class).findRegion(this.animParam.anim_tex_name + "-f0" + pong_frames); 
					anim_tex_regions.add(currFrame);
				}
				
			}
			
			this.animCache = new Animation(this.animParam.duration, anim_tex_regions);
			ImageCache.animations.put(this.animParam.anim_tex_name, this.animCache);
		}
		
		TextureRegion frame = this.animCache.getKeyFrame(0, false);
		this.skinRegion = frame;
		
		if (this.width == 0)
			this.width = frame.getRegionWidth();
		
		if (this.height == 0)
			this.height = frame.getRegionHeight();	
	}
	
	public void AddAnim(AnimKey animParam)
	{
		this.haveAnimation = true;
		this.animParam = animParam;
	}
	
	private void loadParticleData (String s)
	{
		ParticleEffect effect = particleStack.get(s);
		
		effect.load(Gdx.files.internal("data/atlas/" + ImageCache.folderName + "/particles/" + s + ".txt"),
				ImageCache.getManager().get("data/atlas/" + ImageCache.folderName + "/" + this.objectSubFolder + "/pack", TextureAtlas.class)
				);
		
		effect.setPosition(this.getPositionX() + this.getOriginX(), this.getPositionY() + this.getOriginY());
	}
	
	public void createParticle (String name, Boolean inherit_pos)
	{
		if (this.particleStack == null)
		{
			this.particleStack = new ObjectMap <String, ParticleEffect>();
			this.effectsInheritPos = inherit_pos;
		}
		
		ParticleEffect effect = new ParticleEffect();
		this.particleStack.put(name, effect);
	}
	
	public void clearParticles()
	{
		if (particleStack != null && particleStack.size > 0)
		{
			for(ParticleEffect p : particleStack.values())
				p.dispose();
			particleStack.clear();
		}
	}
	
	public void clearParticlesAndRemove()
	{
		clearParticles();
		//this.markToRemove(true);
	}
	
	public void setOriginX(float x)
	{
		this.originX = x;
	}
	
	public void setOriginY(float y)
	{
		this.originY = y;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}
	
	public void setHeight(float height)
	{
		this.height = height;
	}
	
	public void setScaleX(float scaleX)
	{
		this.scaleX = scaleX;
	}
	
	public void setScaleY(float scaleY)
	{
		this.scaleY = scaleY;
	}
	
	public float getOriginX()
	{
		return this.originX;
	}
	
	public float getOriginY()
	{
		return this.originY;
	}
	
	public float getPositionX()
	{
		return this.x;
	}
	
	public float getPositionY()
	{
		return this.y;
	}
	
	public void setPositionXY(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	private void drawSkin (SpriteBatch batch, float parentAlpha)
	{
		if (!LWPApplication.enableParallax)
		{
			batch.setColor(this.color);
			batch.draw(this.skinRegion, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
		}
		else
		{
			this.parallaxCameraOffsetX = x;
			
			if (this.useParallax)
			{
				if(this.camera.recalculateParallax(parallaxX))
					batch.setProjectionMatrix(this.camera.calculateParallaxMatrix(parallaxX));
				
				if (LWPApplication.inPortraitMode)
					this.parallaxCameraOffsetX -= this.camera.viewportWidth * (1 - parallaxX);
				else
					this.parallaxCameraOffsetX -= this.camera.viewportHeight * (1 - parallaxX);
			}
			else
			{
				if(this.camera.recalculateParallax(1))
					batch.setProjectionMatrix(this.camera.calculateParallaxMatrix(1.0f));
			}
					
			batch.setColor(this.color);
			batch.draw(this.skinRegion, this.parallaxCameraOffsetX, y, originX, originY, width, height, scaleX, scaleY, rotation);
			
			
		}
		
	}
	
	private void calcNextFrame()
	{
		this.stateTime += Gdx.graphics.getDeltaTime();
		
		if (this.animPaused)
		{
			if (this.skinRegion == null)
				this.skinRegion = animCache.getKeyFrame(0, false);
			else
				return;
		}
		
		if (this.pauseAnim <= 0)
		{
			this.skinRegion = animCache.getKeyFrame(this.stateTime, true);
		}
		
		else if (this.pauseAnim > 0 && this.stateTime > this.animParam.framesCount * animCache.frameDuration)
		{
			this.animWaitTimer += Gdx.graphics.getDeltaTime();
			if (this.animWaitTimer >= this.pauseAnim)
			{
				this.stateTime = 0;
				this.animWaitTimer = 0;
			}
		}
		
		else
		{
			this.skinRegion = animCache.getKeyFrame(this.stateTime, true);
		}
	}
	
	public void draw(SpriteBatch batch, float parentAlpha)
	{
		if (!visible)
			return;
		
		if (this.skinRegion != null)
		{
			if (this.haveAnimation)
			{
				calcNextFrame();
			}
			
			if (!this.enableBlending)
			{
				batch.disableBlending();
				this.drawSkin(batch, parentAlpha);
				batch.enableBlending();
			} 
			
			else
			{
				this.drawSkin(batch, parentAlpha);
			}
		}
		else if (this.skinName != null && !this.haveAnimation) 
		{
			setSkin();
		} 
		else if (this.haveAnimation)
		{
			loadAnimationFrames();
		}
		
		if (this.children.size() > 0)
			super.draw(batch, parentAlpha);
		
		if (particleStack != null && particleStack.size > 0)
			for (String s : particleStack.keys())
			{
				if (!LWPApplication.enableParallax)
				{
					if (this.effectsInheritPos )
						particleStack.get(s).setPosition(this.getPositionX() + this.getOriginX(), this.getPositionY() + this.getOriginY());
					
					if ( this.useParallax && this.needChangePos)
					{
						this.needChangePos = false;
						this.changeParticlesParallaxPos = true;
						particleStack.get(s).setPosition(this.getPositionX() + this.getOriginX(), this.getPositionY() + this.getOriginY());
					}
					
					if (particleStack.get(s).getEmitters().size == 0)
						loadParticleData(s);
					
					particleStack.get(s).draw(batch, Gdx.graphics.getDeltaTime());
				}
				else
				{
					//------------------PARTICLE DRAW--------------------------------------
					if (particleStack.get(s).getEmitters().size == 0)
						loadParticleData(s);
					
					if (this.effectsInheritPos )
					{
						if (! this.useParallax && !LWPApplication.enableParallax )
							particleStack.get(s).setPosition(this.getPositionX() + this.getOriginX(), this.getPositionY() + this.getOriginY());
						else
							this.changeParticlesParallaxPos = true;
					}
									
					if (this.useParallax && this.changeParticlesParallaxPos)
					{
						this.changeParticlesParallaxPos = false;
						this.needChangePos = true;

						this.parallaxCameraOffsetX = this.getPositionX();
						
						if (LWPApplication.inPortraitMode)
							this.parallaxCameraOffsetX -= this.camera.viewportWidth * (1 - parallaxX);
						else
							this.parallaxCameraOffsetX -= this.camera.viewportHeight * (1 - parallaxX);
						
						particleStack.get(s).setPosition(this.parallaxCameraOffsetX + this.getOriginX(), this.getPositionY() + this.getOriginY());
					}
									
					if (this.useParallax)
					{
						if(this.camera.recalculateParallax(parallaxX))
							batch.setProjectionMatrix(this.camera.calculateParallaxMatrix(parallaxX));
					}
					else
					{
						if(this.camera.recalculateParallax(1))
							batch.setProjectionMatrix(this.camera.calculateParallaxMatrix(1));
					}
					particleStack.get(s).draw(batch, Gdx.graphics.getDeltaTime());
					//---------------------------------------------------------------------
						
				}
			}		
	}
	
	public void clearSkin()
	{	
		if(this.skinRegion != null)
		{
			this.skinRegion.getTexture().dispose();
			this.skinRegion = null;
		}
		
	}
	
	public void clearAnimCache()
	{
		if(animParam != null)
			ImageCache.animations.remove(this.animParam.anim_tex_name);
	}
	
	public void clear()
	{
		List<Actor> objects = this.getActors();
		for(int i = 0; i < objects.size(); i++)
		{
			GameObject obj = (GameObject) objects.get(i);
			obj.clear();
		}
		
		//TODO:�� ��������. ������ ��� ������. ������������, ������� ����� ������� �����.
		//this.clearParticles();
		
		this.clearAnimCache();

		this.clearSkin();
	}
}
