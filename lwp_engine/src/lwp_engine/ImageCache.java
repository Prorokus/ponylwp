package lwp_engine;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.ObjectMap;

public class ImageCache {
   public static AssetManager manager;
   
   public static String folderName;
   public static String subFolderName;
   public static ObjectMap<String, Animation> animations;
	
    public static void create (String name, String subfolder) {
    	if(manager == null)
    	{
    		folderName = name;
    		subFolderName = subfolder;
    		
    		manager = new AssetManager();
    		//manager.getLogger().setLevel(3);
	    	manager.setLoader(TextureAtlas.class, new TextureAtlasLoader(new InternalFileHandleResolver()));
	    	
	    	load(name,subfolder);
	    	
	    	//Texture.setAssetManager(manager);
	    	
	    	animations = new ObjectMap<String, Animation>();
    	}
    }

    public static AssetManager getManager() {
    	return manager;
    }
    
    public static void load(String name, String subfolder) {
    	manager.load("data/atlas/" + name + "/" + subfolder + "/pack", TextureAtlas.class);
    }
    
    public static void unload(String name, String subfolder) {
    	final String fileName = "data/atlas/" + name + "/" + subfolder + "/pack";
		if (ImageCache.manager.isLoaded(fileName))
			manager.unload(fileName);
    }    
}
