package lwp_engine;

public class GameObjectAnimation {
	public static class AnimKey
	{
		public String anim_tex_name;
		
		public float anim_width;
		public float anim_height;
		public int framesCount = 1;
		
		public float duration;
		
		public AnimKey(String name, float duration)
		{
			this.anim_tex_name = name;
			this.duration = duration;
		}
		
		public AnimKey(String name, float duration, float anim_width, float anim_height)
		{
			this.anim_tex_name = name;
			this.anim_width = anim_width;
			this.anim_height = anim_height;
			this.duration = duration;
		}
	}
}
