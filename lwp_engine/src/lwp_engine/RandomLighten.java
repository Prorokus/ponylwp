package lwp_engine;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class RandomLighten extends AnimationAction {
	
	GameObject target;
	
	protected float toAlpha = 0;
	protected float fromAlpha = 0;
	
	protected float randomTo = 0;
	protected float randomFrom = 0;
	
	protected float deltaAlpha = 0;
	
	protected float endAlpha = 0;
	protected float speed = 0;
	protected float cycleDuration = 0;
	protected float cycleDurationLeft = 0;
	
	public RandomLighten(float from, float to, float endAlpha, float speed, float duration)
	{
		this.fromAlpha = from;
		this.toAlpha = to;
		
		this.randomTo = MathUtils.random(from, to);
		this.duration = speed;
		this.invDuration = 1 / speed;
		
		this.endAlpha = endAlpha;
		this.speed = speed;
		this.cycleDuration = duration;
		this.cycleDurationLeft = duration;
	}

	@Override
	public void act(float delta) {
		// TODO Auto-generated method stub
		this.cycleDurationLeft -= delta;
		float alpha = createInterpolatedAlpha(delta);
		
		if (done && this.cycleDurationLeft <= 0) 
		{
			if (this.endAlpha != -1)
			{
				target.color.a = this.endAlpha;
			}
			else
			{
				target.color.a = this.randomTo;
			}
		} 
		
		else if (done && this.cycleDurationLeft > 0)
		{
			this.taken = 0;
			this.done = false;
			
			this.randomTo = MathUtils.random(this.fromAlpha, this.toAlpha);
			this.duration = this.speed;
			this.invDuration = 1 / this.speed;			
			
			this.randomFrom = this.target.color.a;
			this.deltaAlpha = this.randomTo - this.randomFrom;					
		}
		
		else 
		{
			float val = randomFrom + deltaAlpha * alpha;
			target.color.a = Math.min(Math.max(val, 0.0f), 1.0f);
		}
	}

	@Override
	public void setTarget(Actor arg0) {
		// TODO Auto-generated method stub
		this.target = (GameObject)arg0;
		this.randomFrom = this.target.color.a;
		this.deltaAlpha = this.randomTo - this.randomFrom;		
	}
	
	@Override
	public void finish () {
		super.finish();
		
	}
	
	@Override
	public Action copy() {
		// TODO Auto-generated method stub
		RandomLighten randLight = new RandomLighten(this.fromAlpha, this.toAlpha, this.endAlpha, this.speed, this.cycleDuration);
		if (interpolator != null) randLight.setInterpolator(interpolator.copy());
		return randLight;
	}

}
