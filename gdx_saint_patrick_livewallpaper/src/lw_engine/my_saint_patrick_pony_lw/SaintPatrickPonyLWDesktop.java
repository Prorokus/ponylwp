package lw_engine.my_saint_patrick_pony_lw;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.jogl.JoglApplication;

public class SaintPatrickPonyLWDesktop {
	public static void main(String[] argv)
	{
		JoglApplication app = new JoglApplication((ApplicationListener) new MyPatrickPonyLW(), "My Patrick Pony LW App", 960, 854,  false);
	}
}
