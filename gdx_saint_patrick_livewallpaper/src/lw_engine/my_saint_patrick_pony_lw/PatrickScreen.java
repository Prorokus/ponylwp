package lw_engine.my_saint_patrick_pony_lw;

import java.util.ArrayList;
import java.util.List;

import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.WallpaperScreen;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.FadeOut;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

public class PatrickScreen extends WallpaperScreen {
	
	public List<GameObject> ponies;

	public PatrickScreen(String name) {
		super(name);
	}

	@Override
	public void initialize() {
		final GameObject bg = new GameObject("background", true, false);
		bg.setPositionXY(0,0);

		//----------------------CLOUD_LIGHT0-------------------------------------------
		final GameObject cloud_light0_0 = new GameObject("cloudLight0", true, true);
		final GameObject cloud_light0_1 = new GameObject("cloudLight0", true, true);
		cloud_light0_0.setWidth(224);
		cloud_light0_0.setHeight(39);
		cloud_light0_1.setWidth(224);
		cloud_light0_1.setHeight(39);
		
		cloud_light0_0.setPositionXY(719,481);
		cloud_light0_1.setPositionXY(72,481);

		MoveTo cloud_light0_0_moveTo0 = new MoveTo (960, 481, 37, 0, false);
		MoveTo cloud_light0_0_moveTo1 = new MoveTo (-224, 481, 960, 481, 180, 37, true);
		
		MoveTo cloud_light0_1_moveTo0 = new MoveTo (960, 481, 127, 0, false);
		MoveTo cloud_light0_1_moveTo1 = new MoveTo (-224, 481, 960, 481, 180, 127, true);

		cloud_light0_0.action(cloud_light0_0_moveTo0);
		cloud_light0_0.action(cloud_light0_0_moveTo1);
		cloud_light0_1.action(cloud_light0_1_moveTo0);
		cloud_light0_1.action(cloud_light0_1_moveTo1);
		
		//----------------------CLOUD_LIGHT1-------------------------------------------
		final GameObject cloud_light1_0 = new GameObject("cloudLight0", true, true);
		final GameObject cloud_light1_1 = new GameObject("cloudLight0", true, true);
		cloud_light1_0.setWidth(360);
		cloud_light1_0.setHeight(62);
		cloud_light1_1.setWidth(360);
		cloud_light1_1.setHeight(62);
		
		cloud_light1_0.setPositionXY(547,713);
		cloud_light1_1.setPositionXY(-110,713);

		MoveTo cloud_light1_0_moveTo0 = new MoveTo (960, 713, 56, 0, false);
		MoveTo cloud_light1_0_moveTo1 = new MoveTo (-360, 713, 960, 713, 180, 56, true);
		
		MoveTo cloud_light1_1_moveTo0 = new MoveTo (960, 713, 146, 0, false);
		MoveTo cloud_light1_1_moveTo1 = new MoveTo (-360, 713, 960, 713, 180, 146, true);

		cloud_light1_0.action(cloud_light1_0_moveTo0);
		cloud_light1_0.action(cloud_light1_0_moveTo1);
		cloud_light1_1.action(cloud_light1_1_moveTo0);
		cloud_light1_1.action(cloud_light1_1_moveTo1);
		
		//----------------------CLOUD_LIGHT2-------------------------------------------
		final GameObject cloud_light2_0 = new GameObject("cloudLight0", true, true);
		final GameObject cloud_light2_1 = new GameObject("cloudLight0", true, true);
		cloud_light2_0.setWidth(360);
		cloud_light2_0.setHeight(62);
		cloud_light2_0.setOriginX(180);
		cloud_light2_0.setScaleX(-1);
		
		cloud_light2_1.setWidth(360);
		cloud_light2_1.setHeight(62);
		cloud_light2_1.setOriginX(180);
		cloud_light2_1.setScaleX(-1);
		
		cloud_light2_0.setPositionXY(24,641);
		cloud_light2_1.setPositionXY(-360,641);

		MoveTo cloud_light2_0_moveTo0 = new MoveTo (1320, 641, 128, 0, false);
		MoveTo cloud_light2_0_moveTo1 = new MoveTo (-360, 641, 1320, 641, 180, 128, true);
		
		MoveTo cloud_light2_1_moveTo0 = new MoveTo (-360, 641, 1320, 641, 180, 38, true);

		cloud_light2_0.action(cloud_light2_0_moveTo0);
		cloud_light2_0.action(cloud_light2_0_moveTo1);
		cloud_light2_1.action(cloud_light2_1_moveTo0);
		
		//----------------------CLOUD0-------------------------------------------
		final GameObject cloud0_0 = new GameObject("cloud0", true, true);
		final GameObject cloud0_1 = new GameObject("cloud0", true, true);
		cloud0_0.setWidth(200);
		cloud0_0.setHeight(112);
		cloud0_1.setWidth(200);
		cloud0_1.setHeight(112);
		
		cloud0_0.setPositionXY(149, 652);
		cloud0_1.setPositionXY(-200, 652);
		
		MoveTo cloud0_0_moveTo0 = new MoveTo (960, 652, 144, 0, false);
		MoveTo cloud0_0_moveTo1 = new MoveTo (-200, 652, 960, 652, 120, 144, true);
		
		MoveTo cloud0_1_moveTo0 = new MoveTo (960, 652, 120, 54, true);

		cloud0_0.action(cloud0_0_moveTo0);
		cloud0_0.action(cloud0_0_moveTo1);
		cloud0_1.action(cloud0_1_moveTo0);
		
		//----------------------CLOUD1-------------------------------------------
		final GameObject cloud1_0 = new GameObject("cloud0", true, true);
		final GameObject cloud1_1 = new GameObject("cloud0", true, true);
		cloud1_0.setWidth(354);
		cloud1_0.setHeight(194);
		cloud1_1.setWidth(354);
		cloud1_1.setHeight(194);
		
		cloud1_0.setPositionXY(542,505);
		cloud1_1.setPositionXY(-121,505);
		
		MoveTo cloud1_0_moveTo0 = new MoveTo (960, 505, 29, 0, false);
		MoveTo cloud1_0_moveTo1 = new MoveTo (-354, 505, 960, 505, 90, 29, true);
		
		MoveTo cloud1_1_moveTo0 = new MoveTo (960, 505, 74, 0, false);
		MoveTo cloud1_1_moveTo1 = new MoveTo (-354, 505, 960, 505, 90, 74, true);

		cloud1_0.action(cloud1_0_moveTo0);
		cloud1_0.action(cloud1_0_moveTo1);
		cloud1_1.action(cloud1_1_moveTo0);
		cloud1_1.action(cloud1_1_moveTo1);
		
		//----------------------CLOUD2-------------------------------------------
		final GameObject cloud2_0 = new GameObject("cloud0", true, true);
		final GameObject cloud2_1 = new GameObject("cloud0", true, true);
		cloud2_0.setWidth(354);
		cloud2_0.setHeight(194);
		cloud2_1.setWidth(354);
		cloud2_1.setHeight(194);
		cloud2_0.setScaleX(-1);
		
		cloud2_0.setPositionXY(8,397);
		cloud2_1.setPositionXY(-354,397);
		cloud2_1.setScaleX(-1);
		
		MoveTo cloud2_0_moveTo0 = new MoveTo (1314, 397, 43, 0, false);
		MoveTo cloud2_0_moveTo1 = new MoveTo (-354, 397, 1314, 397, 60, 43, true);
		
		MoveTo cloud2_1_moveTo1 = new MoveTo (1314, 397, 60, 13, true);

		cloud2_0.action(cloud2_0_moveTo0);
		cloud2_0.action(cloud2_0_moveTo1);
		cloud2_1.action(cloud2_1_moveTo1);

		//----------------------OWL-------------------------------------------
		//----OWL-CONTAINER----
		final GameObject owl_container = new GameObject();
		owl_container.setPositionXY(133,116);
		owl_container.setOriginX(73);
		owl_container.setOriginY(46);
		
		ScaleTo owl_scale1 = ScaleTo.$(1.02f, 0.98f, 1);
		ScaleTo owl_scale2 = ScaleTo.$(1.0f, 1.0f, 1);
		owl_container.action(Forever.$(Sequence.$(owl_scale1, owl_scale2)));		
		
		//----OWL-MAIN----
		final GameObject owl = new GameObject("owl", false, true);
		owl.setPositionXY(0,0);
 
		//----OWL-WING0----
		final GameObject owl_wing0 = new GameObject("owl-wing0", false, true);
		owl_wing0.setPositionXY(94,64);
		owl_wing0.setOriginX(9);
		owl_wing0.setOriginY(45);
		
		RotateTo owl_wing0_rotate1 = RotateTo.$(10, 1);
		RotateTo owl_wing0_rotate2 = RotateTo.$(0, 1);
		owl_wing0.action(Forever.$(Sequence.$(owl_wing0_rotate1, owl_wing0_rotate2)));

		//----OWL-WING1----
		final GameObject owl_wing1 = new GameObject("owl-wing1", false, true);
		owl_wing1.setPositionXY(13,55);
		owl_wing1.setOriginX(3);
		owl_wing1.setOriginY(41);

		RotateTo owl_wing1_rotate1 = RotateTo.$(-10, 1);
		RotateTo owl_wing1_rotate2 = RotateTo.$(0, 1);
		owl_wing1.action(Forever.$(Sequence.$(owl_wing1_rotate1, owl_wing1_rotate2)));
		
		final GameObject owl_eyes = new GameObject("owl-eyes", false, true);
		owl_eyes.setPositionXY(21, 113);
		
		FadeIn eyelids_fadeIn = FadeIn.$(0.1f);
		FadeOut eyelids_fadeOut = FadeOut.$(0.1f);
		
		owl_eyes.color.a = 0.0f;
		
		owl_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										6)
								), 
					          3)
					   );			
		
		//----ADD_ACTORS----
		owl_container.addActor(owl);
		owl_container.addActor(owl_wing0);
		owl_container.addActor(owl_wing1);
		owl_container.addActor(owl_eyes);
		
		//----------------------FLOWER0-------------------------------------------
		final GameObject flower0 = new GameObject("flower0", true, true);
		flower0.setOnCreateFilter(true);
		flower0.setWidth(75);
		flower0.setHeight(78);
		
		flower0.setPositionXY(18, 181);
		flower0.setOriginX(68);
		flower0.setOriginY(4);

		RotateTo flower0_rotate1 = RotateTo.$(5, 4);
		RotateTo flower0_rotate2 = RotateTo.$(-10, 4);
		flower0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0.action(Forever.$(Sequence.$(flower0_rotate1, flower0_rotate2)));
		
		//----------------------FLOWER1-------------------------------------------
		final GameObject flower1 = new GameObject("flower1", true, true);
		flower1.setOnCreateFilter(true);
		flower1.setWidth(69);
		flower1.setHeight(73);
		
		flower1.setPositionXY(98, 149);
		flower1.setOriginX(15);
		flower1.setOriginY(5);

		RotateTo flower1_rotate1 = RotateTo.$(6, 8);
		RotateTo flower1_rotate2 = RotateTo.$(-11, 8);
		flower1_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1.action(Forever.$(Sequence.$(flower1_rotate1, flower1_rotate2)));
		
		//----------------------FLOWER2-------------------------------------------
		final GameObject flower2 = new GameObject("flower0", true, true);
		flower2.setOnCreateFilter(true);
		flower2.setWidth(63);
		flower2.setHeight(72);
		
		flower2.setPositionXY(567,223);
		flower2.setOriginX(50);
		flower2.setOriginY(4);

		RotateTo flower2_rotate1 = RotateTo.$(4, 6);
		RotateTo flower2_rotate2 = RotateTo.$(-8, 6);
		flower2_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2.action(Forever.$(Sequence.$(flower2_rotate1, flower2_rotate2)));
		
		//----------------------FLOWER3-------------------------------------------
		final GameObject flower3 = new GameObject("flower1", true, true);
		flower3.setOnCreateFilter(true);
		flower3.setWidth(80);
		flower3.setHeight(92);
		
		flower3.setPositionXY(848,164);
		flower3.setOriginX(21);
		flower3.setOriginY(4);

		RotateTo flower3_rotate1 = RotateTo.$(4, 6);
		RotateTo flower3_rotate2 = RotateTo.$(-8, 6);
		flower3_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower3_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower3.action(Forever.$(Sequence.$(flower3_rotate1, flower3_rotate2)));

     	//----------------------OTHER-------------------------------------------
		final GameObject grass0 = new GameObject("grass0", true, true);
		grass0.setPositionXY(0, 0);

     	//----------------------RABBIT-------------------------------------------
		final GameObject ear0 = new GameObject("ear0", true, true);
		ear0.setOnCreateFilter(true);
		ear0.setPositionXY(790, 254);
		ear0.setOriginX(43);
		ear0.setOriginY(12);
		
		RotateTo ear0_rotate1 = RotateTo.$(10, 4);
		RotateTo ear0_rotate2 = RotateTo.$(0, 4);
		ear0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(ear0_rotate1, Delay.$(ear0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		final GameObject ear1 = new GameObject("ear1", true, true);
		ear1.setOnCreateFilter(true);
		ear1.setPositionXY(847, 239);
		ear1.setOriginX(12);
		ear1.setOriginY(16);

		RotateTo ear1_rotate1 = RotateTo.$(-5, 4);
		RotateTo ear1_rotate2 = RotateTo.$(0, 4);
		ear1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(ear1_rotate1, Delay.$(ear1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);
		
		final GameObject rabbit_head = new GameObject("rabbit-head", true, true);
		rabbit_head.setPositionXY(766, 241);
		
		final GameObject rabbit_eyes = new GameObject("rabbit-eyes", true, true);
		rabbit_eyes.setPositionXY(763, 195);
		
		rabbit_eyes.color.a = 0.0f;
		
		rabbit_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										8)
								), 
					          5)
					   );			
		
     	//----------------------PONIES-------------------------------------------
		this.ponies = new ArrayList<GameObject>();
		//----------------RAINBOW DASH-----------------------
		final GameObject rainbow_dash = new GameObject("body0", true, true);
		rainbow_dash.objectSubFolder = "body0";
		rainbow_dash.setPositionXY(307, 105);
		rainbow_dash.visible = false;
		this.ponies.add(rainbow_dash);
		
		final GameObject rainbow_eyes = new GameObject("body0-eyes", false, true);
		rainbow_eyes.setPositionXY(48, 282);
		rainbow_dash.addActor(rainbow_eyes);
		
		rainbow_eyes.color.a = 0.0f;
		
		rainbow_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										10)
								), 
					          7)
					   );		

		//----------------RARITY-----------------------
		final GameObject rarity = new GameObject("body1", true, true);
		rarity.objectSubFolder = "body1";
		rarity.setPositionXY(264, 97);
		rarity.visible = false;
		this.ponies.add(rarity);

		final GameObject rarity_eyes = new GameObject("body1-eyes", false, true);
		rarity_eyes.setPositionXY(42, 257);
		rarity.addActor(rarity_eyes);
		
		rarity_eyes.color.a = 0.0f;
		
		rarity_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										10)
								), 
					          7)
					   );		
		
		//----------------APPLE JACK-----------------------
		final GameObject apple_jack = new GameObject("body2", true, true);
		apple_jack.objectSubFolder = "body2";
		apple_jack.setPositionXY(343, 68);
		apple_jack.visible = false;
		this.ponies.add(apple_jack);

		final GameObject apple_eyes = new GameObject("body2-eyes", false, true);
		apple_eyes.setPositionXY(26, 208);
		apple_jack.addActor(apple_eyes);
		
		apple_eyes.color.a = 0.0f;
		
		apple_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										10)
								), 
					          7)
					   );		
		
		//----------------PINKY PIE-----------------------
		final GameObject pinky_pie = new GameObject("body3", true, true);
		pinky_pie.objectSubFolder = "body3";
		pinky_pie.setPositionXY(300, 89);
		pinky_pie.visible = false;
		this.ponies.add(pinky_pie);

		final GameObject pinky_eyes = new GameObject("body3-eyes", false, true);
		pinky_eyes.setPositionXY(194, 219);
		pinky_pie.addActor(pinky_eyes);
		
		pinky_eyes.color.a = 0.0f;
		
		pinky_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										10)
								), 
					          7)
					   );		
		
		//----------------TWILIGHT SPARKLE-----------------------
		final GameObject twilight_sparkle = new GameObject("body4", true, true);
		twilight_sparkle.objectSubFolder = "body4";
		twilight_sparkle.setPositionXY(268, 94);
		twilight_sparkle.visible = false;
		this.ponies.add(twilight_sparkle);

		final GameObject twilight_eyes = new GameObject("body4-eyes", false, true);
		twilight_eyes.setPositionXY(69, 276);
		twilight_sparkle.addActor(twilight_eyes);
		
		twilight_eyes.color.a = 0.0f;
		
		twilight_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										10)
								), 
					          7)
					   );		
		
		//----------------FLUTTERSHY-----------------------
		final GameObject fluttershy = new GameObject("body5", true, true);
		fluttershy.objectSubFolder = "body5";
		fluttershy.setPositionXY(315, 87);
		fluttershy.visible = false;
		this.ponies.add(fluttershy);

		final GameObject fluttershy_eyes = new GameObject("body5-eyes", false, true);
		fluttershy_eyes.setPositionXY(110, 291);
		fluttershy.addActor(fluttershy_eyes);
		
		fluttershy_eyes.color.a = 0.0f;
		
		fluttershy_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										10)
								), 
					          7)
					   );		
		
		//----------------------GOLD-------------------------------------------
		final GameObject gold = new GameObject("gold", true, true);
		gold.setPositionXY(274, 106);

		//----------------------GOLD PARTICLES-------------------------------------------	
		final GameObject particles1 = new GameObject(true, 1, 1);
		particles1.setPositionXY(0,0);
		particles1.setOriginX(285);
		particles1.setOriginY(235);
		particles1.createParticle("p1", false);

		final GameObject particles2 = new GameObject(true, 1, 1);
		particles2.setPositionXY(0,0);
		particles2.setOriginX(333);
		particles2.setOriginY(237);
		particles2.createParticle("p2", false);
		
	}

	public void setActivePony(int index)
	{

		ImageCache.getManager().finishLoading();
		
		for(GameObject pony: ponies)
		{
			ImageCache.unload(name, pony.skinName);
			pony.clearSkin();
			pony.visible = false;
		}
		
		GameObject pony = ponies.get(index); 
		ImageCache.load(name, pony.skinName);
		pony.visible = true;
	}

	public void initializeParticles() {
		//----------------------PARTICLES-------------------------------------------	
		GameObject main_particles = (GameObject) MyPatrickPonyLW.getRoot().findActor("p0");
		
		if (main_particles == null)
		{
			main_particles = new GameObject("p0",true, 0.1f, 1);
			main_particles.setPositionXY(0,0);
			main_particles.setOriginX(460);
			main_particles.setOriginY(560);
			main_particles.createParticle("p0", false);
		}
		
		sparklesVisible = true;
		main_particles.visible = sparklesVisible;
	}

	public void disableParticles()
	{
		sparklesVisible = false;
		GameObject particle = (GameObject) MyPatrickPonyLW.getRoot().findActor("p0");
		
		if(particle != null)
			//particle.clearParticlesAndRemove();
			particle.visible = sparklesVisible;
		
	}
	
	@Override
	public void onConfigChanged() {
		if (sparklesVisible && !showParticles)
			disableParticles();
		else if(!sparklesVisible && showParticles)
			initializeParticles();
	}
}
