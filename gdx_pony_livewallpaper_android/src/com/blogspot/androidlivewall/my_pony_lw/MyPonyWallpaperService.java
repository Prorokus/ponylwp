package com.blogspot.androidlivewall.my_pony_lw;

import lw_engine.my_pony_lw.MyPonyLW;

import com.badlogic.gdx.backends.android.livewallpaper.AndroidApplication;
import com.eightbitmage.gdxlw.LibdgxWallpaperService;

public class MyPonyWallpaperService extends LibdgxWallpaperService {

	@Override
	public Engine onCreateEngine() {
		//android.os.Debug.waitForDebugger();
		// TODO Auto-generated method stub
		return new MyPonyWallpaperEngine(this);
	}
	
	public class MyPonyWallpaperEngine extends LibdgxWallpaperEngine
	{

		public MyPonyWallpaperEngine(
				LibdgxWallpaperService libdgxWallpaperService) {
			super(libdgxWallpaperService);
			// TODO Auto-generated constructor stub
		}
		
		protected void initialize(AndroidApplication androidApplicationLW)
		{
			MyPonyLW app = new MyPonyLW();
			setWallpaperListener(app);
			androidApplicationLW.initialize(app,false);
			
		}
	}
}