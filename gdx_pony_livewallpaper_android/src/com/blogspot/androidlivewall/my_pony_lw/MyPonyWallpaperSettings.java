package com.blogspot.androidlivewall.my_pony_lw;

import lw_engine.my_pony_lw.MyPonyLW;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceClickListener;
import android.util.Log;
import android.widget.Toast;

import com.appbrain.AppBrain;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.livewallpaper.AndroidPreferences;

public class MyPonyWallpaperSettings extends PreferenceActivity
implements SharedPreferences.OnSharedPreferenceChangeListener, OnPreferenceClickListener
{
	public String MY_PONY_PREFS = "com.blogspot.androidlivewall.my_pony_lw_preferences";
	private AndroidPreferences prefs;
	private boolean prefsChanged = false;
	
	private String uriPkg;
	
	protected void onCreate(Bundle icicle) {
		
    	super.onCreate(icicle);
    	try{
    		if (Gdx.app != null)
    			prefs = (AndroidPreferences) Gdx.app.getPreferences(MY_PONY_PREFS);
    	}
    	catch(NullPointerException ex)
    	{
    		Log.e("MyPonyLW_Setting Error", ex.toString());
    	}
    	addPreferencesFromResource(R.xml.settings);
    	
    	uriPkg = "market://details?id=" + getString(R.string.other_pony_pkg);
    	
    	
    	final Preference soaring_lwp_but = (Preference)findPreference(getString(R.string.soaring_but_key));
    	soaring_lwp_but.setOnPreferenceClickListener(this);
    	
    	final Preference patrick_lwp_but = (Preference)findPreference(getString(R.string.patrick_but_key));
    	patrick_lwp_but.setOnPreferenceClickListener(this);
    	
    	final Preference easter_lwp_but = (Preference)findPreference(getString(R.string.easter_but_key));
    	easter_lwp_but.setOnPreferenceClickListener(this);
    	
    	
    	//AppBrain integration
    	AppBrain.init(this);
    	
    	final Preference appbrain_but = (Preference)findPreference(getString(R.string.appbrain_but_key));
    	appbrain_but.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				// TODO Auto-generated method stub
				if(!AppBrain.getAds().showInterstitial(MyPonyWallpaperSettings.this)){
					Toast.makeText(getApplicationContext(), "Not showing, no internet connection?" , Toast.LENGTH_SHORT).show();
					return false;
				}
				return true;
			}
		});

	}

	@Override
	protected void onResume() {
    	super.onResume();
    	if(prefs != null)
    		prefs.getSharedPrefs().registerOnSharedPreferenceChangeListener(this);
	}

	protected void onPause()
	{
		super.onPause();
		if(prefs != null)
			prefs.getSharedPrefs().unregisterOnSharedPreferenceChangeListener(this);
		
	 	if(this.prefsChanged)
	 		MyPonyLW.prefsChanged = true;
	}
	
	@Override
	protected void onDestroy() {
	 	super.onDestroy();

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		// TODO Auto-generated method stub
		this.prefsChanged = true;
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_VIEW);
		String intentData = "";
		
		switch (preference.getOrder()) {
		
		case 0:
			intentData = "hover_pony_lw_free";
			break;
		case 1:
			intentData = "saint_patrick_pony_lw";
			break;
		case 2:
			intentData = "easter_pony_lw";
			break;

		}
		
		intent.setData(Uri.parse( uriPkg + intentData));
		
		try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
        	Toast.makeText(getBaseContext(), "Cannot find Google Play",
                    Toast.LENGTH_LONG).show();
            return false;
        } 
		return true;
	}

}