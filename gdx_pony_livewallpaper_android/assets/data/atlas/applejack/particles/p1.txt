apple0
- Delay -
active: true
lowMin: 30.0
lowMax: 60.0
- Duration - 
lowMin: 0.0
lowMax: 0.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 2.0
highMax: 2.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 5500.0
highMax: 6000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: square
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 35.0
lowMax: 45.0
highMin: 45.0
highMax: 35.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 5.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 70.0
highMax: 110.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: -45.0
lowMax: 45.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.5294118
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5068493
timeline2: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -15.0
highMax: 15.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Gravity - 
active: true
lowMin: 5.0
lowMax: 5.0
highMin: 10.0
highMax: 20.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 1.0
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.14383562
timeline2: 0.84931505
timeline3: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: false
behind: false
- Image Path -
E:\AndroidProjects\ponylwp\art_src7\apple0.png


apple1
- Delay -
active: true
lowMin: 30.0
lowMax: 50.0
- Duration - 
lowMin: 0.0
lowMax: 0.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 2.0
highMax: 2.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 5500.0
highMax: 6000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: square
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 35.0
lowMax: 45.0
highMin: 45.0
highMax: 35.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 3.0
highMax: 7.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 70.0
highMax: 110.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: -45.0
lowMax: 45.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.5294118
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5
timeline2: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -15.0
highMax: 15.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Gravity - 
active: true
lowMin: 5.0
lowMax: 5.0
highMin: 10.0
highMax: 20.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 1.0
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.15753424
timeline2: 0.8561644
timeline3: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: false
behind: false
- Image Path -
E:\AndroidProjects\ponylwp\art_src7\apple1.png
