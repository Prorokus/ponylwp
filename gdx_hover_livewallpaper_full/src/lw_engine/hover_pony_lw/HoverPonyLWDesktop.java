package lw_engine.hover_pony_lw;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.jogl.JoglApplication;

public class HoverPonyLWDesktop {
	public static void main(String[] argv)
	{
		JoglApplication app = new JoglApplication((ApplicationListener) new HoverPonyLW(), "Hover Pony LW App", 960, 854,  false);
	}
}
