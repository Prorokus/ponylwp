package lw_engine.hover_pony_lw;

import lwp_engine.GameObject;
import lwp_engine.GameObjectAnimation.AnimKey;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.RandomLighten;
import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

public class HoverCloudsScreen extends HoverWallpaperScreen {
	
	public HoverCloudsScreen(String name) {
		super(name);
	}

	@Override
	public void initialize() {
		super.initialize();
		
		ImageCache.load(name, "back2");
		
		//----------------------BACKGROUND-------------------------------------------
		final GameObject bg = new GameObject("background", true, false, 0.1f, 1);
		bg.objectSubFolder = "back2";
		bg.setPositionXY(0,0);
		
		//----------------------CLOUD1-------------------------------------------
		final GameObject cloud1_0 = new GameObject("cloud1", "cloud", true, true, 0.1f, 1);
		cloud1_0.objectSubFolder = "back2";
		
		cloud1_0.setPositionXY(215 + 960 * 0.5f, 670);
		cloud1_0.setScaleX(-0.7f);
		cloud1_0.setScaleY(0.7f);
		
		MoveTo cloud1_0_moveTo0 = new MoveTo (1200, 670, 60, 0, false);
		MoveTo cloud1_0_moveTo1 = new MoveTo (-215, 670, 1200, 670, 120, 60, true);
		
		cloud1_0.action(cloud1_0_moveTo0);
		cloud1_0.action(cloud1_0_moveTo1);
		
		//----------------------CLOUD0-------------------------------------------
		final GameObject cloud0_0 = new GameObject("cloud0", "cloud", true, true, 0.1f, 1);
		cloud0_0.objectSubFolder = "back2";
		
		cloud0_0.setPositionXY(-308, 640);
		
		MoveTo cloud0_0_moveTo0 = new MoveTo (-308, 640, 960, 640, 60, 0, true);

		cloud0_0.action(cloud0_0_moveTo0);
		
		//----------------------SUN-------------------------------------------
		final GameObject sun = new GameObject("sun", true, true, 0.1f, 1);
		sun.setPositionXY(0, 473);
		
		RandomLighten randLight = new RandomLighten (0.5f, 1, -1, 1, 1);
		sun.action(Forever.$(randLight));

		//----------------------BALLOON-CONTAINER0-------------------------------------------
		final GameObject balloon_container0 = new GameObject("balloon_container0");
		balloon_container0.setPositionXY(591, 664);
		
		MoveTo balloon_container0_moveTo0 = new MoveTo (960, 600, -94, 550, 120, 0, true);
		balloon_container0.action(balloon_container0_moveTo0);		
		
		//----------------------BALLOON-CONTAINER-------------------------------------------
		final GameObject balloon_container = new GameObject("balloon_container", false);
		balloon_container.setPositionXY(0, 0);
		
		MoveTo balloon_container_moveTo0 = new MoveTo (0, 0, 0, 5, 4, 0, false);
		MoveTo balloon_container_moveTo1 = new MoveTo (0, 5, 0, 0, 4, 0, false);
		balloon_container_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		balloon_container_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());
		balloon_container.action(Forever.$(Sequence.$(balloon_container_moveTo0, balloon_container_moveTo1)));		

		balloon_container0.addActor(balloon_container);
		
		//----------------------BALLOON0-------------------------------------------
		final GameObject balloon0 = new GameObject("balloon0", false, true, 0.1f, 1);
		balloon0.setOnCreateFilter(true);
		balloon0.setPositionXY(27, 0);
		balloon0.setOriginX(20);
		balloon0.setOriginY(39);
		
		RotateTo balloon0_rotate1 = RotateTo.$(-3, 2);
		RotateTo balloon0_rotate2 = RotateTo.$(3, 2);
		balloon0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		balloon0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		balloon0.action(Forever.$(Sequence.$(balloon0_rotate1, balloon0_rotate2)));
		
		//----------------------BALLOON1-------------------------------------------
		final GameObject balloon1 = new GameObject("balloon1", false, true, 0.1f, 1);
		balloon1.setPositionXY(0, 35);
		
		balloon_container.addActor(balloon0);
		balloon_container.addActor(balloon1);
		
		//----BIRD0----
		final GameObject bird0 = new GameObject("bird0", true, true);
		bird0.pongAnimation = true;
		bird0.setPositionXY(960, 560);
		bird0.setWidth(56);
		bird0.setHeight(74);
		bird0.AddAnim(new AnimKey("bird0", 0.1f));

		MoveTo bird0_moveTo0 = new MoveTo (960, 560, -77, 560, 15, 0, false);
		MoveTo bird0_moveTo1 = new MoveTo (960, 560, 0, 0, false);
		
		bird0.action(Forever.$(
							   Sequence.$(
										  bird0_moveTo0, Delay.$(bird0_moveTo1, 50))
							 )
					 );
		
		//----BIRD1----
		final GameObject bird1 = new GameObject("bird1", true, true);
		bird1.pongAnimation = true;
		bird1.setPositionXY(-77, 670);
		bird1.setWidth(56);
		bird1.setHeight(74);
		bird1.AddAnim(new AnimKey("bird1", 0.1f));

		MoveTo bird1_moveTo0 = new MoveTo (-77, 670, 960, 670, 15, 0, false);
		MoveTo bird1_moveTo1 = new MoveTo (-77, 670, 0, 0, false);
		
		bird1.action(Forever.$(
							   Sequence.$(
									      Delay.$(bird1_moveTo0, 25), Delay.$(bird1_moveTo1, 25))
							   )
				     );
		
		//----------------------BACK1-------------------------------------------
		final GameObject back1 = new GameObject("back1", true, true);
		back1.objectSubFolder = "back2";
		back1.setPositionXY(100, 0);
		
		//----SHADOW----
		final GameObject shadow = new GameObject("shadow", true, true);
		shadow.setPositionXY(330, 85);
		shadow.setOriginX(173);
		shadow.setOriginY(39);

		setActivePony(HoverPonyLW.selectedPony);
		
        //----CLOUD-PARTICLE----
	    final GameObject cloud_p = new GameObject("p3", true);
	    cloud_p.setPositionXY(0,0);
	    cloud_p.setOriginX(480);
	    cloud_p.setOriginY(200);
	    cloud_p.createParticle("p3", false);		  
		
		//----------------------BACK2-------------------------------------------
		final GameObject back2 = new GameObject("back2", true, true, 3, 1);
		back2.objectSubFolder = "back2";
		back2.setOnCreateFilter(true);
		back2.setPositionXY(-150, 0);
	
		
		//----------------------BACK3-------------------------------------------
		final GameObject back3 = new GameObject("back3", "back2", true, true, 3, 1);
		back3.objectSubFolder = "back2";
		back3.setOnCreateFilter(true);
		back3.setPositionXY(1203, 0);
		back3.scaleX = -1;

	}

	@Override
	public void enableAdditionalAnims() {
		// TODO Auto-generated method stub
		//----------------------BACK2-------------------------------------------
		GameObject back2 = (GameObject) HoverPonyLW.getRoot().findActor("back2");
		
		MoveTo back2_body_moveTo0 = new MoveTo (-150, -10, 4, 0, false);
		MoveTo back2_body_moveTo1 = new MoveTo (-150, 0, 4, 0, false);
		back2_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		back2_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());  
		back2.action(Forever.$(Sequence.$(back2_body_moveTo0, back2_body_moveTo1)));
		
		//----------------------BACK3-------------------------------------------
		GameObject back3 = (GameObject) HoverPonyLW.getRoot().findActor("back3");
		
		MoveTo back3_body_moveTo0 = new MoveTo (1203, -10, 3, 0, false);
		MoveTo back3_body_moveTo1 = new MoveTo (1203, 0, 3, 0, false);
		back3_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		back3_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());  
		back3.action(Forever.$(Sequence.$(back3_body_moveTo0, back3_body_moveTo1)));
	}

	@Override
	public void disableAdditionalAnims() {
		// TODO Auto-generated method stub
		//----------------------BACK2-------------------------------------------
		GameObject back2 = (GameObject) HoverPonyLW.getRoot().findActor("back2");
		back2.clearActions();
				
		//----------------------BACK3-------------------------------------------
		GameObject back3 = (GameObject) HoverPonyLW.getRoot().findActor("back3");
		back3.setPositionXY(1203, 0);
		back3.clearActions();
		
	}

	@Override
	public void enableAdditionalParticles() {
		// TODO Auto-generated method stub
		GameObject smoke = (GameObject) HoverPonyLW.getRoot().findActor("p3");	
		smoke.visible = true;	
	}

	@Override
	public void disableAdditionalParticles() {
		// TODO Auto-generated method stub
		GameObject smoke = (GameObject) HoverPonyLW.getRoot().findActor("p3");	
		smoke.visible = false;	
	}

	@Override
	public void enableAnimClouds() {
		//----------------------CLOUD1-------------------------------------------
		GameObject cloud1_0 = (GameObject) HoverPonyLW.getRoot().findActor("cloud1");
		cloud1_0.visible = true;		
		
		
		//----------------------CLOUD0-------------------------------------------
		GameObject cloud0_0 = (GameObject) HoverPonyLW.getRoot().findActor("cloud0");
		cloud0_0.visible = true;		
		
	}

	@Override
	public void disableAnimClouds() {
		//----------------------CLOUD1-------------------------------------------
		GameObject cloud1_0 = (GameObject) HoverPonyLW.getRoot().findActor("cloud1");
		cloud1_0.visible = false;		
		
		//----------------------CLOUD0-------------------------------------------
		GameObject cloud0_0 = (GameObject) HoverPonyLW.getRoot().findActor("cloud0");
		cloud0_0.visible = false;		
		
	}

	@Override
	public void enableAnimBalloon() {
		//----------------------BALLOON-------------------------------------------
		GameObject balloon_container0 = (GameObject) HoverPonyLW.getRoot().findActor("balloon_container0");		
		balloon_container0.visible = true;		
		
	}

	@Override
	public void disableAnimBalloon() {
		//----------------------BALLOON-------------------------------------------
		GameObject balloon_container0 = (GameObject) HoverPonyLW.getRoot().findActor("balloon_container0");		
		balloon_container0.visible = false;		
		
	}

	@Override
	public void enableAnimBird() {
		//----------------------BIRD0-------------------------------------------
		GameObject bird0 = (GameObject) HoverPonyLW.getRoot().findActor("bird0");	
		bird0.visible = true;		

		//----------------------BIRD1-------------------------------------------
		GameObject bird1 = (GameObject) HoverPonyLW.getRoot().findActor("bird1");	
		bird1.visible = true;		
		
	}

	@Override
	public void disableAnimBird() {
		//----------------------BIRD0-------------------------------------------
		GameObject bird0 = (GameObject) HoverPonyLW.getRoot().findActor("bird0");	
		bird0.visible = false;		

		//----------------------BIRD1-------------------------------------------
		GameObject bird1 = (GameObject) HoverPonyLW.getRoot().findActor("bird1");	
		bird1.visible = false;		
		
	}
}
