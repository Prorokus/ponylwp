package lw_engine.hover_pony_lw;

import lw_engine.hover_pony_lw.pony_container.HoverPonySelector;
import lwp_engine.GameObject;
import lwp_engine.WallpaperScreen;

public abstract class HoverWallpaperScreen extends WallpaperScreen {
	
	private HoverPonySelector ponySelector;
	
	private int selectedPonyId;
	
	private boolean animPony = false;
	
	private boolean additAnims = false;
	private boolean additParticles = false;
	
	private boolean animClouds = false;
	private boolean animBalloon = false;
	private boolean animBird = false;
	
	private boolean firstRun = true;

	public HoverWallpaperScreen(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		ponySelector = new HoverPonySelector(name);
		selectedPonyId = -1;
		firstRun = true;
	}
	
	public void initializeParticles() {
		//----------------------PARTICLES-------------------------------------------	
		GameObject main_particles = (GameObject) HoverPonyLW.getRoot().findActor("p0");
		
		if (main_particles == null)
		{
			main_particles = new GameObject("p0",true);
			main_particles.setPositionXY(0,0);
			main_particles.setOriginX(460);
			main_particles.setOriginY(600);
			main_particles.createParticle("p0", false);	
		}
		
		sparklesVisible = true;
		main_particles.visible = sparklesVisible;
	}

	public void disableParticles()
	{
		sparklesVisible = false;
		GameObject particle = (GameObject) HoverPonyLW.getRoot().findActor("p0");
		
		if(particle != null)
			//particle.clearParticlesAndRemove();
			particle.visible = sparklesVisible;
		
	}
	
	public void animateMyPony()
	{
		if (animPony)
			ponySelector.enablePonyAnim();
		else
			ponySelector.disablePonyAnim();
	}
	
	//additional animatoins
	public abstract void enableAdditionalAnims();
	public abstract void disableAdditionalAnims();

	//additional particles
	public abstract void enableAdditionalParticles();
	public abstract void disableAdditionalParticles();
	
	//clouds, bird, balloon animation
	public abstract void enableAnimClouds();
	public abstract void disableAnimClouds();
	
	public abstract void enableAnimBalloon();
	public abstract void disableAnimBalloon();
	
	public abstract void enableAnimBird();
	public abstract void disableAnimBird();
	
	@Override
	public void onConfigChanged() {
		
		//if we change wallpaper or first run - disable all animation to reenable it correctly
		if(firstRun)
		{
			firstRun = false;
			
			disableParticles();
			ponySelector.disablePonyAnim();
			disableAdditionalAnims();
			disableAdditionalParticles();
			disableAnimClouds();
			disableAnimBalloon();
			disableAnimBird();
		}
		
		// TODO Auto-generated method stub
		if (sparklesVisible && !showParticles)
			disableParticles();
		else if(!sparklesVisible && showParticles)
			initializeParticles();
		
		if (animPony != HoverPonyLW.animetePony)
		{
			animPony = HoverPonyLW.animetePony;
			animateMyPony();
		}
		
		if (additAnims != HoverPonyLW.additionalAnims)
		{
			additAnims = HoverPonyLW.additionalAnims;
			
			if (additAnims)
				enableAdditionalAnims();
			else
				disableAdditionalAnims();
		}

		if (additParticles != HoverPonyLW.additionalParticles)
		{
			additParticles = HoverPonyLW.additionalParticles;
			
			if (additParticles)
				enableAdditionalParticles();
			else
				disableAdditionalParticles();
		}
		
		if (animClouds != HoverPonyLW.animClouds)
		{
			animClouds = HoverPonyLW.animClouds;
			
			if (animClouds)
				enableAnimClouds();
			else
				disableAnimClouds();
		}
		
		if (animBalloon != HoverPonyLW.animBalloon)
		{
			animBalloon = HoverPonyLW.animBalloon;
			
			if (animBalloon)
				enableAnimBalloon();
			else
				disableAnimBalloon();
		}
		
		if (animBird != HoverPonyLW.animBird)
		{
			animBird = HoverPonyLW.animBird;
			
			if (animBird)
				enableAnimBird();
			else
				disableAnimBird();
		}		
	}
	
	public void setActivePony(int id)
	{
		if (selectedPonyId != id )
		{
			ponySelector.getPony(id);
			selectedPonyId = id;
			
			if(HoverPonyLW.animetePony)
				ponySelector.enablePonyAnim();
			else
				ponySelector.disablePonyAnim();
			
			animPony = HoverPonyLW.animetePony;
		}
	}

}
