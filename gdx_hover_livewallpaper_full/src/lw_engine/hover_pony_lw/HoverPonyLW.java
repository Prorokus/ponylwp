package lw_engine.hover_pony_lw;

import lwp_engine.LWPApplication;

public class HoverPonyLW extends LWPApplication{

	private final String MY_PONY_PREFS = "com.blogspot.androidlivewall.hover_pony_lw_full_preferences";

	private boolean showParticles = true;
	public  static int selectedPony = 2;
	private int prevSelectedPony = -1;
	
	public static boolean animetePony = true;
	
	public static boolean additionalAnims = true;
	public static boolean additionalParticles = true;
	
	public static boolean animClouds = true;
	public static boolean animBalloon = true;
	public static boolean animBird = true;
	
	@Override
	protected String getPrefsName()
	{
		return MY_PONY_PREFS;
	}
	
	@Override
	protected void loadWallScreen(boolean... params)
	{
		if ((selectedWall == prevSelectedWall) && params[0] != true)
		{
			return;
		}
		else
		{
			prevSelectedWall = selectedWall;
			if (wallScreen != null)
			{
				wallScreen.destroy();
				wallScreen = null;
			}
		}
		
		switch(selectedWall)
		{
			case 0:
				wallScreen = new HoverCityScreen("hover");
				break;
			case 1:
				wallScreen = new HoverVillageScreen("hover");
				break;
			case 2:
				wallScreen = new HoverCloudsScreen("hover");
				break;
		}
			
		wallConfigChanged();

		prevSelectedPony = selectedPony;
		prevSelectedWall = selectedWall;
	}

	@Override
	protected void readAllPrefs() {
		enableParallax = prefs.getBoolean("set_pony_parallax",false);
		
		wallNotMovable = prefs.getBoolean("set_pony_center",false);
		selectedWall = Integer.parseInt(prefs.getString("selected_wallpaper","1"));
		selectedPony = Integer.parseInt(prefs.getString("selected_pony","2"));
		
		showParticles = prefs.getBoolean("set_pony_sparkles",true);
		animetePony = prefs.getBoolean("set_pony_anim",true);
		
		additionalAnims = prefs.getBoolean("set_additional_anims",true);
		additionalParticles = prefs.getBoolean("set_additional_particles", true);
		
		animClouds = prefs.getBoolean("set_clouds_anim",true);
		animBalloon = prefs.getBoolean("set_balloon_anim",true);
		animBird = prefs.getBoolean("set_bird_anim",true);
		
	}
		
	private void changePonyImage()
	{
		final HoverWallpaperScreen screen = (HoverWallpaperScreen)wallScreen;
		screen.setActivePony(selectedPony);
	}

	@Override
	protected void onResumeWallpaper() {
		// TODO Auto-generated method stub
				
		if (selectedWall != prevSelectedWall)
		{
			loadWallScreen();
		}
		else if (selectedPony != prevSelectedPony)
		{
			changePonyImage();
			prevSelectedPony = selectedPony;
		}
		
		if (wallScreen != null)
		{
			wallConfigChanged();
		}
	}
	
	private void wallConfigChanged()
	{
		wallScreen.showParticles = showParticles;
		wallScreen.onConfigChanged();
	}
		
}
