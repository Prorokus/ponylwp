package lw_engine.hover_pony_lw.pony_container;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.GameObjectAnimation.AnimKey;

public class RarityContainer extends PonyContainer {

	public RarityContainer(String name) {
		super(name,"rarity");
		// TODO Auto-generated constructor stub
	}

	@Override
	public GameObject initializePony() {
		//----------------RARITY-----------------------
		final GameObject rarity_container = new GameObject("rarity_container",false);
		rarity_container.setPositionXY(210, 181);
		
		ImageCache.load(name, imgName);
		
		//----WING0----
		final GameObject rarity_wing0 = new GameObject("wing0", false);
		rarity_wing0.objectSubFolder = "rarity";
		rarity_wing0.pongAnimation = true;
		rarity_wing0.setPositionXY(171, 69);
		rarity_wing0.scaleX = -1;
		rarity_wing0.AddAnim(new AnimKey("wing1", 0.1f));

		//----WING1----
		final GameObject rarity_wing1 = new GameObject("wing1", false);
		rarity_wing1.objectSubFolder = "rarity";
		rarity_wing1.pongAnimation = true;
		rarity_wing1.setPositionXY(381, 69);
		rarity_wing1.AddAnim(new AnimKey("wing1", 0.1f));
		
		
		//----EAR0----
		final GameObject rarity_ear0 = new GameObject("ear", false, true);
		rarity_ear0.objectSubFolder = "rarity";
		rarity_ear0.setOnCreateFilter(true);
		rarity_ear0.setPositionXY(294, 340);
		rarity_ear0.setOriginX(12);
		rarity_ear0.setOriginY(14);
		
		RotateTo rarity_ear0_rotate1 = RotateTo.$(-5, 4);
		RotateTo rarity_ear0_rotate2 = RotateTo.$(5, 4);		
		rarity_ear0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(rarity_ear0_rotate1, Delay.$(rarity_ear0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		//----EAR1----
		final GameObject rarity_ear1 = new GameObject("ear", false, true);
		rarity_ear1.objectSubFolder = "rarity";
		rarity_ear1.setOnCreateFilter(true);
		rarity_ear1.setPositionXY(250, 338);
		rarity_ear1.setOriginX(12);
		rarity_ear1.setOriginY(16);
		rarity_ear1.scaleX = -1;
		rarity_ear1.rotation = 5;

		RotateTo rarity_ear1_rotate1 = RotateTo.$(10, 4);
		RotateTo rarity_ear1_rotate2 = RotateTo.$(0, 4);		
		rarity_ear1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(rarity_ear1_rotate1, Delay.$(rarity_ear1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);		
		
		//----BODY----
		final GameObject rarity = new GameObject("body", false, true);
		rarity.objectSubFolder = "rarity";
		rarity.setPositionXY(86, 0);
		
		//----EYE----
		final GameObject rarity_eyes = new GameObject("eye", false, true);
		rarity_eyes.pongAnimation = true;
		rarity_eyes.objectSubFolder = "rarity";
		rarity_eyes.setPositionXY(203, 173);
		rarity_eyes.setWidth(161);
		rarity_eyes.setHeight(109);
		rarity_eyes.pauseAnim = 10;
		rarity_eyes.AddAnim(new AnimKey("eye", 0.1f));

		rarity_container.addActor(rarity_wing0);
		rarity_container.addActor(rarity_wing1);
		rarity_container.addActor(rarity_ear0);
		rarity_container.addActor(rarity_ear1);
		rarity_container.addActor(rarity);
		rarity_container.addActor(rarity_eyes);		
		
		return rarity_container;
	}

	@Override
	public void enablePonyAnim() {
		//----------------RARITY-----------------------
		GameObject rarity_container = (GameObject) ponyObj;
		
		MoveTo rarity_body_moveTo0 = new MoveTo (210, 176, 2, 0, false);
		MoveTo rarity_body_moveTo1 = new MoveTo (210, 186, 2, 0, false);
		rarity_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		rarity_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());		
		rarity_container.action(Forever.$(Sequence.$(rarity_body_moveTo0, rarity_body_moveTo1)));		
		
		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = true;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = true;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		ScaleTo shadow_scale1 = ScaleTo.$(1, 1, 2);
		ScaleTo shadow_scale2 = ScaleTo.$(0.93f, 0.93f, 2);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));	
		
	}

	@Override
	public void disablePonyAnim() {
		//----------------RARITY-----------------------
		GameObject rarity_container = (GameObject) ponyObj;
		rarity_container.clearActions();

		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = false;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = false;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");	
		shadow.clearActions();
		
	}

}
