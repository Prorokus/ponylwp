package lw_engine.hover_pony_lw.pony_container;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.GameObjectAnimation.AnimKey;

public class PinkiePieContainer extends PonyContainer {

	public PinkiePieContainer(String name) {
		super(name,"pinkie");
		// TODO Auto-generated constructor stub
	}

	@Override
	public GameObject initializePony() {
		//----------------PINKIE-----------------------
		final GameObject pinkie_container = new GameObject("pinkie_container",false);
		pinkie_container.setPositionXY(225, 182);
		
		ImageCache.load(name, imgName);
		
		//----WING0----
		final GameObject pinkie_wing0 = new GameObject("wing0", false);
		pinkie_wing0.objectSubFolder = "pinkie";
		pinkie_wing0.pongAnimation = true;
		pinkie_wing0.setPositionXY(152, 101);
		pinkie_wing0.scaleX = -1;
		pinkie_wing0.AddAnim(new AnimKey("wing1", 0.1f));

		//----WING1----
		final GameObject pinkie_wing1 = new GameObject("wing1", false);
		pinkie_wing1.objectSubFolder = "pinkie";
		pinkie_wing1.pongAnimation = true;
		pinkie_wing1.setPositionXY(383, 101);
		pinkie_wing1.AddAnim(new AnimKey("wing1", 0.1f));
		
		//----EAR0----
		final GameObject pinkie_ear0 = new GameObject("ear", false, true);
		pinkie_ear0.objectSubFolder = "pinkie";
		pinkie_ear0.setOnCreateFilter(true);
		pinkie_ear0.setPositionXY(298, 320);
		pinkie_ear0.setOriginX(12);
		pinkie_ear0.setOriginY(14);
		
		RotateTo pinkie_ear0_rotate1 = RotateTo.$(-5, 4);
		RotateTo pinkie_ear0_rotate2 = RotateTo.$(5, 4);		
		pinkie_ear0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(pinkie_ear0_rotate1, Delay.$(pinkie_ear0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		//----EAR1----
		final GameObject pinkie_ear1 = new GameObject("ear", false, true);
		pinkie_ear1.objectSubFolder = "pinkie";
		pinkie_ear1.setOnCreateFilter(true);
		pinkie_ear1.setPositionXY(192, 329);
		pinkie_ear1.setOriginX(12);
		pinkie_ear1.setOriginY(16);
		pinkie_ear1.scaleX = -1;
		pinkie_ear1.rotation = 5;

		RotateTo pinkie_ear1_rotate1 = RotateTo.$(10, 4);
		RotateTo pinkie_ear1_rotate2 = RotateTo.$(0, 4);		
		pinkie_ear1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(pinkie_ear1_rotate1, Delay.$(pinkie_ear1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);		
		
		//----BODY----
		final GameObject pinkie = new GameObject("body", false, true);
		pinkie.objectSubFolder = "pinkie";
		pinkie.setPositionXY(106, 0);
		
		//----EYE----
		final GameObject pinkie_eyes = new GameObject("eye", false, true);
		pinkie_eyes.pongAnimation = true;
		pinkie_eyes.objectSubFolder = "pinkie";
		pinkie_eyes.setPositionXY(203, 197);
		pinkie_eyes.setWidth(163);
		pinkie_eyes.setHeight(102);
		pinkie_eyes.pauseAnim = 10;
		pinkie_eyes.AddAnim(new AnimKey("eye", 0.1f));

		pinkie_container.addActor(pinkie_wing0);
		pinkie_container.addActor(pinkie_wing1);
		pinkie_container.addActor(pinkie_ear1);
		pinkie_container.addActor(pinkie_ear0);
		pinkie_container.addActor(pinkie);
		pinkie_container.addActor(pinkie_eyes);	
		
        return pinkie_container;
	}

	@Override
	public void enablePonyAnim() {
		//----------------PINKIE-----------------------
		GameObject pinkie_container = (GameObject) ponyObj;
		
		MoveTo pinkie_body_moveTo0 = new MoveTo (225, 177, 2, 0, false);
		MoveTo pinkie_body_moveTo1 = new MoveTo (225, 187, 2, 0, false);
		pinkie_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		pinkie_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());		
		pinkie_container.action(Forever.$(Sequence.$(pinkie_body_moveTo0, pinkie_body_moveTo1)));
		
		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = true;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = true;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		ScaleTo shadow_scale1 = ScaleTo.$(1, 1, 2);
		ScaleTo shadow_scale2 = ScaleTo.$(0.93f, 0.93f, 2);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));	
		
	}

	@Override
	public void disablePonyAnim() {
		//----------------PINKIE-----------------------
		GameObject pinkie_container = (GameObject) ponyObj;
		pinkie_container.clearActions();

		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = false;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = false;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");	
		shadow.clearActions();
		
	}

}
