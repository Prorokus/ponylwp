package lw_engine.hover_pony_lw.pony_container;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.GameObjectAnimation.AnimKey;

public class RainbowDashContainer extends PonyContainer {

	public RainbowDashContainer(String name) {
		super(name,"rainbow");
		// TODO Auto-generated constructor stub
	}

	@Override
	public GameObject initializePony() {
		//----------------RAINBOW-----------------------
		final GameObject rainbow_container = new GameObject("rainbow_container",false);
		rainbow_container.setPositionXY(237, 180);
		
		ImageCache.load(name, imgName);
		
		//----WING0----
		final GameObject rainbow_wing0 = new GameObject("wing0", false);
		rainbow_wing0.objectSubFolder = "rainbow";
		rainbow_wing0.pongAnimation = true;
		rainbow_wing0.setPositionXY(149, 95);
		rainbow_wing0.scaleX = -1;
		rainbow_wing0.AddAnim(new AnimKey("wing1", 0.1f));

		//----WING1----
		final GameObject rainbow_wing1 = new GameObject("wing1", false);
		rainbow_wing1.objectSubFolder = "rainbow";
		rainbow_wing1.pongAnimation = true;
		rainbow_wing1.setPositionXY(329, 95);
		rainbow_wing1.AddAnim(new AnimKey("wing1", 0.1f));
		
		//----EAR0----
		final GameObject rainbow_ear0 = new GameObject("ear", false, true);
		rainbow_ear0.objectSubFolder = "rainbow";
		rainbow_ear0.setOnCreateFilter(true);
		rainbow_ear0.setPositionXY(265, 331);
		rainbow_ear0.setOriginX(12);
		rainbow_ear0.setOriginY(14);
		
		RotateTo rainbow_ear0_rotate1 = RotateTo.$(-5, 4);
		RotateTo rainbow_ear0_rotate2 = RotateTo.$(5, 4);		
		rainbow_ear0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(rainbow_ear0_rotate1, Delay.$(rainbow_ear0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		//----EAR1----
		final GameObject rainbow_ear1 = new GameObject("ear", false, true);
		rainbow_ear1.objectSubFolder = "rainbow";
		rainbow_ear1.setOnCreateFilter(true);
		rainbow_ear1.setPositionXY(205, 339);
		rainbow_ear1.setOriginX(12);
		rainbow_ear1.setOriginY(16);
		rainbow_ear1.scaleX = -1;
		rainbow_ear1.rotation = 5;

		RotateTo rainbow_ear1_rotate1 = RotateTo.$(10, 4);
		RotateTo rainbow_ear1_rotate2 = RotateTo.$(0, 4);		
		rainbow_ear1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(rainbow_ear1_rotate1, Delay.$(rainbow_ear1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);		
		
		//----BODY----
		final GameObject rainbow = new GameObject("body", false, true);
		rainbow.objectSubFolder = "rainbow";
		rainbow.setPositionXY(92, 0);
		
		//----EYE----
		final GameObject rainbow_eyes = new GameObject("eye", false, true);
		rainbow_eyes.pongAnimation = true;
		rainbow_eyes.objectSubFolder = "rainbow";
		rainbow_eyes.setPositionXY(172, 192);
		rainbow_eyes.setWidth(163);
		rainbow_eyes.setHeight(77);
		rainbow_eyes.pauseAnim = 10;
		rainbow_eyes.AddAnim(new AnimKey("eye", 0.1f));

		rainbow_container.addActor(rainbow_wing0);
		rainbow_container.addActor(rainbow_wing1);
		rainbow_container.addActor(rainbow_ear1);
		rainbow_container.addActor(rainbow);
		rainbow_container.addActor(rainbow_ear0);
		rainbow_container.addActor(rainbow_eyes);
		
		return rainbow_container;
	}

	@Override
	public void enablePonyAnim() {
		//----------------RAINBOW-----------------------
		GameObject rainbow_container = (GameObject) ponyObj;
		
		MoveTo rainbow_body_moveTo0 = new MoveTo (237, 175, 2, 0, false);
		MoveTo rainbow_body_moveTo1 = new MoveTo (237, 185, 2, 0, false);
		rainbow_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		rainbow_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());		
		rainbow_container.action(Forever.$(Sequence.$(rainbow_body_moveTo0, rainbow_body_moveTo1)));	
		
		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = true;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = true;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		ScaleTo shadow_scale1 = ScaleTo.$(1, 1, 2);
		ScaleTo shadow_scale2 = ScaleTo.$(0.93f, 0.93f, 2);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));	
		
	}

	@Override
	public void disablePonyAnim() {
		//----------------RAINBOW-----------------------
		GameObject rainbow_container = (GameObject) ponyObj;
		rainbow_container.clearActions();

		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = false;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = false;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");	
		shadow.clearActions();
		
	}

}
