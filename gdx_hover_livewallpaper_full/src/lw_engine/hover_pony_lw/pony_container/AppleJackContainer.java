package lw_engine.hover_pony_lw.pony_container;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.GameObjectAnimation.AnimKey;

public class AppleJackContainer extends PonyContainer {
	
	public AppleJackContainer(String name) {
		super(name,"aplj");
	}

	@Override
	public GameObject initializePony() {
		//----------------APPLE JACK-----------------------
		final GameObject apple_jack_container = new GameObject("apple_jack_container",false);
		apple_jack_container.setPositionXY(237, 85);
		
		ImageCache.load(name, imgName);
		
		//----WING0----
		final GameObject apple_jack_wing0 = new GameObject("wing0", false);
		apple_jack_wing0.objectSubFolder = "aplj";
		apple_jack_wing0.pongAnimation = true;
		apple_jack_wing0.setPositionXY(164, 187);
		apple_jack_wing0.scaleX = -1;
		apple_jack_wing0.AddAnim(new AnimKey("wing1", 0.1f));

		//----WING1----
		final GameObject apple_jack_wing1 = new GameObject("wing1", false);
		apple_jack_wing1.objectSubFolder = "aplj";
		apple_jack_wing1.pongAnimation = true;
		apple_jack_wing1.setPositionXY(322, 187);
		apple_jack_wing1.AddAnim(new AnimKey("wing1", 0.1f));
		
		//----BODY----
		final GameObject apple_jack = new GameObject("body", false, true);
		apple_jack.objectSubFolder = "aplj";
		apple_jack.setPositionXY(105, 93);
		
		//----EAR0----
		final GameObject apple_jack_ear0 = new GameObject("ear", false, true);
		apple_jack_ear0.objectSubFolder = "aplj";
		apple_jack_ear0.setOnCreateFilter(true);
		apple_jack_ear0.setPositionXY(265, 411);
		apple_jack_ear0.setOriginX(12);
		apple_jack_ear0.setOriginY(14);
		
		RotateTo apple_jack_ear0_rotate1 = RotateTo.$(-5, 4);
		RotateTo apple_jack_ear0_rotate2 = RotateTo.$(5, 4);
		apple_jack_ear0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(apple_jack_ear0_rotate1, Delay.$(apple_jack_ear0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		//----EAR1----
		final GameObject apple_jack_ear1 = new GameObject("ear", false, true);
		apple_jack_ear1.objectSubFolder = "aplj";
		apple_jack_ear1.setOnCreateFilter(true);
		apple_jack_ear1.setPositionXY(205, 411);
		apple_jack_ear1.setOriginX(12);
		apple_jack_ear1.setOriginY(16);
		apple_jack_ear1.scaleX = -1;
		apple_jack_ear1.rotation = -10;

		RotateTo apple_jack_ear1_rotate1 = RotateTo.$(-5, 4);
		RotateTo apple_jack_ear1_rotate2 = RotateTo.$(-15, 4);
		apple_jack_ear1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(apple_jack_ear1_rotate1, Delay.$(apple_jack_ear1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);		
		
		//----EYE----
		final GameObject apple_eyes = new GameObject("eye", false, true);
		apple_eyes.pongAnimation = true;
		apple_eyes.objectSubFolder = "aplj";
		apple_eyes.setPositionXY(170, 241);
		apple_eyes.setWidth(202);
		apple_eyes.setHeight(164);
		apple_eyes.pauseAnim = 10;
		apple_eyes.AddAnim(new AnimKey("eye", 0.1f));

		apple_jack_container.addActor(apple_jack_wing0);
		apple_jack_container.addActor(apple_jack_wing1);
		apple_jack_container.addActor(apple_jack);
		apple_jack_container.addActor(apple_jack_ear0);
		apple_jack_container.addActor(apple_jack_ear1);
		apple_jack_container.addActor(apple_eyes);
		
		//----SHADOW----
		final GameObject shadow = new GameObject("shadow", true, true);
		shadow.setPositionXY(310, 85);
		shadow.setOriginX(173);
		shadow.setOriginY(39);		
		
		return apple_jack_container;
	}

	@Override
	public void enablePonyAnim() {
		//----------------APPLE JACK-----------------------
		GameObject apple_jack_container = (GameObject) ponyObj;
		
		MoveTo apple_jack_body_moveTo0 = new MoveTo (237, 80, 2, 0, false);
		MoveTo apple_jack_body_moveTo1 = new MoveTo (237, 90, 2, 0, false);
		apple_jack_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		apple_jack_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());
		apple_jack_container.action(Forever.$(Sequence.$(apple_jack_body_moveTo0, apple_jack_body_moveTo1)));	
		
		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = true;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = true;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		ScaleTo shadow_scale1 = ScaleTo.$(1, 1, 2);
		ScaleTo shadow_scale2 = ScaleTo.$(0.93f, 0.93f, 2);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));	
		
	}

	@Override
	public void disablePonyAnim() {
		//----------------APPLE JACK-----------------------
		GameObject apple_jack_container = (GameObject) ponyObj;
		apple_jack_container.clearActions();

		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = false;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = false;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");	
		shadow.clearActions();
		
	}

}
