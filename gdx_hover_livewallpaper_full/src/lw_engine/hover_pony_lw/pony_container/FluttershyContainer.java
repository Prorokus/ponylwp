package lw_engine.hover_pony_lw.pony_container;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.GameObjectAnimation.AnimKey;

public class FluttershyContainer extends PonyContainer{

	public FluttershyContainer(String name) {
		super(name, "fltsh");
		// TODO Auto-generated constructor stub
	}

	@Override
	public GameObject initializePony() {
		//----------------FLTSH-----------------------
		final GameObject fltsh_container = new GameObject("fltsh_container",false);
		fltsh_container.setPositionXY(240, 191);
	
		ImageCache.load(name, imgName);
		
		//----WING0----
		final GameObject fltsh_wing0 = new GameObject("wing0", false);
		fltsh_wing0.objectSubFolder = "fltsh";
		fltsh_wing0.pongAnimation = true;
		fltsh_wing0.setPositionXY(151, 84);
		fltsh_wing0.scaleX = -1;
		fltsh_wing0.AddAnim(new AnimKey("wing1", 0.1f));

		//----WING1----
		final GameObject fltsh_wing1 = new GameObject("wing1", false);
		fltsh_wing1.objectSubFolder = "fltsh";
		fltsh_wing1.pongAnimation = true;
		fltsh_wing1.setPositionXY(337, 84);
		fltsh_wing1.AddAnim(new AnimKey("wing1", 0.1f));
		
		
		//----EAR0----
		final GameObject fltsh_ear0 = new GameObject("ear", false, true);
		fltsh_ear0.objectSubFolder = "fltsh";
		fltsh_ear0.setOnCreateFilter(true);
		fltsh_ear0.setPositionXY(271, 265);
		fltsh_ear0.setOriginX(12);
		fltsh_ear0.setOriginY(14);
		
		RotateTo fltsh_ear0_rotate1 = RotateTo.$(-5, 4);
		RotateTo fltsh_ear0_rotate2 = RotateTo.$(5, 4);		
		fltsh_ear0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(fltsh_ear0_rotate1, Delay.$(fltsh_ear0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		//----EAR1----
		final GameObject fltsh_ear1 = new GameObject("ear", false, true);
		fltsh_ear1.objectSubFolder = "fltsh";
		fltsh_ear1.setOnCreateFilter(true);
		fltsh_ear1.setPositionXY(210, 286);
		fltsh_ear1.setOriginX(12);
		fltsh_ear1.setOriginY(16);
		fltsh_ear1.scaleX = -1;
		fltsh_ear1.rotation = 5;

		RotateTo fltsh_ear1_rotate1 = RotateTo.$(10, 4);
		RotateTo fltsh_ear1_rotate2 = RotateTo.$(0, 4);		
		fltsh_ear1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(fltsh_ear1_rotate1, Delay.$(fltsh_ear1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);		
		
		//----BODY----
		final GameObject fltsh = new GameObject("body", false, true);
		fltsh.objectSubFolder = "fltsh";
		fltsh.setPositionXY(103, 0);
		
		//----EYE----
		final GameObject fltsh_eyes = new GameObject("eye", false, true);
		fltsh_eyes.pongAnimation = true;
		fltsh_eyes.objectSubFolder = "fltsh";
		fltsh_eyes.setPositionXY(182, 153);
		fltsh_eyes.setWidth(198);
		fltsh_eyes.setHeight(133);
		fltsh_eyes.pauseAnim = 10;
		fltsh_eyes.AddAnim(new AnimKey("eye", 0.1f));

		fltsh_container.addActor(fltsh_wing0);
		fltsh_container.addActor(fltsh_wing1);
		fltsh_container.addActor(fltsh_ear1);
		fltsh_container.addActor(fltsh);
		fltsh_container.addActor(fltsh_ear0);
		fltsh_container.addActor(fltsh_eyes);	
		
        return fltsh_container;
	}

	@Override
	public void enablePonyAnim() {
		//----------------FLTSH-----------------------
		GameObject fltsh_container = (GameObject) ponyObj;
		
		MoveTo fltsh_body_moveTo0 = new MoveTo (240, 186, 2, 0, false);
		MoveTo fltsh_body_moveTo1 = new MoveTo (240, 196, 2, 0, false);
		fltsh_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		fltsh_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());		
		fltsh_container.action(Forever.$(Sequence.$(fltsh_body_moveTo0, fltsh_body_moveTo1)));	
		
		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = true;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = true;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		ScaleTo shadow_scale1 = ScaleTo.$(1, 1, 2);
		ScaleTo shadow_scale2 = ScaleTo.$(0.93f, 0.93f, 2);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));	
		
	}

	@Override
	public void disablePonyAnim() {
		//----------------FLTSH-----------------------
		GameObject fltsh_container = (GameObject) ponyObj;
		fltsh_container.clearActions();

		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = false;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = false;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");	
		shadow.clearActions();
		
	}

}
