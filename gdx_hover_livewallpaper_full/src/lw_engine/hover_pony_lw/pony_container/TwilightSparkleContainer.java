package lw_engine.hover_pony_lw.pony_container;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.GameObjectAnimation.AnimKey;

public class TwilightSparkleContainer extends PonyContainer {

	public TwilightSparkleContainer(String name) {
		super(name,"sparkle");
		// TODO Auto-generated constructor stub
	}

	@Override
	public GameObject initializePony() {
		//----------------SPARKLE-----------------------
		final GameObject sparkle_container = new GameObject("sparkle_container",false);
		sparkle_container.setPositionXY(223, 194);
		
		ImageCache.load(name, imgName);
		
		//----WING0----
		final GameObject sparkle_wing0 = new GameObject("wing0", false);
		sparkle_wing0.objectSubFolder = "sparkle";
		sparkle_wing0.pongAnimation = true;
		sparkle_wing0.setPositionXY(149, 69);
		sparkle_wing0.scaleX = -1;
		sparkle_wing0.AddAnim(new AnimKey("wing1", 0.1f));

		//----WING1----
		final GameObject sparkle_wing1 = new GameObject("wing1", false);
		sparkle_wing1.objectSubFolder = "sparkle";
		sparkle_wing1.pongAnimation = true;
		sparkle_wing1.setPositionXY(386, 69);
		sparkle_wing1.AddAnim(new AnimKey("wing1", 0.1f));
		
		//----EAR0----
		final GameObject sparkle_ear0 = new GameObject("ear", false, true);
		sparkle_ear0.objectSubFolder = "sparkle";
		sparkle_ear0.setOnCreateFilter(true);
		sparkle_ear0.setPositionXY(284, 319);
		sparkle_ear0.setOriginX(12);
		sparkle_ear0.setOriginY(14);
		
		RotateTo sparkle_ear0_rotate1 = RotateTo.$(-5, 4);
		RotateTo sparkle_ear0_rotate2 = RotateTo.$(5, 4);		
		sparkle_ear0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(sparkle_ear0_rotate1, Delay.$(sparkle_ear0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		//----EAR1----
		final GameObject sparkle_ear1 = new GameObject("ear", false, true);
		sparkle_ear1.objectSubFolder = "sparkle";
		sparkle_ear1.setOnCreateFilter(true);
		sparkle_ear1.setPositionXY(188, 316);
		sparkle_ear1.setOriginX(12);
		sparkle_ear1.setOriginY(16);
		sparkle_ear1.scaleX = -1;
		sparkle_ear1.rotation = 5;

		RotateTo sparkle_ear1_rotate1 = RotateTo.$(10, 4);
		RotateTo sparkle_ear1_rotate2 = RotateTo.$(0, 4);		
		sparkle_ear1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(sparkle_ear1_rotate1, Delay.$(sparkle_ear1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);		
		
		//----BODY----
		final GameObject sparkle = new GameObject("body", false, true);
		sparkle.objectSubFolder = "sparkle";
		sparkle.setPositionXY(109, 0);
		
		//----EYE----
		final GameObject sparkle_eyes = new GameObject("eye", false, true);
		sparkle_eyes.pongAnimation = true;
		sparkle_eyes.objectSubFolder = "sparkle";
		sparkle_eyes.setPositionXY(185, 178);
		sparkle_eyes.setWidth(186);
		sparkle_eyes.setHeight(112);
		sparkle_eyes.pauseAnim = 10;
		sparkle_eyes.AddAnim(new AnimKey("eye", 0.1f));

		sparkle_container.addActor(sparkle_wing0);
		sparkle_container.addActor(sparkle_wing1);
		sparkle_container.addActor(sparkle_ear1);
		sparkle_container.addActor(sparkle_ear0);
		sparkle_container.addActor(sparkle);
		sparkle_container.addActor(sparkle_eyes);
		
		return sparkle_container;
	}

	@Override
	public void enablePonyAnim() {
		//----------------SPARKLE-----------------------
		GameObject sparkle_container = (GameObject) ponyObj;
		
		MoveTo sparkle_body_moveTo0 = new MoveTo (223, 189, 2, 0, false);
		MoveTo sparkle_body_moveTo1 = new MoveTo (223, 199, 2, 0, false);
		sparkle_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		sparkle_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());		
		sparkle_container.action(Forever.$(Sequence.$(sparkle_body_moveTo0, sparkle_body_moveTo1)));	
		
		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = true;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = true;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		ScaleTo shadow_scale1 = ScaleTo.$(1, 1, 2);
		ScaleTo shadow_scale2 = ScaleTo.$(0.93f, 0.93f, 2);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));	
		
	}

	@Override
	public void disablePonyAnim() {
		//----------------SPARKLE-----------------------
		GameObject sparkle_container = (GameObject) ponyObj;
		sparkle_container.clearActions();

		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = false;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = false;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");	
		shadow.clearActions();
		
	}

}
