package lw_engine.hover_pony_lw.pony_container;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;

public class HoverPonySelector
{

	private String name;
	public PonyContainer ponyContainer;
	
	public HoverPonySelector(String name)
	{
		this.name = name;
	}
	
	public void enablePonyAnim()
	{
		ponyContainer.enablePonyAnim();
	}
	
	public void disablePonyAnim()
	{
		ponyContainer.disablePonyAnim();
	}
	
	public void getPony(int id)
	{
		int index = -1;
		
		if (ponyContainer != null)
		{
			ponyContainer.clearContainer();
			final GameObject oldPony = (GameObject) HoverPonyLW.root.findActor(ponyContainer.ponyObj.name);
			index = HoverPonyLW.root.getActors().indexOf(oldPony);
			HoverPonyLW.root.removeActor(oldPony);
		}
		
		
		switch(id)
		{
			case 0:
				ponyContainer = getAppleJack();
				break;
			case 1:
				ponyContainer = getFluttershy();
				break;
			case 2:
				ponyContainer = getDerpy();
				break;
			case 3:
				ponyContainer = getPinkiePie();
				break;
			case 4:
				ponyContainer = getRainbowDash();
				break;
			case 5:
				ponyContainer = getRarity();
				break;
			case 6:
				ponyContainer = getTwilightSparkle();
				break;
			default:
				ponyContainer = null;
				break;
		}
		
		if(index != -1)
		{	
			HoverPonyLW.root.getRoot().addActorAt(index, ponyContainer.ponyObj);
		}
		else
		{
			HoverPonyLW.root.addActor(ponyContainer.ponyObj);
		}
	}
	
	private PonyContainer getAppleJack()
	{
		return new AppleJackContainer(name);
	}
	
	private PonyContainer getRarity()
	{
		return new RarityContainer(name);
	}
	
	private PonyContainer getFluttershy()
	{
		return new FluttershyContainer(name);
	}
	
	private PonyContainer getRainbowDash()
	{
		return new RainbowDashContainer(name);
	}
	
	private PonyContainer getPinkiePie()
	{
		return new PinkiePieContainer(name);
	}
	
	private PonyContainer getTwilightSparkle()
	{
		return new TwilightSparkleContainer(name);
	}
	
	private PonyContainer getDerpy()
	{
		return new DerpyContainer(name);
	}
}
