package lw_engine.hover_pony_lw.pony_container;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;
import lwp_engine.GameObjectAnimation.AnimKey;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.RandomLighten;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

public class DerpyContainer extends PonyContainer {

	public DerpyContainer(String name) {
		super(name,"derpy");
		// TODO Auto-generated constructor stub
	}

	@Override
	public GameObject initializePony() {
		  //----------------DERPY-----------------------
		  final GameObject derpy_container = new GameObject("derpy_container",false);
		  derpy_container.setPositionXY(265, 231);
		  derpy_container.visible = true;
		  
		  derpy_container.setOriginX(228);
		  derpy_container.setOriginY(37);
		  
		  ImageCache.load(name, imgName);
		  
		  //----TAIL----
		  final GameObject tail = new GameObject("tail", false);
		  tail.objectSubFolder = "derpy";
		  tail.pongAnimation = true;
		  tail.setPositionXY(72, 135);
		  tail.AddAnim(new AnimKey("tail", 1));
		  
		  //----WING0----
		  final GameObject derpy_wing0 = new GameObject("wing0", false);
		  derpy_wing0.objectSubFolder = "derpy";
		  derpy_wing0.pongAnimation = true;
		  derpy_wing0.pauseAnim = 10;
		  derpy_wing0.setPositionXY(284, 101);
		  derpy_wing0.AddAnim(new AnimKey("wing0", 0.2f));

		  //----WING1----
		  final GameObject derpy_wing1 = new GameObject("wing1", false);
		  derpy_wing1.objectSubFolder = "derpy";
		  derpy_wing1.pongAnimation = true;
		  derpy_wing1.pauseAnim = 10;
		  derpy_wing1.setPositionXY(144, 130);
		  derpy_wing1.AddAnim(new AnimKey("wing1", 0.2f));
		  
		  //----WING1-BACK----
		  final GameObject wing1_back = new GameObject("wing1-back", false, true);
		  wing1_back.objectSubFolder = "derpy";
		  wing1_back.setPositionXY(220, 151);
		  
		  //----RAIN----
		  final GameObject rain = new GameObject("p2", false);
		  rain.setPositionXY(0,0);
	      rain.setOriginX(75);
		  rain.setOriginY(80);
		  rain.createParticle("p2", false);		  
		  
		  //----BODY----
		  final GameObject derpy = new GameObject("body", false, true);
		  derpy.objectSubFolder = "derpy";
		  derpy.setOnCreateFilter(true);
		  derpy.setPositionXY(17, 22);
		  
		  //----EYE----
		  final GameObject derpy_eyes = new GameObject("eye", false, true);
		  derpy_eyes.pongAnimation = true;
		  derpy_eyes.objectSubFolder = "derpy";
		  derpy_eyes.setPositionXY(216, 215);
		  derpy_eyes.setWidth(75);
		  derpy_eyes.setHeight(49);
		  derpy_eyes.pauseAnim = 10;
		  derpy_eyes.AddAnim(new AnimKey("eye", 0.1f));

		  //----LIGHTNING0----
		  final GameObject lightning0 = new GameObject("lightning0", false, true);
		  lightning0.objectSubFolder = "derpy";
		  lightning0.setPositionXY(66, 171);
		  lightning0.color.a = 0;
		  
		  RandomLighten randLightning = new RandomLighten (0.0f, 1.0f, 0.0f, 0.12f, 0.5f);
		  lightning0.action(Forever.$(Sequence.$(randLightning, Delay.$(4))));

		  //----LIGHTNING1----
		  final GameObject lightning1 = new GameObject("lightning1", false, true);
		  lightning1.objectSubFolder = "derpy";
		  lightning1.setPositionXY(1, 42);
		  lightning1.color.a = 0;
		  
		  lightning1.action(Delay.$(Forever.$(Sequence.$(randLightning, Delay.$(6))), 0.25f));

		  //----LIGHTNING2----
		  final GameObject lightning2 = new GameObject("lightning2", false, true);
		  lightning2.objectSubFolder = "derpy";
		  lightning2.setPositionXY(78, 1);
		  lightning2.color.a = 0;
		  
		  lightning2.action(Delay.$(Forever.$(Sequence.$(randLightning, Delay.$(3))), 1));

		  //----LIGHTNING3----
		  final GameObject lightning3 = new GameObject("lightning3", false, true);
		  lightning3.objectSubFolder = "derpy";
		  lightning3.setPositionXY(122, 138);
		  lightning3.color.a = 0;
		  
		  lightning3.action(Delay.$(Forever.$(Sequence.$(randLightning, Delay.$(5))), 1.5f));

		  //----LIGHTNING4----
		  final GameObject lightning4 = new GameObject("lightning4", false, true);
		  lightning4.objectSubFolder = "derpy";
		  lightning4.setPositionXY(319, 23);
		  lightning4.color.a = 0;
		  
		  lightning4.action(Delay.$(Forever.$(Sequence.$(randLightning, Delay.$(5))), 2));

		  //----LIGHTNING5----
		  final GameObject lightning5 = new GameObject("lightning5", false, true);
		  lightning5.objectSubFolder = "derpy";
		  lightning5.setPositionXY(402, 14);
		  lightning5.color.a = 0;
		  
		  lightning5.action(Delay.$(Forever.$(Sequence.$(randLightning, Delay.$(3))), 0.5f));
		  
		  derpy_container.addActor(rain);
		  derpy_container.addActor(derpy);
		  derpy_container.addActor(tail);
		  derpy_container.addActor(derpy_eyes);
		  derpy_container.addActor(derpy_wing0);
		  derpy_container.addActor(derpy_wing1);
		  derpy_container.addActor(wing1_back); 
		  
		  derpy_container.addActor(lightning0);
		  derpy_container.addActor(lightning1);
		  derpy_container.addActor(lightning2);
		  derpy_container.addActor(lightning3);
		  derpy_container.addActor(lightning4);
		  derpy_container.addActor(lightning5);
		  
		  return derpy_container;
	}

	@Override
	public void enablePonyAnim() {
		//----------------DERPY-----------------------
		GameObject derpy_container = (GameObject) ponyObj;
		
   	    MoveTo derpy_body_moveTo0 = new MoveTo (265, 236, 2, 0, false);
		MoveTo derpy_body_moveTo1 = new MoveTo (265, 226, 2, 0, false);
		derpy_body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		derpy_body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());  
		derpy_container.action(Forever.$(Sequence.$(derpy_body_moveTo0, derpy_body_moveTo1)));	
		
		ScaleTo derpy_scale1 = ScaleTo.$(1.005f, 0.995f, 2);
		ScaleTo derpy_scale2 = ScaleTo.$(1.0f, 1.0f, 2);
		derpy_container.action(Forever.$(Sequence.$(derpy_scale1, derpy_scale2)));   
		
		//----TAIL----
		GameObject tail = (GameObject) ponyObj.findActor("tail");
		tail.animPaused = false;
		
		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = true;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = true;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		
		ScaleTo shadow_scale1 = ScaleTo.$(1, 1, 2);
		ScaleTo shadow_scale2 = ScaleTo.$(0.93f, 0.93f, 2);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));	
		
	}

	@Override
	public void disablePonyAnim() {
		//----------------DERPY-----------------------
		GameObject derpy_container = (GameObject) ponyObj;
		derpy_container.clearActions();
		derpy_container.scaleX = 1;
		derpy_container.scaleY = 1;

		//----TAIL----
		GameObject tail = (GameObject) ponyObj.findActor("tail");
		tail.animPaused = true;

		//----WING0----
		GameObject wing0 = (GameObject) ponyObj.findActor("wing0");
		wing0.visible = false;
		
		//----WING1----
		GameObject wing1 = (GameObject) ponyObj.findActor("wing1");
		wing1.visible = false;
		
		//----SHADOW----		
		GameObject shadow = (GameObject) HoverPonyLW.getRoot().findActor("shadow");	
		shadow.clearActions();
		
	}

}
