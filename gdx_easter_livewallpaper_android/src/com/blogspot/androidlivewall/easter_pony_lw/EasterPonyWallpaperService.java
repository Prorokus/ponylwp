package com.blogspot.androidlivewall.easter_pony_lw;

import lw_engine.easter_pony_lw.MyEasterPonyLW;

import com.badlogic.gdx.backends.android.livewallpaper.AndroidApplication;
import com.eightbitmage.gdxlw.LibdgxWallpaperService;

public class EasterPonyWallpaperService extends LibdgxWallpaperService {
	
	@Override
	public Engine onCreateEngine() {
		//android.os.Debug.waitForDebugger();
		// TODO Auto-generated method stub
		return new EasterPonyWallpaperEngine(this);
	}
	
	public class EasterPonyWallpaperEngine extends LibdgxWallpaperEngine
	{

		public EasterPonyWallpaperEngine(
				LibdgxWallpaperService libdgxWallpaperService) {
			super(libdgxWallpaperService);
			// TODO Auto-generated constructor stub
		} 
		
		protected void initialize(AndroidApplication androidApplicationLW)
		{
			MyEasterPonyLW app = new MyEasterPonyLW();
			setWallpaperListener(app);
			androidApplicationLW.initialize(app,false);
		}
	}
}