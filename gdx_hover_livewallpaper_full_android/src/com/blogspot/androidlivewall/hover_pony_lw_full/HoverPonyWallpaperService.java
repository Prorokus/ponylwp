package com.blogspot.androidlivewall.hover_pony_lw_full;

import lw_engine.hover_pony_lw.HoverPonyLW;

import com.badlogic.gdx.backends.android.livewallpaper.AndroidApplication;
import com.eightbitmage.gdxlw.LibdgxWallpaperService;

public class HoverPonyWallpaperService extends LibdgxWallpaperService {
	
	@Override
	public Engine onCreateEngine() {
		//android.os.Debug.waitForDebugger();
		// TODO Auto-generated method stub
		return new HoverPonyWallpaperEngine(this);
	}
	
	public class HoverPonyWallpaperEngine extends LibdgxWallpaperEngine
	{

		public HoverPonyWallpaperEngine(
				LibdgxWallpaperService libdgxWallpaperService) {
			super(libdgxWallpaperService);
			// TODO Auto-generated constructor stub
		} 
		
		protected void initialize(AndroidApplication androidApplicationLW)
		{
			HoverPonyLW app = new HoverPonyLW();
			setWallpaperListener(app);
			androidApplicationLW.initialize(app,false);
		}
	}
}