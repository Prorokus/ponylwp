package lw_engine.my_pony_lw;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.jogl.JoglApplication;


public class PonyLWDesktop {
	public static void main(String[] argv)
	{
		JoglApplication app = new JoglApplication((ApplicationListener) new MyPonyLW(), "MyPony LW App", 960, 854,  false);
	}

}
