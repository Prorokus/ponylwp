package lw_engine.my_pony_lw;

import lwp_engine.LWPApplication;

public class MyPonyLW extends LWPApplication{
	
	public final String MY_PONY_PREFS = "com.blogspot.androidlivewall.my_pony_lw_preferences";
	
	public static boolean showParticles = true;
	public static boolean animetePony = true;
	
	protected String getPrefsName()
	{
		return MY_PONY_PREFS;
	}

	protected void readAllPrefs()
	{
		wallNotMovable = prefs.getBoolean("set_pony_center",false);
		selectedWall = Integer.parseInt(prefs.getString("selected_pony","0"));
		enableParallax = prefs.getBoolean("set_pony_parallax",false);
		showParticles = prefs.getBoolean("set_pony_sparkles",true);
		animetePony = prefs.getBoolean("set_pony_anim",true);
	}
	
	protected void loadWallScreen(boolean ... params)
	{
		if ((selectedWall == prevSelectedWall) && params[0] != true)
		{
			return;
		}
		else
		{
			prevSelectedWall = selectedWall;
			if (wallScreen != null)
			{
				wallScreen.destroy();
				wallScreen = null;
			}
		}
		
		switch(selectedWall)
		{
			case 0:
				wallScreen = new FluttershyScreen("fluttershy");
				break;
			case 1:
				wallScreen = new RainbowDashScreen("rainbowdash");
				break;
			case 2:
				wallScreen = new RarityScreen("rarity");
				break;
			case 3:
				wallScreen = new AppleJackScreen("applejack");
				break;
		}
		
		wallScreen.showParticles = showParticles;
		wallScreen.onConfigChanged();
	}

	@Override
	protected void onResumeWallpaper() {
		if (selectedWall != prevSelectedWall)
		{
			loadWallScreen();
		}
		
		if(wallScreen != null)
		{
			wallScreen.showParticles = showParticles;
			wallScreen.onConfigChanged();
		}
	}
}
