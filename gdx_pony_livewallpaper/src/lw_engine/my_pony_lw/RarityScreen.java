package lw_engine.my_pony_lw;

import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.WallpaperScreen;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.FadeOut;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

public class RarityScreen extends WallpaperScreen{
	private boolean animPony = false;
	
	public RarityScreen(String name)
	{
		super(name);
	}

	@Override
	public void initialize() {
		ImageCache.subFolderName = "main";
		final GameObject bg = new GameObject("background", true, false, 0.25f, 1.0f);
		bg.setPositionXY(0,0);
		
		//----------------------CLOUD_LIGHT0-------------------------------------------
		final GameObject cloud_light0_0 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		final GameObject cloud_light0_1 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		cloud_light0_0.setWidth(224);
		cloud_light0_0.setHeight(39);
		cloud_light0_1.setWidth(224);
		cloud_light0_1.setHeight(39);
		
		cloud_light0_0.setPositionXY(719,581);
		cloud_light0_1.setPositionXY(72,581);

		MoveTo cloud_light0_0_moveTo0 = new MoveTo (960, 581, 37, 0, false);
		MoveTo cloud_light0_0_moveTo1 = new MoveTo (-224, 581, 960, 581, 180, 37, true);
		
		MoveTo cloud_light0_1_moveTo0 = new MoveTo (960, 581, 127, 0, false);
		MoveTo cloud_light0_1_moveTo1 = new MoveTo (-224, 581, 960, 581, 180, 127, true);

		cloud_light0_0.action(cloud_light0_0_moveTo0);
		cloud_light0_0.action(cloud_light0_0_moveTo1);
		cloud_light0_1.action(cloud_light0_1_moveTo0);
		cloud_light0_1.action(cloud_light0_1_moveTo1);
		
		//----------------------CLOUD_LIGHT1-------------------------------------------
		final GameObject cloud_light1_0 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		final GameObject cloud_light1_1 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		cloud_light1_0.setWidth(360);
		cloud_light1_0.setHeight(62);
		cloud_light1_1.setWidth(360);
		cloud_light1_1.setHeight(62);
		
		cloud_light1_0.setPositionXY(547,813);
		cloud_light1_1.setPositionXY(-110,813);

		MoveTo cloud_light1_0_moveTo0 = new MoveTo (960, 813, 56, 0, false);
		MoveTo cloud_light1_0_moveTo1 = new MoveTo (-360, 813, 960, 813, 180, 56, true);
		
		MoveTo cloud_light1_1_moveTo0 = new MoveTo (960, 813, 146, 0, false);
		MoveTo cloud_light1_1_moveTo1 = new MoveTo (-360, 813, 960, 813, 180, 146, true);

		cloud_light1_0.action(cloud_light1_0_moveTo0);
		cloud_light1_0.action(cloud_light1_0_moveTo1);
		cloud_light1_1.action(cloud_light1_1_moveTo0);
		cloud_light1_1.action(cloud_light1_1_moveTo1);
		
		//----------------------CLOUD_LIGHT2-------------------------------------------
		final GameObject cloud_light2_0 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		final GameObject cloud_light2_1 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		cloud_light2_0.setWidth(360);
		cloud_light2_0.setHeight(62);
		cloud_light2_0.setOriginX(180);
		cloud_light2_0.setScaleX(-1);
		
		cloud_light2_1.setWidth(360);
		cloud_light2_1.setHeight(62);
		cloud_light2_1.setOriginX(180);
		cloud_light2_1.setScaleX(-1);
		
		cloud_light2_0.setPositionXY(24,741);
		cloud_light2_1.setPositionXY(-360,741);

		MoveTo cloud_light2_0_moveTo0 = new MoveTo (1320, 741, 128, 0, false);
		MoveTo cloud_light2_0_moveTo1 = new MoveTo (-360, 741, 1320, 741, 180, 128, true);
		
		MoveTo cloud_light2_1_moveTo0 = new MoveTo (-360, 741, 1320, 741, 180, 38, true);

		cloud_light2_0.action(cloud_light2_0_moveTo0);
		cloud_light2_0.action(cloud_light2_0_moveTo1);
		cloud_light2_1.action(cloud_light2_1_moveTo0);
		
		//----------------------CLOUD0-------------------------------------------
		final GameObject cloud0_0 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		final GameObject cloud0_1 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		cloud0_0.setWidth(200);
		cloud0_0.setHeight(112);
		cloud0_1.setWidth(200);
		cloud0_1.setHeight(112);
		
		cloud0_0.setPositionXY(149, 752);
		cloud0_1.setPositionXY(-200, 752);
		
		MoveTo cloud0_0_moveTo0 = new MoveTo (960, 752, 144, 0, false);
		MoveTo cloud0_0_moveTo1 = new MoveTo (-200, 752, 960, 752, 120, 144, true);
		
		MoveTo cloud0_1_moveTo0 = new MoveTo (960, 752, 120, 54, true);

		cloud0_0.action(cloud0_0_moveTo0);
		cloud0_0.action(cloud0_0_moveTo1);
		cloud0_1.action(cloud0_1_moveTo0);
		
		//----------------------CLOUD1-------------------------------------------
		final GameObject cloud1_0 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		final GameObject cloud1_1 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		cloud1_0.setWidth(354);
		cloud1_0.setHeight(194);
		cloud1_1.setWidth(354);
		cloud1_1.setHeight(194);
		
		cloud1_0.setPositionXY(542,605);
		cloud1_1.setPositionXY(-121,605);
		
		MoveTo cloud1_0_moveTo0 = new MoveTo (960, 605, 29, 0, false);
		MoveTo cloud1_0_moveTo1 = new MoveTo (-354, 605, 960, 605, 90, 29, true);
		
		MoveTo cloud1_1_moveTo0 = new MoveTo (960, 605, 74, 0, false);
		MoveTo cloud1_1_moveTo1 = new MoveTo (-354, 605, 960, 605, 90, 74, true);

		cloud1_0.action(cloud1_0_moveTo0);
		cloud1_0.action(cloud1_0_moveTo1);
		cloud1_1.action(cloud1_1_moveTo0);
		cloud1_1.action(cloud1_1_moveTo1);
		
		//----------------------CLOUD2-------------------------------------------
		final GameObject cloud2_0 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		final GameObject cloud2_1 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		cloud2_0.setWidth(354);
		cloud2_0.setHeight(194);
		cloud2_1.setWidth(354);
		cloud2_1.setHeight(194);
		cloud2_0.setScaleX(-1);
		
		cloud2_0.setPositionXY(8,497);
		cloud2_1.setPositionXY(-354,497);
		cloud2_1.setScaleX(-1);
		
		MoveTo cloud2_0_moveTo0 = new MoveTo (1314, 497, 43, 0, false);
		MoveTo cloud2_0_moveTo1 = new MoveTo (-354, 497, 1314, 497, 60, 43, true);
		
		MoveTo cloud2_1_moveTo1 = new MoveTo (1314, 497, 60, 13, true);

		cloud2_0.action(cloud2_0_moveTo0);
		cloud2_0.action(cloud2_0_moveTo1);
		cloud2_1.action(cloud2_1_moveTo1);
		
		//----------------------FLOWER0-------------------------------------------
		final GameObject flower0 = new GameObject("flower0", true, true, 0.8f, 1.0f);
		flower0.setOnCreateFilter(true);
		flower0.setPositionXY(38, 178);
		flower0.setOriginX(47);
		flower0.setOriginY(5);

		RotateTo flower0_rotate1 = RotateTo.$(5, 4);
		RotateTo flower0_rotate2 = RotateTo.$(-10, 4);
		flower0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0.action(Forever.$(Sequence.$(flower0_rotate1, flower0_rotate2)));
		
		//----------------------FLOWER1-------------------------------------------
		final GameObject flower1 = new GameObject("flower1", true, true, 0.8f, 1.0f);
		flower1.setOnCreateFilter(true);
		flower1.setPositionXY(787, 146);
		flower1.setOriginX(32);
		flower1.setOriginY(4);

		RotateTo flower1_rotate1 = RotateTo.$(6, 8);
		RotateTo flower1_rotate2 = RotateTo.$(-11, 8);
		flower1_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1.action(Forever.$(Sequence.$(flower1_rotate1, flower1_rotate2)));
		
		//----------------------FLOWER2-------------------------------------------
		final GameObject flower2 = new GameObject("flower2", true, true, 0.8f, 1.0f);
		flower2.setOnCreateFilter(true);
		flower2.setPositionXY(831,160);
		flower2.setOriginX(39);
		flower2.setOriginY(9);

		RotateTo flower2_rotate1 = RotateTo.$(4, 6);
		RotateTo flower2_rotate2 = RotateTo.$(-8, 6);
		flower2_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2.action(Forever.$(Sequence.$(flower2_rotate1, flower2_rotate2)));
		
		//----------------------OTHER-------------------------------------------
		final GameObject grass0 = new GameObject("grass0", true, true, 0.8f, 1.0f);
		grass0.setPositionXY(0, 0);
		
		//----------------------PONY-------------------------------------------
		//----SHADOW----
		final GameObject shadow = new GameObject("shadow", true, true);
		shadow.setPositionXY(292, 100);
		shadow.setOriginX(183);
		shadow.setOriginY(31);
		
		//----BODY-CONTAINER----
		final GameObject body_container = new GameObject("body_container");
		body_container.setPositionXY(270, 117);
		body_container.setOriginX(218);
		body_container.setOriginY(6);
		
		//----BODY----
		final GameObject body = new GameObject("body", false, true);
		body.setPositionXY(121, 0);
		body.setOnCreateFilter(true);
				
		//----EYELIDS----
		final GameObject eyelids = new GameObject("eyes", false, true);
		eyelids.setPositionXY(262, 253);
		eyelids.color.a = 0.0f;
				
		FadeIn eyelids_fadeIn1 = FadeIn.$(0.1f);
		FadeOut eyelids_fadeOut1 = FadeOut.$(0.1f);
		eyelids.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn1, Delay.$(eyelids_fadeOut1, 0.3f)), 
										10)
								), 
					          5)
					   );
		
		//----TAIL0----
		final GameObject tail0 = new GameObject("hair2", false, true);
		tail0.setPositionXY(38, 81);
		tail0.setOriginX(138);
		tail0.setOriginY(139);

		//----TAIL1----
		final GameObject tail1 = new GameObject("hair1", false, true);
		tail1.setPositionXY(-3, -41);
		tail0.addActor(tail1);
		tail1.setOriginX(43);
		tail1.setOriginY(76);

		//----TAIL2----
		final GameObject tail2 = new GameObject("hair0", false, true);
		tail2.setPositionXY(-35, 4);
		tail1.addActor(tail2);
		tail2.setOriginX(39);
		tail2.setOriginY(26);		

		//----HAIR3----
		final GameObject hair3 = new GameObject("hair3", false, true);
		hair3.setPositionXY(234, 315);
		hair3.setOriginX(75);
		hair3.setOriginY(57);		

		//----LASHES----
		final GameObject lashes = new GameObject("lashes", false, true);
		lashes.setPositionXY(352, 354);

		//----HAIR4----
		final GameObject hair4 = new GameObject("hair4", false, true);
		hair4.setPositionXY(210, 132);
		hair4.setOriginX(10);
		hair4.setOriginY(178);		

		//----ADD_ACTORS----
		body_container.addActor(tail0);
		body_container.addActor(hair3);
		body_container.addActor(body);
		body_container.addActor(lashes);
		body_container.addActor(eyelids);
		body_container.addActor(hair4);
	
		animateMyPony();
	}

	public void initializeParticles() {
		GameObject main_particles = (GameObject) MyPonyLW.getRoot().findActor("p1");
		
		if (main_particles == null)
		{
			main_particles = new GameObject("p1",true);
			main_particles.setPositionXY(0,0);
			main_particles.setOriginX(480);
			main_particles.setOriginY(600);
			main_particles.createParticle("p1", false);
		}
		
		sparklesVisible = true;
		main_particles.visible = sparklesVisible;
	}
	
	private void enablePonyAnim()
	{
		int speed = 1;
		GameObject shadow = (GameObject) MyPonyLW.getRoot().findActor("shadow");
		ScaleTo shadow_scale1 = ScaleTo.$(1.01f, 1.01f, speed);
		ScaleTo shadow_scale2 = ScaleTo.$(0.99f, 0.99f, speed);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));		
		
		GameObject body_container = (GameObject) MyPonyLW.getRoot().findActor("body_container");
		ScaleTo body_scale1 = ScaleTo.$(1.0075f, 0.9925f, speed);
		ScaleTo body_scale2 = ScaleTo.$(1.0f, 1.0f, speed);
		body_container.action(Forever.$(Sequence.$(body_scale1, body_scale2)));		
		
		GameObject tail0 = (GameObject) body_container.findActor("hair2");
		RotateTo tail0_rotate1 = RotateTo.$(0.5f, speed);
		RotateTo tail0_rotate2 = RotateTo.$(-0.5f, speed);
		tail0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail0.action(Forever.$(Sequence.$(tail0_rotate1, tail0_rotate2)));
		
		GameObject tail1 = (GameObject) tail0.findActor("hair1");
		RotateTo tail1_rotate1 = RotateTo.$(1, speed);
		RotateTo tail1_rotate2 = RotateTo.$(-1, speed);
		tail1_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail1_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail1.action(Forever.$(Sequence.$(tail1_rotate1, tail1_rotate2)));
		
		GameObject tail2 = (GameObject) tail1.findActor("hair0");
		RotateTo tail2_rotate1 = RotateTo.$(2, speed);
		RotateTo tail2_rotate2 = RotateTo.$(-2, speed);
		tail2_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail2_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail2.action(Forever.$(Sequence.$(tail2_rotate1, tail2_rotate2)));
		
		GameObject hair3 = (GameObject) body_container.findActor("hair3");
		RotateTo hair3_rotate1 = RotateTo.$(0.5f, speed);
		RotateTo hair3_rotate2 = RotateTo.$(-0.5f, speed);
		hair3_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		hair3_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		hair3.action(Forever.$(Sequence.$(hair3_rotate1, hair3_rotate2)));
		
		GameObject hair4 = (GameObject) body_container.findActor("hair4");
		ScaleTo hair4_scale1 = ScaleTo.$(1, 1.01f, speed);
		ScaleTo hair4_scale2 = ScaleTo.$(1, 0.99f, speed);
		hair4_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		hair4_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		hair4.action(Forever.$(Sequence.$(hair4_scale1, hair4_scale2)));		
	}
	
	private void disablePonyAnim()
	{
		GameObject shadow = (GameObject) MyPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		
		GameObject body_container = (GameObject) MyPonyLW.getRoot().findActor("body_container");
		body_container.clearActions();		

		GameObject tail0 = (GameObject) body_container.findActor("hair2");
		tail0.clearActions();		

		GameObject tail1 = (GameObject) tail0.findActor("hair1");
		tail1.clearActions();		

		GameObject tail2 = (GameObject) tail1.findActor("hair0");
		tail2.clearActions();		

		GameObject hair3 = (GameObject) body_container.findActor("hair3");
		hair3.clearActions();		

		GameObject hair4 = (GameObject) body_container.findActor("hair4");
		hair4.clearActions();		
	}
	
	public void disableParticles()
	{
		sparklesVisible = false;
		GameObject particle = (GameObject) MyPonyLW.getRoot().findActor("p1");
		
		if(particle != null)
			particle.visible = sparklesVisible;		
	}
	
	public void animateMyPony()
	{
		if (animPony)
			enablePonyAnim();
		else
			disablePonyAnim();
	}

	@Override
	public void onConfigChanged() {
		if (sparklesVisible && !showParticles)
			disableParticles();
		else if(!sparklesVisible && showParticles)
			initializeParticles();
		if (animPony != MyPonyLW.animetePony)
		{
			animPony = MyPonyLW.animetePony;
			animateMyPony();
		}
	}
}
