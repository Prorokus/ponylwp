package lw_engine.my_pony_lw;

import lwp_engine.GameObject;
import lwp_engine.GameObjectAnimation.AnimKey;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.WallpaperScreen;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.FadeOut;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

public class RainbowDashScreen extends WallpaperScreen {
	
	private boolean animPony = false;
	
	public RainbowDashScreen(String name)
	{
		super(name);
	}

	@Override
	public void initialize() {
		ImageCache.subFolderName = "main";
		final GameObject bg = new GameObject("background", true, false);
		bg.setPositionXY(0,0);

		//----------------------CLOUD_LIGHT0-------------------------------------------
		final GameObject cloud_light0_0 = new GameObject("cloudLight0", true, true);
		final GameObject cloud_light0_1 = new GameObject("cloudLight0", true, true);
		cloud_light0_0.setWidth(224);
		cloud_light0_0.setHeight(39);
		cloud_light0_1.setWidth(224);
		cloud_light0_1.setHeight(39);
		
		cloud_light0_0.setPositionXY(719,481);
		cloud_light0_1.setPositionXY(72,481);

		MoveTo cloud_light0_0_moveTo0 = new MoveTo (960, 481, 37, 0, false);
		MoveTo cloud_light0_0_moveTo1 = new MoveTo (-224, 481, 960, 481, 180, 37, true);
		
		MoveTo cloud_light0_1_moveTo0 = new MoveTo (960, 481, 127, 0, false);
		MoveTo cloud_light0_1_moveTo1 = new MoveTo (-224, 481, 960, 481, 180, 127, true);

		cloud_light0_0.action(cloud_light0_0_moveTo0);
		cloud_light0_0.action(cloud_light0_0_moveTo1);
		cloud_light0_1.action(cloud_light0_1_moveTo0);
		cloud_light0_1.action(cloud_light0_1_moveTo1);
		
		//----------------------CLOUD_LIGHT1-------------------------------------------
		final GameObject cloud_light1_0 = new GameObject("cloudLight0", true, true);
		final GameObject cloud_light1_1 = new GameObject("cloudLight0", true, true);
		cloud_light1_0.setWidth(360);
		cloud_light1_0.setHeight(62);
		cloud_light1_1.setWidth(360);
		cloud_light1_1.setHeight(62);
		
		cloud_light1_0.setPositionXY(547,713);
		cloud_light1_1.setPositionXY(-110,713);

		MoveTo cloud_light1_0_moveTo0 = new MoveTo (960, 713, 56, 0, false);
		MoveTo cloud_light1_0_moveTo1 = new MoveTo (-360, 713, 960, 713, 180, 56, true);
		
		MoveTo cloud_light1_1_moveTo0 = new MoveTo (960, 713, 146, 0, false);
		MoveTo cloud_light1_1_moveTo1 = new MoveTo (-360, 713, 960, 713, 180, 146, true);

		cloud_light1_0.action(cloud_light1_0_moveTo0);
		cloud_light1_0.action(cloud_light1_0_moveTo1);
		cloud_light1_1.action(cloud_light1_1_moveTo0);
		cloud_light1_1.action(cloud_light1_1_moveTo1);
		
		//----------------------CLOUD_LIGHT2-------------------------------------------
		final GameObject cloud_light2_0 = new GameObject("cloudLight0", true, true);
		final GameObject cloud_light2_1 = new GameObject("cloudLight0", true, true);
		cloud_light2_0.setWidth(360);
		cloud_light2_0.setHeight(62);
		cloud_light2_0.setOriginX(180);
		cloud_light2_0.setScaleX(-1);
		
		cloud_light2_1.setWidth(360);
		cloud_light2_1.setHeight(62);
		cloud_light2_1.setOriginX(180);
		cloud_light2_1.setScaleX(-1);
		
		cloud_light2_0.setPositionXY(24,641);
		cloud_light2_1.setPositionXY(-360,641);

		MoveTo cloud_light2_0_moveTo0 = new MoveTo (1320, 641, 128, 0, false);
		MoveTo cloud_light2_0_moveTo1 = new MoveTo (-360, 641, 1320, 641, 180, 128, true);
		
		MoveTo cloud_light2_1_moveTo0 = new MoveTo (-360, 641, 1320, 641, 180, 38, true);

		cloud_light2_0.action(cloud_light2_0_moveTo0);
		cloud_light2_0.action(cloud_light2_0_moveTo1);
		cloud_light2_1.action(cloud_light2_1_moveTo0);
		
		//----------------------CLOUD0-------------------------------------------
		final GameObject cloud0_0 = new GameObject("cloud0", true, true);
		final GameObject cloud0_1 = new GameObject("cloud0", true, true);
		cloud0_0.setWidth(200);
		cloud0_0.setHeight(112);
		cloud0_1.setWidth(200);
		cloud0_1.setHeight(112);
		
		cloud0_0.setPositionXY(149, 652);
		cloud0_1.setPositionXY(-200, 652);
		
		MoveTo cloud0_0_moveTo0 = new MoveTo (960, 652, 144, 0, false);
		MoveTo cloud0_0_moveTo1 = new MoveTo (-200, 652, 960, 652, 120, 144, true);
		
		MoveTo cloud0_1_moveTo0 = new MoveTo (960, 652, 120, 54, true);

		cloud0_0.action(cloud0_0_moveTo0);
		cloud0_0.action(cloud0_0_moveTo1);
		cloud0_1.action(cloud0_1_moveTo0);
		
		//----------------------CLOUD1-------------------------------------------
		final GameObject cloud1_0 = new GameObject("cloud0", true, true);
		final GameObject cloud1_1 = new GameObject("cloud0", true, true);
		cloud1_0.setWidth(354);
		cloud1_0.setHeight(194);
		cloud1_1.setWidth(354);
		cloud1_1.setHeight(194);
		
		cloud1_0.setPositionXY(542,505);
		cloud1_1.setPositionXY(-121,505);
		
		MoveTo cloud1_0_moveTo0 = new MoveTo (960, 505, 29, 0, false);
		MoveTo cloud1_0_moveTo1 = new MoveTo (-354, 505, 960, 505, 90, 29, true);
		
		MoveTo cloud1_1_moveTo0 = new MoveTo (960, 505, 74, 0, false);
		MoveTo cloud1_1_moveTo1 = new MoveTo (-354, 505, 960, 505, 90, 74, true);

		cloud1_0.action(cloud1_0_moveTo0);
		cloud1_0.action(cloud1_0_moveTo1);
		cloud1_1.action(cloud1_1_moveTo0);
		cloud1_1.action(cloud1_1_moveTo1);
		
		//----------------------CLOUD2-------------------------------------------
		final GameObject cloud2_0 = new GameObject("cloud0", true, true);
		final GameObject cloud2_1 = new GameObject("cloud0", true, true);
		cloud2_0.setWidth(354);
		cloud2_0.setHeight(194);
		cloud2_1.setWidth(354);
		cloud2_1.setHeight(194);
		cloud2_0.setScaleX(-1);
		
		cloud2_0.setPositionXY(8,397);
		cloud2_1.setPositionXY(-354,397);
		cloud2_1.setScaleX(-1);
		
		MoveTo cloud2_0_moveTo0 = new MoveTo (1314, 397, 43, 0, false);
		MoveTo cloud2_0_moveTo1 = new MoveTo (-354, 397, 1314, 397, 60, 43, true);
		
		MoveTo cloud2_1_moveTo1 = new MoveTo (1314, 397, 60, 13, true);

		cloud2_0.action(cloud2_0_moveTo0);
		cloud2_0.action(cloud2_0_moveTo1);
		cloud2_1.action(cloud2_1_moveTo1);
		
		//----------------------FLOWER0-------------------------------------------
		final GameObject flower0 = new GameObject("flower0", true, true);
		flower0.setOnCreateFilter(true);
		flower0.setPositionXY(38, 178);
		flower0.setOriginX(47);
		flower0.setOriginY(5);

		RotateTo flower0_rotate1 = RotateTo.$(5, 4);
		RotateTo flower0_rotate2 = RotateTo.$(-10, 4);
		flower0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0.action(Forever.$(Sequence.$(flower0_rotate1, flower0_rotate2)));
		
		//----------------------FLOWER1-------------------------------------------
		final GameObject flower1 = new GameObject("flower1", true, true);
		flower1.setOnCreateFilter(true);
		flower1.setPositionXY(787, 146);
		flower1.setOriginX(32);
		flower1.setOriginY(4);

		RotateTo flower1_rotate1 = RotateTo.$(6, 8);
		RotateTo flower1_rotate2 = RotateTo.$(-11, 8);
		flower1_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1.action(Forever.$(Sequence.$(flower1_rotate1, flower1_rotate2)));
		
		//----------------------FLOWER2-------------------------------------------
		final GameObject flower2 = new GameObject("flower2", true, true);
		flower2.setOnCreateFilter(true);
		flower2.setPositionXY(831,160);
		flower2.setOriginX(39);
		flower2.setOriginY(9);

		RotateTo flower2_rotate1 = RotateTo.$(4, 6);
		RotateTo flower2_rotate2 = RotateTo.$(-8, 6);
		flower2_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2.action(Forever.$(Sequence.$(flower2_rotate1, flower2_rotate2)));

		//----------------------FLOWER3-------------------------------------------
		final GameObject flower3 = new GameObject("flower2", true, true);
		flower3.setOnCreateFilter(true);
		flower3.setPositionXY(255,213);
		flower3.setOriginX(39);
		flower3.setOriginY(9);

		RotateTo flower3_rotate1 = RotateTo.$(4, 6);
		RotateTo flower3_rotate2 = RotateTo.$(-8, 6);
		flower3_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower3_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower3.action(Forever.$(Sequence.$(flower3_rotate1, flower3_rotate2)));
		
		//----------------------OTHER-------------------------------------------
		final GameObject grass0 = new GameObject("grass0", true, true);
		grass0.setPositionXY(0, 0);
		
		//----------------------PONY-------------------------------------------
		//----SHADOW----
		final GameObject shadow = new GameObject("shadow", true, true, 0.25f, 1);
		shadow.setPositionXY(290,89);
		shadow.setOriginX(149);
		shadow.setOriginY(22);
		
		//----BODY-CONTAINER----
		final GameObject body_container = new GameObject("body_container");
		body_container.setPositionXY(245,171);
		
		//----BODY----
		final GameObject body = new GameObject("body", false, true, 0.25f, 1);
		body.setPositionXY(0,0);
		
		//----EYELIDS----
		final GameObject eyelids = new GameObject("eyes0", false, true, 0.25f, 1);
		eyelids.setPositionXY(22, 163);
		eyelids.color.a = 0.0f;
				
		FadeIn eyelids_fadeIn1 = FadeIn.$(0.1f);
		FadeOut eyelids_fadeOut1 = FadeOut.$(0.1f);
		eyelids.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn1, Delay.$(eyelids_fadeOut1, 0.3f)), 
										7)
								), 
					          3)
					   );
		
		//----WING----
		final GameObject wing0 = new GameObject("wing0",false, 0.25f, 1);
		wing0.AddAnim(new AnimKey("wing0", 0.12f));
		wing0.setPositionXY(225, 77);

		//----WING----
		final GameObject wing1 = new GameObject("wing1",false, 0.25f, 1);
		wing1.AddAnim(new AnimKey("wing1", 0.12f));
		wing1.setPositionXY(75, 88);
		
		//----WING-OFF----
		final GameObject wing_off = new GameObject("wing-off", false, true, 0.25f, 1);
		wing_off.setPositionXY(214, 156);		
		
		//----ADD_ACTORS----
		body_container.addActor(wing1);
		body_container.addActor(body);
		body_container.addActor(wing0);
		body_container.addActor(wing_off);		
		body_container.addActor(eyelids);		
	
		animateMyPony();
	}

	public void initializeParticles() {
		GameObject main_particles = (GameObject) MyPonyLW.getRoot().findActor("p1");
		
		if (main_particles == null)
		{
			main_particles = new GameObject("p1",true, 0.25f, 1);
			main_particles.setPositionXY(0,0);
			main_particles.setOriginX(460);
			main_particles.setOriginY(600);
			main_particles.createParticle("p1", false);
		}
		
		sparklesVisible = true;
		main_particles.visible = sparklesVisible;
	}
	
	public void enableWings ()
	{
		GameObject shadow = (GameObject) MyPonyLW.getRoot().findActor("shadow");
		
		ScaleTo shadow_scale1 = ScaleTo.$(0.8f, 0.8f, 1);
		ScaleTo shadow_scale2 = ScaleTo.$(0.73f, 0.73f, 1);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));		
		
		GameObject body_container = (GameObject) MyPonyLW.getRoot().findActor("body_container");
		
		MoveTo body_moveTo0 = new MoveTo (265, 176, 1, 0, false);
		MoveTo body_moveTo1 = new MoveTo (265, 186, 1, 0, false);
		body_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		body_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());
		body_container.action(Forever.$(Sequence.$(body_moveTo0, body_moveTo1)));	
		
		GameObject wing0 = (GameObject) MyPonyLW.getRoot().findActor("wing0");
		wing0.visible = true;
		
		GameObject wing1 = (GameObject) MyPonyLW.getRoot().findActor("wing1");
		wing1.visible = true;
		
		GameObject wing_off = (GameObject) MyPonyLW.getRoot().findActor("wing-off");
		wing_off.visible = false;
	}
	
	public void disableWings()
	{
		GameObject shadow = (GameObject) MyPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		
		GameObject body_container = (GameObject) MyPonyLW.getRoot().findActor("body_container");
		body_container.clearActions();
		
		GameObject wing0 = (GameObject) MyPonyLW.getRoot().findActor("wing0");
		wing0.visible = false;
		
		GameObject wing1 = (GameObject) MyPonyLW.getRoot().findActor("wing1");
		wing1.visible = false;
		
		GameObject wing_off = (GameObject) MyPonyLW.getRoot().findActor("wing-off");
		wing_off.visible = true;		
	}	
	
	public void disableParticles()
	{
		sparklesVisible = false;
		GameObject particle = (GameObject) MyPonyLW.getRoot().findActor("p1");
		
		if(particle != null)
			particle.visible = sparklesVisible;	
	}
	
	public void animateMyPony()
	{
		if (animPony)
			enableWings();
		else
			disableWings();
	}

	@Override
	public void onConfigChanged() {
		if (sparklesVisible && !showParticles)
			disableParticles();
		else if(!sparklesVisible && showParticles)
			initializeParticles();
		if (animPony != MyPonyLW.animetePony)
		{
			animPony = MyPonyLW.animetePony;
			animateMyPony();
		}
	}
}
