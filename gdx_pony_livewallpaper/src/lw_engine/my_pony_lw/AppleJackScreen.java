package lw_engine.my_pony_lw;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.FadeOut;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

import lwp_engine.GameObject;
import lwp_engine.GameObjectAnimation.AnimKey;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.WallpaperScreen;

public class AppleJackScreen extends WallpaperScreen {
	
	private boolean animPony = false;

	public AppleJackScreen(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize() {
		ImageCache.subFolderName = "main";
		
		//----------------------BACKGROUND-------------------------------------------
		final GameObject bg = new GameObject("background", true, false, 0.25f, 1.0f);
		bg.setPositionXY(0,0);

		//----------------------CLOUD_LIGHT0-------------------------------------------
		final GameObject cloud_light0_0 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		final GameObject cloud_light0_1 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		cloud_light0_0.setWidth(224);
		cloud_light0_0.setHeight(39);
		cloud_light0_1.setWidth(224);
		cloud_light0_1.setHeight(39);
		
		cloud_light0_0.setPositionXY(719,481);
		cloud_light0_1.setPositionXY(72,481);

		MoveTo cloud_light0_0_moveTo0 = new MoveTo (960, 481, 37, 0, false);
		MoveTo cloud_light0_0_moveTo1 = new MoveTo (-224, 481, 960, 481, 180, 37, true);
		
		MoveTo cloud_light0_1_moveTo0 = new MoveTo (960, 481, 127, 0, false);
		MoveTo cloud_light0_1_moveTo1 = new MoveTo (-224, 481, 960, 481, 180, 127, true);

		cloud_light0_0.action(cloud_light0_0_moveTo0);
		cloud_light0_0.action(cloud_light0_0_moveTo1);
		cloud_light0_1.action(cloud_light0_1_moveTo0);
		cloud_light0_1.action(cloud_light0_1_moveTo1);
		
		//----------------------CLOUD_LIGHT1-------------------------------------------
		final GameObject cloud_light1_0 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		final GameObject cloud_light1_1 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		cloud_light1_0.setWidth(360);
		cloud_light1_0.setHeight(62);
		cloud_light1_1.setWidth(360);
		cloud_light1_1.setHeight(62);
		
		cloud_light1_0.setPositionXY(547,713);
		cloud_light1_1.setPositionXY(-110,713);

		MoveTo cloud_light1_0_moveTo0 = new MoveTo (960, 713, 56, 0, false);
		MoveTo cloud_light1_0_moveTo1 = new MoveTo (-360, 713, 960, 713, 180, 56, true);
		
		MoveTo cloud_light1_1_moveTo0 = new MoveTo (960, 713, 146, 0, false);
		MoveTo cloud_light1_1_moveTo1 = new MoveTo (-360, 713, 960, 713, 180, 146, true);

		cloud_light1_0.action(cloud_light1_0_moveTo0);
		cloud_light1_0.action(cloud_light1_0_moveTo1);
		cloud_light1_1.action(cloud_light1_1_moveTo0);
		cloud_light1_1.action(cloud_light1_1_moveTo1);
		
		//----------------------CLOUD_LIGHT2-------------------------------------------
		final GameObject cloud_light2_0 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		final GameObject cloud_light2_1 = new GameObject("cloudLight0", true, true, 0.25f, 1.0f);
		cloud_light2_0.setWidth(360);
		cloud_light2_0.setHeight(62);
		cloud_light2_0.setOriginX(180);
		cloud_light2_0.setScaleX(-1);
		
		cloud_light2_1.setWidth(360);
		cloud_light2_1.setHeight(62);
		cloud_light2_1.setOriginX(180);
		cloud_light2_1.setScaleX(-1);
		
		cloud_light2_0.setPositionXY(24,641);
		cloud_light2_1.setPositionXY(-360,641);

		MoveTo cloud_light2_0_moveTo0 = new MoveTo (1320, 641, 128, 0, false);
		MoveTo cloud_light2_0_moveTo1 = new MoveTo (-360, 641, 1320, 641, 180, 128, true);
		
		MoveTo cloud_light2_1_moveTo0 = new MoveTo (-360, 641, 1320, 641, 180, 38, true);

		cloud_light2_0.action(cloud_light2_0_moveTo0);
		cloud_light2_0.action(cloud_light2_0_moveTo1);
		cloud_light2_1.action(cloud_light2_1_moveTo0);
		
		//----------------------CLOUD0-------------------------------------------
		final GameObject cloud0_0 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		final GameObject cloud0_1 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		cloud0_0.setWidth(200);
		cloud0_0.setHeight(112);
		cloud0_1.setWidth(200);
		cloud0_1.setHeight(112);
		
		cloud0_0.setPositionXY(149, 652);
		cloud0_1.setPositionXY(-200, 652);
		
		MoveTo cloud0_0_moveTo0 = new MoveTo (960, 652, 144, 0, false);
		MoveTo cloud0_0_moveTo1 = new MoveTo (-200, 652, 960, 652, 120, 144, true);
		
		MoveTo cloud0_1_moveTo0 = new MoveTo (960, 652, 120, 54, true);

		cloud0_0.action(cloud0_0_moveTo0);
		cloud0_0.action(cloud0_0_moveTo1);
		cloud0_1.action(cloud0_1_moveTo0);
		
		//----------------------CLOUD1-------------------------------------------
		final GameObject cloud1_0 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		final GameObject cloud1_1 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		cloud1_0.setWidth(354);
		cloud1_0.setHeight(194);
		cloud1_1.setWidth(354);
		cloud1_1.setHeight(194);
		
		cloud1_0.setPositionXY(542,505);
		cloud1_1.setPositionXY(-121,505);
		
		MoveTo cloud1_0_moveTo0 = new MoveTo (960, 505, 29, 0, false);
		MoveTo cloud1_0_moveTo1 = new MoveTo (-354, 505, 960, 505, 90, 29, true);
		
		MoveTo cloud1_1_moveTo0 = new MoveTo (960, 505, 74, 0, false);
		MoveTo cloud1_1_moveTo1 = new MoveTo (-354, 505, 960, 505, 90, 74, true);

		cloud1_0.action(cloud1_0_moveTo0);
		cloud1_0.action(cloud1_0_moveTo1);
		cloud1_1.action(cloud1_1_moveTo0);
		cloud1_1.action(cloud1_1_moveTo1);
		
		//----------------------CLOUD2-------------------------------------------
		final GameObject cloud2_0 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		final GameObject cloud2_1 = new GameObject("cloud0", true, true, 0.25f, 1.0f);
		cloud2_0.setWidth(354);
		cloud2_0.setHeight(194);
		cloud2_1.setWidth(354);
		cloud2_1.setHeight(194);
		cloud2_0.setScaleX(-1);
		
		cloud2_0.setPositionXY(8,397);
		cloud2_1.setPositionXY(-354,397);
		cloud2_1.setScaleX(-1);
		
		MoveTo cloud2_0_moveTo0 = new MoveTo (1314, 397, 43, 0, false);
		MoveTo cloud2_0_moveTo1 = new MoveTo (-354, 397, 1314, 397, 60, 43, true);
		
		MoveTo cloud2_1_moveTo1 = new MoveTo (1314, 397, 60, 13, true);

		cloud2_0.action(cloud2_0_moveTo0);
		cloud2_0.action(cloud2_0_moveTo1);
		cloud2_1.action(cloud2_1_moveTo1);
		
		//----------------------FLOWER0-------------------------------------------
		final GameObject flower0 = new GameObject("flower0", true, true, 0.8f, 1.0f);
		flower0.setOnCreateFilter(true);
		flower0.setPositionXY(38, 204);
		flower0.setOriginX(47);
		flower0.setOriginY(5);

		RotateTo flower0_rotate1 = RotateTo.$(5, 4);
		RotateTo flower0_rotate2 = RotateTo.$(-10, 4);
		flower0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0.action(Forever.$(Sequence.$(flower0_rotate1, flower0_rotate2)));
		
		//----------------------FLOWER1-------------------------------------------
		final GameObject flower1 = new GameObject("flower1", true, true, 0.8f, 1.0f);
		flower1.setOnCreateFilter(true);
		flower1.setPositionXY(785, 173);
		flower1.setOriginX(32);
		flower1.setOriginY(4);

		RotateTo flower1_rotate1 = RotateTo.$(6, 8);
		RotateTo flower1_rotate2 = RotateTo.$(-11, 8);
		flower1_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1.action(Forever.$(Sequence.$(flower1_rotate1, flower1_rotate2)));
		
		//----------------------FLOWER2-------------------------------------------
		final GameObject flower2 = new GameObject("flower2", true, true, 0.8f, 1.0f);
		flower2.setOnCreateFilter(true);
		flower2.setPositionXY(830, 188);
		flower2.setOriginX(39);
		flower2.setOriginY(9);

		RotateTo flower2_rotate1 = RotateTo.$(4, 6);
		RotateTo flower2_rotate2 = RotateTo.$(-8, 6);
		flower2_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2.action(Forever.$(Sequence.$(flower2_rotate1, flower2_rotate2)));
		
		//----------------------FLOWER3-------------------------------------------
		final GameObject flower3 = new GameObject("flower2", true, true, 0.8f, 1.0f);
		flower3.setOnCreateFilter(true);
		flower3.setPositionXY(255, 240);
		flower3.setOriginX(39);
		flower3.setOriginY(9);

		RotateTo flower3_rotate1 = RotateTo.$(4, 6);
		RotateTo flower3_rotate2 = RotateTo.$(-8, 6);
		flower3_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower3_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower3.action(Forever.$(Sequence.$(flower3_rotate1, flower3_rotate2)));
		
		//----------------------OTHER-------------------------------------------
		final GameObject grass0 = new GameObject("grass0", true, true, 0.8f, 1.0f);
		grass0.setPositionXY(0, 0);
		
		//----------------------PONY-------------------------------------------
		//----SHADOW----
		final GameObject shadow = new GameObject("shadow", true, true);
		shadow.setPositionXY(319, 100);
		shadow.setOriginX(183);
		shadow.setOriginY(31);
		
		//----BODY-CONTAINER----
		final GameObject body_container = new GameObject("body_container");
		body_container.setPositionXY(265, 118);
		body_container.setOriginX(238);
		body_container.setOriginY(13);
		
		//----BODY----
		final GameObject body = new GameObject("body", false, true);
		body.setPositionXY(113, 0);
		body.setOnCreateFilter(true);
		
		//----TAIL0----
		final GameObject tail0 = new GameObject("hair1", false, true);
		tail0.setPositionXY(261, 88);
		tail0.setOriginX(15);
		tail0.setOriginY(131);

		//----TAIL1----
		final GameObject tail1 = new GameObject("hair0", false, true);
		tail1.setPositionXY(112, -38);
		tail0.addActor(tail1);
		tail1.setOriginX(14);
		tail1.setOriginY(44);
		
		//----LEG----
		final GameObject leg = new GameObject("leg", false);
		leg.setPositionXY(137, 22);
		leg.pongAnimation = true;
		leg.pauseAnim = 7;		
		leg.AddAnim(new AnimKey("leg", 0.2f));

		//----HAIR2----
		final GameObject hair2 = new GameObject("hair2", false, true);
		hair2.setPositionXY(141, 137);
		hair2.setOriginX(48);
		hair2.setOriginY(118);		

		//----HEAD----
		final GameObject head = new GameObject("head", false, true);
		head.setPositionXY(0, 222);

		//----LASHES----
		final GameObject lashes = new GameObject("lashes", false, true);
		lashes.setPositionXY(195, 315);
		
		FadeIn lashes_fadeIn1 = FadeIn.$(0.1f);
		FadeOut lashes_fadeOut1 = FadeOut.$(0.1f);
		lashes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(lashes_fadeOut1, Delay.$(lashes_fadeIn1, 0.3f)), 
										10)
								), 
					          5)
					   );		
		
		//----EYELIDS----
		final GameObject eyelids = new GameObject("eyes", false, true);
		eyelids.setPositionXY(89, 229);
		eyelids.color.a = 0.0f;
				
		FadeIn eyelids_fadeIn1 = FadeIn.$(0.1f);
		FadeOut eyelids_fadeOut1 = FadeOut.$(0.1f);
		eyelids.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn1, Delay.$(eyelids_fadeOut1, 0.3f)), 
										10)
								), 
					          5)
					   );		
		
		//----HAIR3----
		final GameObject hair3 = new GameObject("hair3", false, true);
		hair3.setPositionXY(57, 313);
		hair3.setOriginX(21);
		hair3.setOriginY(27);			
		
		//----ADD_ACTORS----
		body_container.addActor(tail0);
		body_container.addActor(body);
		body_container.addActor(leg);
		body_container.addActor(hair2);
		body_container.addActor(head);		
		body_container.addActor(lashes);
		body_container.addActor(eyelids);
		body_container.addActor(hair3);
	
		animateMyPony();		
	}
	
	public void initializeParticles() {
		GameObject main_particles = (GameObject) MyPonyLW.getRoot().findActor("p1");
		
		if (main_particles == null)
		{
			main_particles = new GameObject("p1",true);
			main_particles.setPositionXY(0,0);
			main_particles.setOriginX(480);
			main_particles.setOriginY(600);
			main_particles.createParticle("p1", false);
		}
		
		sparklesVisible = true;
		main_particles.visible = sparklesVisible;
	}
	
	public void disableParticles()
	{
		sparklesVisible = false;
		GameObject particle = (GameObject) MyPonyLW.getRoot().findActor("p1");
		
		if(particle != null)
			particle.visible = sparklesVisible;	
	}
	
	private void enablePonyAnim()
	{
		int speed = 1;
		GameObject shadow = (GameObject) MyPonyLW.getRoot().findActor("shadow");
		ScaleTo shadow_scale1 = ScaleTo.$(1.005f, 1.005f, speed);
		ScaleTo shadow_scale2 = ScaleTo.$(0.99f, 0.99f, speed);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));
		
		GameObject body_container = (GameObject) MyPonyLW.getRoot().findActor("body_container");
		ScaleTo body_scale1 = ScaleTo.$(1.004f, 0.996f, speed);
		ScaleTo body_scale2 = ScaleTo.$(1.0f, 1.0f, speed);
		body_container.action(Forever.$(Sequence.$(body_scale1, body_scale2)));
		
		GameObject tail0 = (GameObject) body_container.findActor("hair1");
		RotateTo tail0_rotate1 = RotateTo.$(0.5f, speed);
		RotateTo tail0_rotate2 = RotateTo.$(-0.5f, speed);
		tail0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail0.action(Forever.$(Sequence.$(tail0_rotate1, tail0_rotate2)));
		
		GameObject tail1 = (GameObject) tail0.findActor("hair0");
		RotateTo tail1_rotate1 = RotateTo.$(2, speed);
		RotateTo tail1_rotate2 = RotateTo.$(-2, speed);
		tail1_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail1_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tail1.action(Forever.$(Sequence.$(tail1_rotate1, tail1_rotate2)));
		
		GameObject hair2 = (GameObject) body_container.findActor("hair2");
		ScaleTo hair2_scale1 = ScaleTo.$(1, 1.01f, speed);
		ScaleTo hair2_scale2 = ScaleTo.$(1, 0.99f, speed);
		hair2_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		hair2_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		hair2.action(Forever.$(Sequence.$(hair2_scale1, hair2_scale2)));
		
		GameObject hair3 = (GameObject) body_container.findActor("hair3");
		RotateTo hair3_rotate1 = RotateTo.$(0.25f, speed);
		RotateTo hair3_rotate2 = RotateTo.$(-0.25f, speed);
		hair3_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		hair3_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		hair3.action(Forever.$(Sequence.$(hair3_rotate1, hair3_rotate2)));
	}
	
	private void disablePonyAnim()
	{
		GameObject shadow = (GameObject) MyPonyLW.getRoot().findActor("shadow");
		shadow.clearActions();
		shadow.scaleX = 1;
		shadow.scaleY = 1;
		
		GameObject body_container = (GameObject) MyPonyLW.getRoot().findActor("body_container");
		body_container.clearActions();
		body_container.scaleX = 1;
		body_container.scaleY = 1;
		
		GameObject tail0 = (GameObject) body_container.findActor("hair1");
		tail0.clearActions();
		tail0.rotation = 0;
		
		GameObject tail1 = (GameObject) tail0.findActor("hair0");
		tail1.clearActions();
		tail1.rotation = 0;
		
		GameObject hair2 = (GameObject) body_container.findActor("hair2");
		hair2.clearActions();
		hair2.scaleX = 1;
		hair2.scaleY = 1;
		
		GameObject hair3 = (GameObject) body_container.findActor("hair3");
		hair3.clearActions();
		hair3.rotation = 0;
	}
	
	public void animateMyPony()
	{
		if (animPony)
			enablePonyAnim();
		else
			disablePonyAnim();
	}
	
	@Override
	public void onConfigChanged() {
		// TODO Auto-generated method stub
		if (sparklesVisible && !showParticles)
			disableParticles();
		else if(!sparklesVisible && showParticles)
			initializeParticles();
		if (animPony != MyPonyLW.animetePony)
		{
			animPony = MyPonyLW.animetePony;
			animateMyPony();
		}
	}

}
