package lw_engine.birds_lw;

import lwp_engine.GameObject;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.WallpaperScreen;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.FadeOut;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

public class BirdsScreen extends WallpaperScreen{
	
	public BirdsScreen(String name)
	{
		super(name);
	}

	@Override
	public void initialize() {
		
	}

	public void initializeParticles() {

	}
	
	public void disableParticles()
	{
				
	}
	
	@Override
	public void onConfigChanged() {


	}
}
