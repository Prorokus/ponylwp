package lw_engine.birds_lw;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.jogl.JoglApplication;

public class BirdsLWDesktop {
	public static void main(String[] argv)
	{
		JoglApplication app = new JoglApplication((ApplicationListener) new BirdsLW(), "Birds LW App", 960, 854,  false);
	}
}
