package lw_engine.birds_lw;

import lwp_engine.LWPApplication;

public class BirdsLW extends LWPApplication{

	private final String MY_PONY_PREFS = "com.blogspot.androidlivewall.birds_lw_full_preferences";

	private boolean showParticles = true;
		
	@Override
	protected String getPrefsName()
	{
		return MY_PONY_PREFS;
	}
	
	@Override
	protected void loadWallScreen(boolean... params)
	{
		wallScreen = new BirdsScreen("hover");
		wallConfigChanged();
	}

	@Override
	protected void readAllPrefs() {
		//enableParallax = prefs.getBoolean("set_pony_parallax",false);
	}
		
	@Override
	protected void onResumeWallpaper() {
		// TODO Auto-generated method stub
				
		if (wallScreen != null)
		{
			wallConfigChanged();
		}
	}
	
	private void wallConfigChanged()
	{
		wallScreen.onConfigChanged();
	}
		
}
