package com.blogspot.androidlivewall.saint_patrick_pony_lw;

import lw_engine.my_saint_patrick_pony_lw.MyPatrickPonyLW;

import com.badlogic.gdx.backends.android.livewallpaper.AndroidApplication;
import com.eightbitmage.gdxlw.LibdgxWallpaperService;

public class SaintPatrickPonyWallpaperService extends LibdgxWallpaperService {
	
	@Override
	public Engine onCreateEngine() {
		//android.os.Debug.waitForDebugger();
		// TODO Auto-generated method stub
		return new SainPatrickPonyWallpaperEngine(this);
	}
	
	public class SainPatrickPonyWallpaperEngine extends LibdgxWallpaperEngine
	{

		public SainPatrickPonyWallpaperEngine(
				LibdgxWallpaperService libdgxWallpaperService) {
			super(libdgxWallpaperService);
			// TODO Auto-generated constructor stub
		} 
		
		protected void initialize(AndroidApplication androidApplicationLW)
		{
			MyPatrickPonyLW app = new MyPatrickPonyLW();
			setWallpaperListener(app);
			androidApplicationLW.initialize(app,false);
		}
	}
}