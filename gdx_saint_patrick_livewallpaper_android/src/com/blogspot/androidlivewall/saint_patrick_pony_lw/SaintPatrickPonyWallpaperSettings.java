package com.blogspot.androidlivewall.saint_patrick_pony_lw;

import lw_engine.my_saint_patrick_pony_lw.MyPatrickPonyLW;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.livewallpaper.AndroidPreferences;

public class SaintPatrickPonyWallpaperSettings extends PreferenceActivity
implements SharedPreferences.OnSharedPreferenceChangeListener
{
	public String MY_PONY_PREFS = "com.blogspot.androidlivewall.saint_patrick_pony_lw_preferences";
	private AndroidPreferences prefs;
	private boolean prefsChanged = false;
	
	protected void onCreate(Bundle icicle) {
		
    	super.onCreate(icicle);
    	try{
    		if (Gdx.app != null)
    			prefs = (AndroidPreferences) Gdx.app.getPreferences(MY_PONY_PREFS);
    	}
    	catch(NullPointerException ex)
    	{
    		Log.e("MyPonyLW_Setting Error", ex.toString());
    	}
    	addPreferencesFromResource(R.xml.settings);
	}

	@Override
	protected void onResume() {
    	super.onResume();
    	if(prefs != null)
    		prefs.getSharedPrefs().registerOnSharedPreferenceChangeListener(this);
	}

	protected void onPause()
	{
		super.onPause();
		if(prefs != null)
			prefs.getSharedPrefs().unregisterOnSharedPreferenceChangeListener(this);
		
	 	if(this.prefsChanged)
	 		MyPatrickPonyLW.prefsChanged = true;
	}
	
	@Override
	protected void onDestroy() {
	 	super.onDestroy();

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		// TODO Auto-generated method stub
		this.prefsChanged = true;
	}

}