package lw_engine.hover_pony_lw.pony_container;

import lw_engine.hover_pony_lw.HoverPonyLW;
import lwp_engine.GameObject;
import lwp_engine.ImageCache;

public abstract class PonyContainer {
	
	GameObject ponyObj;
	String name;
	String imgName;
	
	public PonyContainer(String name)
	{
		this(name,null);
	}
	
	public PonyContainer(String name, String imgName)
	{
		this.name = name;
		this.imgName = imgName;
		ponyObj = initializePony();
	}
	
	public void clearContainer()
	{
		if (ponyObj != null)
		{
			ImageCache.unload(name, imgName);
			ponyObj.clear();
			ponyObj.visible = false;
		}
	}
	
	public abstract GameObject initializePony();
	public abstract void enablePonyAnim();
	public abstract void disablePonyAnim();
		
}
