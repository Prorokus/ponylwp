package lw_engine.hover_pony_lw;

import lwp_engine.LWPApplication;

public class HoverPonyLW extends LWPApplication{

	private final String MY_PONY_PREFS = "com.blogspot.androidlivewall.hover_pony_lw_free_preferences";

	private boolean showParticles = true;
	public static boolean animetePony = true;

	
	@Override
	protected String getPrefsName()
	{
		return MY_PONY_PREFS;
	}
	
	@Override
	protected void loadWallScreen(boolean... params)
	{
		if (params[0] == true && wallScreen == null)
			wallScreen = new HoverVillageScreen("hover");
				
		wallConfigChanged();
		prevSelectedWall = selectedWall;
	}

	@Override
	protected void readAllPrefs() {
		enableParallax = prefs.getBoolean("set_pony_parallax",false);
		
		wallNotMovable = prefs.getBoolean("set_pony_center",false);
		selectedWall = Integer.parseInt(prefs.getString("selected_wallpaper","0"));
		showParticles = prefs.getBoolean("set_pony_sparkles",true);
		animetePony = prefs.getBoolean("set_pony_anim",true);
	}
		
	@Override
	protected void onResumeWallpaper() {
		// TODO Auto-generated method stub
				
		if (selectedWall != prevSelectedWall)
		{
			loadWallScreen();
		}
		
		if (wallScreen != null)
		{
			wallConfigChanged();
		}
	}
	
	private void wallConfigChanged()
	{
		wallScreen.showParticles = showParticles;
		wallScreen.onConfigChanged();
	}
		
}
