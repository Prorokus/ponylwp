package lw_engine.hover_pony_lw;

import lwp_engine.GameObject;
import lwp_engine.GameObjectAnimation.AnimKey;
import lwp_engine.ImageCache;
import lwp_engine.MoveTo;
import lwp_engine.RandomLighten;
import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

public class HoverVillageScreen extends HoverWallpaperScreen {
	
	public HoverVillageScreen(String name) {
		super(name);
	}

	@Override
	public void initialize() {
		super.initialize();
		
		ImageCache.load(name, "back3");
		
		//----------------------BACKGROUND-------------------------------------------
		final GameObject bg = new GameObject("background", true, false, 0.1f, 1);
		bg.objectSubFolder = "back3";
		bg.setPositionXY(0,0);
		

		//----------------------CLOUD1-------------------------------------------
		final GameObject cloud1_0 = new GameObject("cloud", true, true, 0.1f, 1);
		cloud1_0.objectSubFolder = "back3";
		
		cloud1_0.setPositionXY(215 + 960 * 0.5f, 620);
		cloud1_0.setScaleX(-0.7f);
		cloud1_0.setScaleY(0.7f);
		
		MoveTo cloud1_0_moveTo0 = new MoveTo (1200, 620, 60, 0, false);
		MoveTo cloud1_0_moveTo1 = new MoveTo (-215, 620, 1200, 620, 120, 60, true);
		
		cloud1_0.action(cloud1_0_moveTo0);
		cloud1_0.action(cloud1_0_moveTo1);
		
		//----------------------CLOUD0-------------------------------------------
		final GameObject cloud0_0 = new GameObject("cloud", true, true, 0.1f, 1);
		cloud0_0.objectSubFolder = "back3";
		
		cloud0_0.setPositionXY(-308, 590);
		
		MoveTo cloud0_0_moveTo0 = new MoveTo (-308, 590, 960, 590, 60, 0, true);

		cloud0_0.action(cloud0_0_moveTo0);
		
		//----------------------SUN-------------------------------------------
		final GameObject sun = new GameObject("sun", true, true, 0.1f, 1);
		sun.setPositionXY(0, 473);
		
		RandomLighten randLight = new RandomLighten (0.5f, 1, -1, 1, 1);
		sun.action(Forever.$(randLight));

		//----------------------BALLOON-CONTAINER0-------------------------------------------
		final GameObject balloon_container0 = new GameObject("balloon_container0");
		balloon_container0.setPositionXY(591, 564);
		
		//MoveTo balloon_container0_moveTo0 = new MoveTo (960, 600, -94, 550, 120, 0, true);
		//balloon_container0.action(balloon_container0_moveTo0);		
		
		//----------------------BALLOON-CONTAINER-------------------------------------------
		final GameObject balloon_container = new GameObject("balloon_container", false);
		balloon_container.setPositionXY(0, 0);
		
		MoveTo balloon_container_moveTo0 = new MoveTo (0, 0, 0, 5, 4, 0, false);
		MoveTo balloon_container_moveTo1 = new MoveTo (0, 5, 0, 0, 4, 0, false);
		balloon_container_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		balloon_container_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());
		balloon_container.action(Forever.$(Sequence.$(balloon_container_moveTo0, balloon_container_moveTo1)));		

		balloon_container0.addActor(balloon_container);
		
		//----------------------BALLOON0-------------------------------------------
		final GameObject balloon0 = new GameObject("balloon0", false, true, 0.1f, 1);
		balloon0.setOnCreateFilter(true);
		balloon0.setPositionXY(27, 0);
		balloon0.setOriginX(20);
		balloon0.setOriginY(39);
		
		RotateTo balloon0_rotate1 = RotateTo.$(-3, 2);
		RotateTo balloon0_rotate2 = RotateTo.$(3, 2);
		balloon0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		balloon0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		balloon0.action(Forever.$(Sequence.$(balloon0_rotate1, balloon0_rotate2)));
		
		//----------------------BALLOON1-------------------------------------------
		final GameObject balloon1 = new GameObject("balloon1", false, true, 0.1f, 1);
		balloon1.setPositionXY(0, 35);
		
		balloon_container.addActor(balloon0);
		balloon_container.addActor(balloon1);
		
		//----BIRD0----
		final GameObject bird0 = new GameObject("bird0", true, true);
		bird0.pongAnimation = true;
		bird0.setPositionXY(960, 650);
		bird0.setWidth(56);
		bird0.setHeight(74);
		bird0.AddAnim(new AnimKey("bird0", 0.1f));

		MoveTo bird0_moveTo0 = new MoveTo (960, 650, -77, 650, 15, 0, false);
		MoveTo bird0_moveTo1 = new MoveTo (960, 650, 0, 0, false);
		
		bird0.action(Forever.$(
							   Sequence.$(
										  bird0_moveTo0, Delay.$(bird0_moveTo1, 50))
							 )
					 );
		
		//----BIRD1----
		final GameObject bird1 = new GameObject("bird1", true, true);
		bird1.pongAnimation = true;
		bird1.setPositionXY(-77, 690);
		bird1.setWidth(56);
		bird1.setHeight(74);
		bird1.AddAnim(new AnimKey("bird1", 0.1f));

		MoveTo bird1_moveTo0 = new MoveTo (-77, 690, 960, 690, 15, 0, false);
		MoveTo bird1_moveTo1 = new MoveTo (-77, 690, 0, 0, false);
		
		bird1.action(Forever.$(
							   Sequence.$(
									      Delay.$(bird1_moveTo0, 25), Delay.$(bird1_moveTo1, 25))
							   )
				     );
		
		//----------------------BACK1-------------------------------------------
		final GameObject back1 = new GameObject("back1", true, true, 0.5f, 1);
		back1.objectSubFolder = "back3";
		back1.setPositionXY(0, 0);
		
		/*//----SMOKE----
		GameObject smoke = new GameObject("p1", true, true, 0.5f, 1);
		smoke.setPositionXY(0,0);
		smoke.setOriginX(396);
		smoke.setOriginY(723);
		smoke.createParticle("p1", false);		
		*/
		
		//----------------------GRASS-------------------------------------------
		final GameObject grass = new GameObject("grass", true, true);
		grass.objectSubFolder = "back3";
		grass.setPositionXY(0, 0);
		
		setActivePony(0);
		
		//----------------------TREE-------------------------------------------
		final GameObject tree = new GameObject("tree", true, true);
		tree.objectSubFolder = "back3";
		tree.setOnCreateFilter(true);
		
		tree.setPositionXY(-108, 664);
		tree.setOriginX(8);
		tree.setOriginY(102);
		tree.rotation = -1;

		RotateTo tree_rotate1 = RotateTo.$(1, 5);
		RotateTo tree_rotate2 = RotateTo.$(-1, 5);
		tree_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tree_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tree.action(Forever.$(Sequence.$(tree_rotate1, tree_rotate2)));
		
		ScaleTo tree_scale1 = ScaleTo.$(1.02f, 1.02f, 5);
		ScaleTo tree_scale2 = ScaleTo.$(0.98f, 0.98f, 5);
		tree_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tree_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tree.action(Forever.$(Sequence.$(tree_scale1, tree_scale2)));
		
		//----------------------BEE2-------------------------------------------
		final GameObject bee2 = new GameObject("bee2", false, true);
		bee2.setOnCreateFilter(true);
		bee2.objectSubFolder = "back3";
		bee2.setPositionXY(376, 52);		

		//----------------------BEE2-WING0-------------------------------------------
		final GameObject bee2_wing0 = new GameObject("bee-wing2", false, true);
		bee2_wing0.setOnCreateFilter(true);
		bee2_wing0.objectSubFolder = "back3";
		bee2_wing0.setOriginX(8);
		bee2_wing0.setOriginY(3);
		bee2_wing0.setPositionXY(392, 75);		
		
		/*
		RotateTo bee2_wing0_rotate1 = RotateTo.$(-15, 0.04f);
		RotateTo bee2_wing0_rotate2 = RotateTo.$(0, 0.04f);
		bee2_wing0.action(Forever.$(Sequence.$(bee2_wing0_rotate1, bee2_wing0_rotate2)));		
		*/
		
		//----------------------BEE2-WING1-------------------------------------------
		final GameObject bee2_wing1 = new GameObject("bee-wing2", false, true);
		bee2_wing1.setOnCreateFilter(true);
		bee2_wing1.objectSubFolder = "back3";
		bee2_wing1.setOriginX(8);
		bee2_wing1.setOriginY(3);
		bee2_wing1.setScaleX(-1);
		bee2_wing1.setPositionXY(392, 75);
		bee2_wing1.rotation = 10;
		
		/*RotateTo bee2_wing1_rotate1 = RotateTo.$(25, 0.04f);
		RotateTo bee2_wing1_rotate2 = RotateTo.$(10, 0.04f);
		bee2_wing1.action(Forever.$(Sequence.$(bee2_wing1_rotate1, bee2_wing1_rotate2)));*/	
		
		tree.addActor(bee2_wing0);
		tree.addActor(bee2_wing1);
		tree.addActor(bee2);
		
		//----------------------FLOWER0-------------------------------------------
		final GameObject flower0 = new GameObject("flower0", true, true, 2, 1);
		flower0.objectSubFolder = "back3";
		flower0.setOnCreateFilter(true);
		flower0.setPositionXY(25, 75);
		flower0.setOriginX(59);
		flower0.setOriginY(10);

		RotateTo flower0_rotate1 = RotateTo.$(5, 4);
		RotateTo flower0_rotate2 = RotateTo.$(-5, 4);
		flower0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower0.action(Forever.$(Sequence.$(flower0_rotate1, flower0_rotate2)));
		
		//----------------------FLOWER1-------------------------------------------
		final GameObject flower1 = new GameObject("flower1", true, true, 2, 1);
		flower1.objectSubFolder = "back3";
		flower1.setOnCreateFilter(true);
		flower1.setPositionXY(261, 60);
		flower1.setOriginX(51);
		flower1.setOriginY(7);

		RotateTo flower1_rotate1 = RotateTo.$(6, 8);
		RotateTo flower1_rotate2 = RotateTo.$(-6, 8);
		flower1_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower1.action(Forever.$(Sequence.$(flower1_rotate1, flower1_rotate2)));
		
		//----------------------FLOWER2-------------------------------------------
		final GameObject flower2 = new GameObject("flower2", true, true, 2, 1);
		flower2.objectSubFolder = "back3";
		flower2.setOnCreateFilter(true);
		flower2.setPositionXY(660, 40);
		flower2.setOriginX(55);
		flower2.setOriginY(7);

		RotateTo flower2_rotate1 = RotateTo.$(4, 6);
		RotateTo flower2_rotate2 = RotateTo.$(-4, 6);
		flower2_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower2.action(Forever.$(Sequence.$(flower2_rotate1, flower2_rotate2)));
		
		//----------------------FLOWER3-------------------------------------------
		final GameObject flower3 = new GameObject("flower3", true, true, 2, 1);
		flower3.objectSubFolder = "back3";
		flower3.setOnCreateFilter(true);
		flower3.setPositionXY(752, 50);
		flower3.setOriginX(67);
		flower3.setOriginY(18);

		RotateTo flower3_rotate1 = RotateTo.$(4, 6);
		RotateTo flower3_rotate2 = RotateTo.$(-4, 6);
		flower3_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower3_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		flower3.action(Forever.$(Sequence.$(flower3_rotate1, flower3_rotate2)));

		//----------------------GRASS-PARALLAX-------------------------------------------
		final GameObject grass_parallax = new GameObject("grass-parallax", true, true, 2, 1);
		grass_parallax.objectSubFolder = "back3";
		grass_parallax.setWidth(1154);
		grass_parallax.setHeight(442);
		grass_parallax.setPositionXY(-250, -230);
		
		//----------------------BEE0-------------------------------------------
		final GameObject bee0 = new GameObject("bee", true, true);
		bee0.setOnCreateFilter(true);
		
		bee0.objectSubFolder = "back3";
		bee0.setPositionXY(139, 191);	
		
		/*MoveTo bee0_moveTo0 = new MoveTo (139, 196, 3, 0, false);
		MoveTo bee0_moveTo1 = new MoveTo (139, 186, 3, 0, false);
		bee0_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		bee0_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());
		bee0.action(Forever.$(Sequence.$(bee0_moveTo0, bee0_moveTo1)));	*/		

		//----------------------BEE0-WING0-------------------------------------------
		final GameObject bee0_wing0 = new GameObject("bee-wing", false, true);
		bee0_wing0.setOnCreateFilter(true);
		bee0_wing0.objectSubFolder = "back3";
		bee0_wing0.setOriginX(22);
		bee0_wing0.setOriginY(5);
		bee0_wing0.setPositionXY(23, 30);		
		
		/*RotateTo bee0_wing0_rotate1 = RotateTo.$(15, 0.04f);
		RotateTo bee0_wing0_rotate2 = RotateTo.$(0, 0.04f);
		bee0_wing0.action(Forever.$(Sequence.$(bee0_wing0_rotate1, bee0_wing0_rotate2)));*/	
		
		//----------------------BEE0-WING1-------------------------------------------
		final GameObject bee0_wing1 = new GameObject("bee-wing", false, true);
		bee0_wing1.setOnCreateFilter(true);
		bee0_wing1.objectSubFolder = "back3";
		bee0_wing1.setOriginX(22);
		bee0_wing1.setOriginY(5);
		bee0_wing1.setScaleX(-1);
		bee0_wing1.setPositionXY(23, 30);
		bee0_wing1.rotation = -10;
		
		/*RotateTo bee0_wing1_rotate1 = RotateTo.$(-25, 0.04f);
		RotateTo bee0_wing1_rotate2 = RotateTo.$(-10, 0.04f);
		bee0_wing1.action(Forever.$(Sequence.$(bee0_wing1_rotate1, bee0_wing1_rotate2)));*/			
		
		bee0.addActor(bee0_wing0);
		bee0.addActor(bee0_wing1);		
		
		//----------------------BEE1-------------------------------------------
		final GameObject bee1 = new GameObject("bee2", true, true);
		bee1.setOnCreateFilter(true);
		bee1.objectSubFolder = "back3";
		bee1.setPositionXY(756, 182);		
		
		/*MoveTo bee1_moveTo0 = new MoveTo (756, 187, 4, 0, false);
		MoveTo bee1_moveTo1 = new MoveTo (756, 177, 4, 0, false);
		bee1_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		bee1_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());
		bee1.action(Forever.$(Sequence.$(bee1_moveTo0, bee1_moveTo1)));	*/	

		//----------------------BEE1-WING0-------------------------------------------
		final GameObject bee1_wing0 = new GameObject("bee-wing2", false, true);
		bee1_wing0.setOnCreateFilter(true);
		bee1_wing0.objectSubFolder = "back3";
		bee1_wing0.setOriginX(8);
		bee1_wing0.setOriginY(3);
		bee1_wing0.setPositionXY(16, 23);		
		
		/*RotateTo bee1_wing0_rotate1 = RotateTo.$(-15, 0.04f);
		RotateTo bee1_wing0_rotate2 = RotateTo.$(0, 0.04f);
		bee1_wing0.action(Forever.$(Sequence.$(bee1_wing0_rotate1, bee1_wing0_rotate2)));	*/	

		//----------------------BEE2-WING1-------------------------------------------
		final GameObject bee1_wing1 = new GameObject("bee-wing2", false, true);
		bee1_wing1.setOnCreateFilter(true);
		bee1_wing1.objectSubFolder = "back3";
		bee1_wing1.setOriginX(8);
		bee1_wing1.setOriginY(3);
		bee1_wing1.setScaleX(-1);
		bee1_wing1.setPositionXY(16, 23);
		bee1_wing1.rotation = 10;
		
		/*RotateTo bee1_wing1_rotate1 = RotateTo.$(25, 0.04f);
		RotateTo bee1_wing1_rotate2 = RotateTo.$(10, 0.04f);
		bee1_wing1.action(Forever.$(Sequence.$(bee1_wing1_rotate1, bee1_wing1_rotate2)));*/			
		
		bee1.addActor(bee1_wing0);
		bee1.addActor(bee1_wing1);
		
	}

	@Override
	public void enableAdditionalAnims() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableAdditionalAnims() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableAdditionalParticles() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableAdditionalParticles() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableAnimClouds() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableAnimClouds() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableAnimBalloon() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableAnimBalloon() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableAnimBird() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableAnimBird() {
		// TODO Auto-generated method stub
		
	}
}
