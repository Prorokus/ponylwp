package lw_engine.easter_pony_lw;

import java.util.List;

import lwp_engine.GameObject;
import lwp_engine.MoveTo;
import lwp_engine.WallpaperScreen;

import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.FadeOut;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateDecelerateInterpolator;

public class EasterMainScreen extends WallpaperScreen {
	
	public List<GameObject> ponies;

	public EasterMainScreen(String name) {
		super(name);
	}

	@Override
	public void initialize() {
		//----------------------BACKGROUNDs-------------------------------------------
		final GameObject bg = new GameObject("background", true, false, 0.1f, 1);
		bg.setPositionXY(0,0);
		
		final GameObject bg1 = new GameObject("background1", true, true);
		bg1.setPositionXY(0,78);
		
		//----------------------CLOUD0-------------------------------------------
		final GameObject cloud0_0 = new GameObject("cloud0", true, true);//, 0.1f, 1);
		final GameObject cloud0_1 = new GameObject("cloud0", true, true);//, 0.1f, 1);
		
		cloud0_0.setPositionXY(-200, 782);
		cloud0_1.setPositionXY(-200, 782);
		
		MoveTo cloud0_0_moveTo0 = new MoveTo (-200, 782, 960, 782, 60, 0, true);
		MoveTo cloud0_1_moveTo0 = new MoveTo (-200, 782, 960, 782, 60, 30, true);

		cloud0_0.action(cloud0_0_moveTo0);
		cloud0_1.action(cloud0_1_moveTo0);
		
		//----------------------CLOUD1-------------------------------------------
		final GameObject cloud1_0 = new GameObject("cloud0", true, true);//, 0.1f, 1);
		final GameObject cloud1_1 = new GameObject("cloud0", true, true);//, 0.1f, 1);
		
		cloud1_0.setPositionXY(-150 + 960 * 0.5f, 695);
		cloud1_0.setScaleX(0.75f);
		cloud1_0.setScaleY(0.75f);
		
		cloud1_1.setPositionXY(-150, 715);
		cloud1_1.setScaleX(0.75f);
		cloud1_1.setScaleY(0.75f);
		
		MoveTo cloud1_0_moveTo0 = new MoveTo (1010, 695, 60, 0, false);
		MoveTo cloud1_0_moveTo1 = new MoveTo (-150, 695, 1010, 695, 120, 60, true);
		
		MoveTo cloud1_1_moveTo0 = new MoveTo (1010, 695, 120, 0, true);

		cloud1_0.action(cloud1_0_moveTo0);
		cloud1_0.action(cloud1_0_moveTo1);
		cloud1_1.action(cloud1_1_moveTo0);
		
		//----------------------CLOUD2-------------------------------------------
		final GameObject cloud2_0 = new GameObject("cloud0", true, true);//, 0.1f, 1);
		final GameObject cloud2_1 = new GameObject("cloud0", true, true);//, 0.1f, 1);
		cloud2_0.setScaleX(-1);
		
		cloud2_0.setPositionXY(-100 + 960 * 0.75f, 637);
		cloud2_0.setScaleX(-0.5f);
		cloud2_0.setScaleY(0.5f);
		
		cloud2_1.setPositionXY(-100 + 960 * 0.25f, 637);
		cloud2_1.setScaleX(-0.5f);
		cloud2_1.setScaleY(0.5f);
		
		MoveTo cloud2_0_moveTo0 = new MoveTo (1060, 637, 60, 0, false);
		MoveTo cloud2_0_moveTo1 = new MoveTo (-100, 637, 1060, 637, 240, 60, true);
		
		MoveTo cloud2_1_moveTo0 = new MoveTo (1060, 637, 180, 0, false);
		MoveTo cloud2_1_moveTo1 = new MoveTo (-100, 637, 1060, 637, 240, 180, true);

		cloud2_0.action(cloud2_0_moveTo0);
		cloud2_0.action(cloud2_0_moveTo1);
		cloud2_1.action(cloud2_1_moveTo0);
		cloud2_1.action(cloud2_1_moveTo1);
     	
		//----------------------RABBIT1-------------------------------------------
		final GameObject ear1_0 = new GameObject("rabbit1-ear0", true, true);
		ear1_0.setOnCreateFilter(true);
		ear1_0.setPositionXY(799, 295);
		ear1_0.setOriginX(66);
		ear1_0.setOriginY(3);
		
		RotateTo ear1_0_rotate1 = RotateTo.$(10, 4);
		RotateTo ear1_0_rotate2 = RotateTo.$(0, 4);
		ear1_0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(ear1_0_rotate1, Delay.$(ear1_0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		final GameObject ear1_1 = new GameObject("rabbit1-ear1", true, true);
		ear1_1.setOnCreateFilter(true);
		ear1_1.setPositionXY(861, 280);
		ear1_1.setOriginX(32);
		ear1_1.setOriginY(11);

		RotateTo ear1_1_rotate1 = RotateTo.$(-5, 4);
		RotateTo ear1_1_rotate2 = RotateTo.$(0, 4);
		ear1_1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(ear1_1_rotate1, Delay.$(ear1_1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);
		
		FadeIn eyelids_fadeIn = FadeIn.$(0.1f);
		FadeOut eyelids_fadeOut = FadeOut.$(0.1f);

		final GameObject rabbit1 = new GameObject("rabbit1", true, true);
		rabbit1.setPositionXY(820, 156);
		
		final GameObject rabbit1_eyes = new GameObject("rabbit1-eyes", true, true);
		rabbit1_eyes.setPositionXY(828, 253);
		
		rabbit1_eyes.color.a = 0.0f;
		
		rabbit1_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										8)
								), 
					          5)
					   );			
		
		//----------------------EGG2-------------------------------------------
		final GameObject egg2 = new GameObject("egg2", true, true);
		egg2.setPositionXY(814, 159);

		//----------------------EGG1-------------------------------------------
		final GameObject egg1 = new GameObject("egg1", true, true);
		egg1.setPositionXY(846, 104);
		
		//----------------------MAIN_GRASS-------------------------------------------
		final GameObject main_grass = new GameObject("main-grass", true, true);
		main_grass.setPositionXY(0, 0);

		//----SPIKE_SHADOW----
		final GameObject spike_shadow = new GameObject("object-shadow", true, true);
		spike_shadow.setPositionXY(422, 133);
		spike_shadow.setOriginX(115);
		spike_shadow.setOriginY(13);
		
		ScaleTo shadow_scale1 = ScaleTo.$(1.02f, 1.02f, 2);
		ScaleTo shadow_scale2 = ScaleTo.$(0.98f, 0.98f, 2);
		shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		spike_shadow.action(Forever.$(Sequence.$(shadow_scale1, shadow_scale2)));		
		

		//----EGG_SHADOW----
		final GameObject egg_shadow = new GameObject("object-shadow", true, true);
		egg_shadow.setPositionXY(210, 133);
		egg_shadow.setOriginX(115);
		egg_shadow.setOriginY(13);
		egg_shadow.scaleX = 0.5f;
		egg_shadow.scaleY = 0.5f;

		ScaleTo egg_shadow_scale1 = ScaleTo.$(0.55f, 0.55f, 4);
		ScaleTo egg_shadow_scale2 = ScaleTo.$(0.45f, 0.45f, 4);
		egg_shadow_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		egg_shadow_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		egg_shadow.action(Forever.$(Sequence.$(egg_shadow_scale1, egg_shadow_scale2)));		
		
		//----------------------EGG0-------------------------------------------
		final GameObject egg0 = new GameObject("egg0", true, true);
		egg0.setPositionXY(274, 320);
		egg0.setOriginX(43);
		egg0.setOriginY(39);
		
		MoveTo egg0_moveTo0 = new MoveTo (274, 315, 4, 0, false);
		MoveTo egg0_moveTo1 = new MoveTo (274, 325, 4, 0, false);
		egg0_moveTo0.setInterpolator(AccelerateDecelerateInterpolator.$());
		egg0_moveTo1.setInterpolator(AccelerateDecelerateInterpolator.$());
		egg0.action(Forever.$(Sequence.$(egg0_moveTo0, egg0_moveTo1)));	
		
		ScaleTo egg0_scale1 = ScaleTo.$(0.95f, 0.95f, 4);
		ScaleTo egg0_scale2 = ScaleTo.$(1.05f, 1.05f, 4);
		egg0_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		egg0_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		egg0.action(Forever.$(Sequence.$(egg0_scale1, egg0_scale2)));			

		RotateTo egg0_rotate1 = RotateTo.$(10, 8);
		RotateTo egg0_rotate2 = RotateTo.$(0, 8);
		egg0_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		egg0_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		egg0.action(Forever.$(Sequence.$(egg0_rotate1, egg0_rotate2)));
		
		//----------------------SPIKE-------------------------------------------
		//----SPIKE-CONTAINER----
		final GameObject spike_container = new GameObject();
		spike_container.setPositionXY(405, 140);
		spike_container.setOriginX(156);
		spike_container.setOriginY(6);
		
		ScaleTo spike_scale1 = ScaleTo.$(1.01f, 0.99f, 2);
		ScaleTo spike_scale2 = ScaleTo.$(1.0f, 1.0f, 2);
		spike_container.action(Forever.$(Sequence.$(spike_scale1, spike_scale2)));	
		
		//----SPIKE-MAIN----
		final GameObject spike = new GameObject("spike", false, true);
		spike.setOnCreateFilter(true);
		spike.setPositionXY(55, 0);
 
		//----SPIKE-HAND0----
		final GameObject spike_hand0 = new GameObject("spike-hand0", false, true);
		spike_hand0.setPositionXY(148, 201);
		spike_hand0.setOriginX(12);
		spike_hand0.setOriginY(10);
		
		RotateTo spike0_rotate1 = RotateTo.$(5, 2);
		RotateTo spike0_rotate2 = RotateTo.$(0, 2);
		spike_hand0.action(Forever.$(Sequence.$(spike0_rotate1, spike0_rotate2)));

		//----SPIKE-HAND1----
		final GameObject spike_hand1 = new GameObject("spike-hand1", false, true);
		spike_hand1.setPositionXY(0, 259);
		spike_hand1.setOriginX(66);
		spike_hand1.setOriginY(19);

		RotateTo spike_rotate1 = RotateTo.$(-5, 2);
		RotateTo spike_rotate2 = RotateTo.$(0, 2);
		spike_hand1.action(Forever.$(Sequence.$(spike_rotate1, spike_rotate2)));
		
		final GameObject spike_eyes = new GameObject("spike-eyes", false, true);
		spike_eyes.setPositionXY(69, 244);
		
		eyelids_fadeIn = FadeIn.$(0.1f);
		eyelids_fadeOut = FadeOut.$(0.1f);
		
		spike_eyes.color.a = 0.0f;
		
		spike_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										6)
								), 
					          3)
					   );			
		
		//----ADD_ACTORS----
		spike_container.addActor(spike_hand1);
		spike_container.addActor(spike);
		spike_container.addActor(spike_eyes);
		spike_container.addActor(spike_hand0);		
		
		//----------------------SHADOW-------------------------------------------
		final GameObject shadow = new GameObject("shadow", true, true);
		shadow.setPositionXY(0, 0);
		shadow.setWidth(538);
		shadow.setHeight(643);
		
     	//----------------------RABBIT0-------------------------------------------
		final GameObject ear0_0 = new GameObject("rabbit0-ear0", true, true);
		ear0_0.setOnCreateFilter(true);
		ear0_0.setPositionXY(141, 299);
		ear0_0.setOriginX(33);
		ear0_0.setOriginY(4);
		
		RotateTo ear0_0_rotate1 = RotateTo.$(10, 4);
		RotateTo ear0_0_rotate2 = RotateTo.$(0, 4);
		ear0_0.action(Delay.$(
				Forever.$(
						Delay.$(
								Sequence.$(ear0_0_rotate1, Delay.$(ear0_0_rotate2, 2))
								,5) 
						 )
				   ,5)
			);

		final GameObject ear0_1 = new GameObject("rabbit0-ear1", true, true);
		ear0_1.setOnCreateFilter(true);
		ear0_1.setPositionXY(192, 319);
		ear0_1.setOriginX(16);
		ear0_1.setOriginY(4);

		RotateTo ear0_1_rotate1 = RotateTo.$(-5, 4);
		RotateTo ear0_1_rotate2 = RotateTo.$(0, 4);
		ear0_1.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(ear0_1_rotate1, Delay.$(ear0_1_rotate2, 2))
										,5) 
								 )
						   ,5)
					);
		
		final GameObject rabbit0 = new GameObject("rabbit0", true, true);
		rabbit0.setPositionXY(149, 187);
		
		final GameObject rabbit0_eyes = new GameObject("rabbit0-eyes", true, true);
		rabbit0_eyes.setPositionXY(189, 278);
		
		rabbit0_eyes.color.a = 0.0f;
		
		rabbit0_eyes.action(Delay.$(
						Forever.$(
								Delay.$(
										Sequence.$(eyelids_fadeIn, Delay.$(eyelids_fadeOut, 0.3f)), 
										8)
								), 
					          8)
					   );			
		
		//----------------------GRASS-------------------------------------------
		//----GRASS1----
		final GameObject grass1 = new GameObject("grass1", true, true, 3, 1);
		grass1.setWidth(1600);
		grass1.setHeight(339);
		grass1.setPositionXY(-540, -5);

		//----------------------BEE0-------------------------------------------
		//----BEE-CONTAINER----
		final GameObject bee0_container = new GameObject();
		bee0_container.setPositionXY(308, 94);
		
		//----BEE-BODY----
		final GameObject bee0 = new GameObject("bee", false, true, 3, 1);
		bee0.setPositionXY(1, 0);
 
		//----BEE-WING0----
		final GameObject bee0_wing0 = new GameObject("bee-wing", false, true, 3, 1);
		bee0_wing0.setPositionXY(23, 30);
		bee0_wing0.setOriginX(22);
		bee0_wing0.setOriginY(5);

		//----BEE-WING1----
		final GameObject bee0_wing1 = new GameObject("bee-wing", false, true, 3, 1);
		bee0_wing1.setPositionXY(23, 30);
		bee0_wing1.setOriginX(22);
		bee0_wing1.setOriginY(5);
		bee0_wing1.rotation = -50;

		//----ADD_ACTORS----
		bee0_container.addActor(bee0_wing0);
		bee0_container.addActor(bee0_wing1);
		bee0_container.addActor(bee0);

		//----------------------BEE1-------------------------------------------
		//----BEE-CONTAINER----
		final GameObject bee1_container = new GameObject();
		bee1_container.setPositionXY(696, 98);
		
		//----BEE-BODY----
		final GameObject bee1 = new GameObject("bee2", false, true, 3, 1);
		bee1.setPositionXY(1, 0);
 
		//----BEE-WING0----
		final GameObject bee1_wing0 = new GameObject("bee-wing2", false, true, 3, 1);
		bee1_wing0.setPositionXY(16, 23);
		bee1_wing0.setOriginX(8);
		bee1_wing0.setOriginY(4);
		bee1_wing0.rotation = 50;		

		//----BEE-WING1----
		final GameObject bee1_wing1 = new GameObject("bee-wing2", false, true, 3, 1);
		bee1_wing1.setPositionXY(16, 23);
		bee1_wing1.setOriginX(8);
		bee1_wing1.setOriginY(4);

		//----ADD_ACTORS----
		bee1_container.addActor(bee1_wing0);
		bee1_container.addActor(bee1_wing1);
		bee1_container.addActor(bee1);
		
		
		//----------------------TREE-------------------------------------------
		final GameObject tree = new GameObject("tree", true, true);
		tree.setOnCreateFilter(true);
		
		tree.setPositionXY(540, 290);
		tree.setWidth(469);
		tree.setHeight(533);
		tree.setOriginX(458);
		tree.setOriginY(261);
		tree.rotation = -1;

		RotateTo tree_rotate1 = RotateTo.$(1, 5);
		RotateTo tree_rotate2 = RotateTo.$(-1, 5);
		tree_rotate1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tree_rotate2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tree.action(Forever.$(Sequence.$(tree_rotate1, tree_rotate2)));
		
		ScaleTo tree_scale1 = ScaleTo.$(1.02f, 1.02f, 5);
		ScaleTo tree_scale2 = ScaleTo.$(0.98f, 0.98f, 5);
		tree_scale1.setInterpolator(AccelerateDecelerateInterpolator.$());
		tree_scale2.setInterpolator(AccelerateDecelerateInterpolator.$());
		tree.action(Forever.$(Sequence.$(tree_scale1, tree_scale2)));		
		
	}

	public void setActivePony(int index)
	{

		/*ImageCache.getManager().finishLoading();
		
		for(GameObject pony: ponies)
		{
			ImageCache.unload(name, pony.skinName);
			pony.clearSkin();
			pony.visible = false;
		}
		
		GameObject pony = ponies.get(index); 
		ImageCache.load(name, pony.skinName);
		pony.visible = true;*/
	}

	public void initializeParticles() {
		//----------------------PARTICLES-------------------------------------------	
		/*GameObject main_particles = (GameObject) MyEasterPonyLW.getRoot().findActor("p0");
		
		if (main_particles == null)
		{
			main_particles = new GameObject("p0",true, 0.1f, 1);
			main_particles.setPositionXY(0,0);
			main_particles.setOriginX(460);
			main_particles.setOriginY(560);
			main_particles.createParticle("p0", false);
		}
		
		sparklesVisible = true;
		main_particles.visible = sparklesVisible;*/
	}

	public void disableParticles()
	{
		sparklesVisible = false;
		GameObject particle = (GameObject) MyEasterPonyLW.getRoot().findActor("p0");
		
		if(particle != null)
			//particle.clearParticlesAndRemove();
			particle.visible = sparklesVisible;
		
	}
	
	@Override
	public void onConfigChanged() {
		if (sparklesVisible && !showParticles)
			disableParticles();
		else if(!sparklesVisible && showParticles)
			initializeParticles();
	}
}
