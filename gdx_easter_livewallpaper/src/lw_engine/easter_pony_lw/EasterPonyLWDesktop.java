package lw_engine.easter_pony_lw;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.jogl.JoglApplication;

public class EasterPonyLWDesktop {
	public static void main(String[] argv)
	{
		JoglApplication app = new JoglApplication((ApplicationListener) new MyEasterPonyLW(), "My Easter Pony LW App", 960, 854,  false);
	}
}
