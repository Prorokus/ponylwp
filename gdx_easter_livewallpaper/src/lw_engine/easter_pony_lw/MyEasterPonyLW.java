package lw_engine.easter_pony_lw;

import lwp_engine.LWPApplication;

public class MyEasterPonyLW extends LWPApplication{

	public String MY_PONY_PREFS = "com.blogspot.androidlivewall.easter_pony_lw_preferences";

	public static boolean showParticles = true;
	public static boolean animetePony = true;
	
	@Override
	protected String getPrefsName()
	{
		return MY_PONY_PREFS;
	}
	
	@Override
	protected void loadWallScreen(boolean... params)
	{
		if (params[0] == true)
		{
			wallScreen = new EasterMainScreen("easter");
		}
		
		prevSelectedWall = selectedWall;
		
		wallScreen.showParticles = showParticles;
		wallScreen.onConfigChanged();
		
		changePonyImage();
	}

	@Override
	protected void readAllPrefs() {
		wallNotMovable = prefs.getBoolean("set_pony_center",false);
		selectedWall = Integer.parseInt(prefs.getString("selected_pony","0"));
		enableParallax = prefs.getBoolean("set_pony_parallax",false);
		showParticles = prefs.getBoolean("set_pony_sparkles",true);
		animetePony = prefs.getBoolean("set_pony_anim",true);
	}
		
	private void changePonyImage()
	{
		final EasterMainScreen screen = (EasterMainScreen)wallScreen;
		screen.setActivePony(selectedWall);
	}

	@Override
	protected void onResumeWallpaper() {
		// TODO Auto-generated method stub
		if (selectedWall != prevSelectedWall)
		{
			changePonyImage();
			prevSelectedWall = selectedWall;
		}
		
		if (wallScreen != null)
		{
			wallScreen.showParticles = showParticles;
			wallScreen.onConfigChanged();
		}
	}
		
}
